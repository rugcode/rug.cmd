# Rugland Digital Systems Console Framework (Rug.Cmd) 

Rugland Console Framework is a collection of classes to enable the fast and consistent development of .NET console applications. Parse command line arguments and write your applications usage. If you are developing command line or build process tools in .NET then this maybe the lightweight framework for you. It is developed in C#.

## Features 


**Argument Parser**

* Easy to use and expandable command line argument parser. Handles: Bool, Plus / Minus, String, String List, CSV, Enumeration. 
* Built in '/?' help mode.
* Built in '/??' and '/?D' document generator modes.



**Verbosity Levels**

* Show incremental levels of detail to aid debugging 



**Text Formatting Helpers** 

* Standardized Formatting for Errors and Exceptions.
* Built in 'Build mode' for integration with visual studio build events.
* Pad and wrap large text blocks.
* Colorize text blocks for maximum readability.



**Basic Console Gui** 

* Headings.
* Simple progress bar.
* Simple logo render methods.



**Console Color Themes**

* Entirely optional.
* Works with all background colors. 
* Embed your color theme in your application. 
* 4 built in color themes. 
* Built in high contrast mode for users with impaired vision.



**Theme Designer Application**

* Simple hybrid console / forms application for editing console themes.



..lots more


## Usage Example 

**Argument Parser** 

```
#!c#

static void Main(string[] args)
{            
	// create the argument parser
	ArgumentParser parser = new ArgumentParser("ArgumentExample", "Example of argument parsing");

	// create the argument for a string
	StringArgument StringArg = new StringArgument("String", "Example string argument", "This argument demonstrates string arguments");

	// add the argument to the parser 
	parser.Add("/", "String", StringArg);
            
	// parse arguemnts
	parser.Parse(args);

	// did the parser detect a /? arguemnt 
	if (parser.HelpMode == false) 
	{
		// was the string argument defiend 
		if (StringArg.Defined == true)
		{
			// write its value
			RC.WriteLine("String argument was defined");
			RC.WriteLine(StringArg.Value);
		}
	}
}

```

**Color Themes**

```
#!c#

RC.Theme = ConsoleColorTheme.Load(ConsoleColorDefaultThemes.Colorful);

``` 

**Verbosity Level Dependant Output**

```
#!c#

RC.WriteLine(ConsoleVerbosity.Silent, ConsoleThemeColor.Text, "This will allways appear");
RC.WriteLine(ConsoleVerbosity.Quiet, ConsoleThemeColor.Text1, "This will only appear when in vebose level is set to Quiet or higher");
RC.WriteLine(ConsoleVerbosity.Minimal, ConsoleThemeColor.Text2, "This will only appear when in vebose level is set to Minimal or higher");
RC.WriteLine(ConsoleVerbosity.Normal, ConsoleThemeColor.SubText2, "This will only appear when in vebose level is set to Normal or higher");
RC.WriteLine(ConsoleVerbosity.Verbose, ConsoleThemeColor.Text3, "This will only appear when in vebose level is set to Verbose or higher");
RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText3, "This will only appear when in vebose level is set to Debug");

```

## Features still to come
* Console panels and controls for creating console based multi threaded applications.
* Better support for PowerScript environments.