﻿/* 
 * Rug.Cmd.Forms
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Rug.Cmd.Forms
{
	public class ConsoleControl : RichTextBox
	{
		private delegate void TextUpdateInvoke(string newText);

		#region Private Members

		private const short WM_PAINT = 0x00f;

		private const UInt32 WM_SETREDRAW = 0x000B;
		private const UInt32 WM_USER = 0x400;
		private const UInt32 EM_GETEVENTMASK = (WM_USER + 59);
		private const UInt32 EM_SETEVENTMASK = (WM_USER + 69);

		[DllImport("user32", CharSet = CharSet.Auto)]
		private extern static IntPtr SendMessage(IntPtr hWnd, UInt32 msg, IntPtr wParam, IntPtr lParam);

		private ConsoleBuffer m_Buffer;

		private string m_Header;

		private long m_OldLastTop = 0;

		#endregion

		#region Public Events And Properties

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public ConsoleBuffer Buffer { get { return m_Buffer; } }

		Timer m_Timer;
		object m_BufferLock = new object();

		// change colors and stuff in the RichTextBox
		StringBuilder m_ContentBuilder = new StringBuilder();
		private bool m_BufferHasChanged = false;


		#endregion

		public ConsoleControl()
		{
			m_Buffer = new ConsoleBuffer();
			m_Timer = new Timer();
			m_Timer.Interval = 1;
			m_Timer.Tick += new EventHandler(m_Timer_Tick);
			
			this.ScrollBars = RichTextBoxScrollBars.ForcedBoth;

			this.BackColor = GetColor(m_Buffer.BackgroundColor);

			this.ReadOnly = true;

			BuildHeader();

			m_Buffer.ContentsChanged += new EventHandler(m_Buffer_ContentsChanged);

			m_Timer.Enabled = true; 
		}

		void m_Timer_Tick(object sender, EventArgs e)
		{
			if (m_BufferHasChanged == true)
			{
				lock (m_BufferLock)
				{
					if (m_BufferHasChanged == true)
					{
						m_ContentBuilder.Remove(0, m_ContentBuilder.Length);

						m_ContentBuilder.Append(m_Header);

						long top;
						int count; 

						foreach (string line in m_Buffer.GetLines(out top, out count))
						{
							m_ContentBuilder.Append(ParseLine(line));
						}

						m_ContentBuilder.Append("}");

						//if (this.InvokeRequired == true)
						//{
						//	this.Invoke(new TextUpdateInvoke(ReplaceText), m_ContentBuilder.ToString());
						//}
						//else
						//{
						ReplaceText(m_ContentBuilder.ToString(), (int)(top - m_OldLastTop));

						m_OldLastTop = top;
						//}

						m_BufferHasChanged = false;
					}
				}
			}
		}

		public void SetColors()
		{
			this.BackColor = GetColor(m_Buffer.BackgroundColor);
		}		 

		private void ReplaceText(string newText, int lineOffset)
		{
			IntPtr eventMask = IntPtr.Zero;

			try
			{
				// Stop redrawing:
				SendMessage(this.Handle, WM_SETREDRAW, IntPtr.Zero, IntPtr.Zero);

				// Stop sending of events:
				eventMask = SendMessage(this.Handle, EM_GETEVENTMASK, IntPtr.Zero, IntPtr.Zero);

				UpdateNewText(newText, lineOffset); 

			}
			finally
			{
				// turn on events
				SendMessage(this.Handle, EM_SETEVENTMASK, IntPtr.Zero, eventMask);

				// turn on redrawing
				SendMessage(this.Handle, WM_SETREDRAW, (IntPtr)1, IntPtr.Zero);
			}

			Invalidate();
		}

		private void UpdateNewText(string newText, int lineOffset)
		{

			int lastLine = GetLineFromCharIndex(this.TextLength - 1);
			int currentLine = GetLineFromCharIndex(GetCharIndexFromPosition(new Point(ClientSize.Width, ClientSize.Height)));
			int topVisibleLine = GetLineFromCharIndex(GetCharIndexFromPosition(new Point(0, 0)));

			int lineCount = currentLine - topVisibleLine;

			this.Rtf = newText;

			if (currentLine >= lastLine)
			{
				this.Select(this.TextLength, 0);
			}
			else
			{
				this.Select(GetFirstCharIndexFromLine(Math.Max(0, (currentLine - lineCount) + 1 - lineOffset)), 0);
			}

			this.ScrollToCaret();					
		}
		
		void m_Buffer_ContentsChanged(object sender, EventArgs e)
		{
			lock (m_BufferLock)
			{
				m_BufferHasChanged = true;
			}
		}

		private string ParseLine(string buffer)
		{
			StringBuilder sb = new StringBuilder();

			// It is unfortunate that this method must be internalised to this class from ConsoleFormatter
			// i would hope that i could move it back. 
			// "\\par\r\n"

			int lastIndex = 0;
			Stack<ConsoleColorExt> stack = new Stack<ConsoleColorExt>();
			ConsoleColorExt foregroundColor = m_Buffer.ForegroundColor;

			sb.Append(@"\cf" + ((int)foregroundColor + 1) + @"\ulnone ");

			foreach (Match match in ConsoleFormatter.FormatRegex.Matches(buffer))
			{
				if (match.Groups["Tag"].Success)
				{
					// start tag 
					// parse the tags inner value
					// e.g. c:# (where # is the name or ID of the colour to use (From ConsoleColourExt) 
					// add the parsed colour to the stack 

					ConsoleColorExt col = ConsoleFormatter.ParseColour(match.Groups["Inner"].Value, m_Buffer.Theme);

					stack.Push(foregroundColor);

					foregroundColor = col;

					sb.Append(@"\cf" + ((int)foregroundColor + 1) + @"\ulnone ");
				}
				else if (match.Groups["EndTag"].Success)
				{
					// end tag 
					// handle stack changes
					if (stack.Count >= 0)
					{
						foregroundColor = stack.Pop();

						sb.Append(@"\cf" + ((int)foregroundColor + 1) + @"\ulnone ");
					}
					else
					{
						//throw new Exception(string.Format(Strings.ConsoleInterpreter_UnexpectedEndTag, match.Index));
						throw new Exception(string.Format("Unexpected end tag {0}", match.Index));
					}
				}
				else if (match.Groups["Text"].Success)
				{
					string wholeMessage = ConsoleFormatter.UnescapeString(match.Value);

					List<string> lines = ConsoleFormatter.SplitLinebreaks(wholeMessage);

					foreach (string line in lines)
					{
						string str = line;

						if (str == "\n" || str == Environment.NewLine)
						{
							sb.AppendLine(@"\par");
							lastIndex = 0;
						}
						else
						{
							string toRender = CleanLiteral(str);

							sb.Append(toRender);

							lastIndex += str.Length;
						}
					}
				}
			}

			sb.AppendLine(@"\par");

			return sb.ToString();
		}

		private string CleanLiteral(string value)
		{
			return value.Replace("\\", @"\'5c").Replace(@"{", @"\{").Replace(@"}", @"\}");
		}

		public void BuildHeader()
		{
			StringBuilder RtfBuilder = new StringBuilder();

			RtfBuilder.AppendLine(@"{\rtf1\ansi\ansicpg1252\deff0{\fonttbl{\f0\fnil\fcharset0 Courier New;}}");
			RtfBuilder.AppendLine(GetColourTable());
			RtfBuilder.Append(@"\viewkind4\uc1\pard\cf1\tx375\lang2057\f0\fs20");

			m_Header = RtfBuilder.ToString();
		}

		private string ToColourTableString(Color color)
		{
			return string.Format(@"\red{0}\green{1}\blue{2};", color.R, color.G, color.B);
		}

		public static Color GetColor(ConsoleColorExt color)
		{
			if (color == ConsoleColorExt.Inhreit)
			{
				return GetColor(ConsoleColor.White);
			}
			else
			{
				return GetColor((ConsoleColor)color);
			}
		}

		public static Color GetColor(ConsoleColor color)
		{
			switch (color)
			{
				case ConsoleColor.Black:
					return Color.Black;
				case ConsoleColor.Blue:
					return Color.Blue;
				case ConsoleColor.Cyan:
					return Color.Cyan;
				case ConsoleColor.DarkBlue:
					return Color.DarkBlue;
				case ConsoleColor.DarkCyan:
					return Color.DarkCyan;
				case ConsoleColor.DarkGray:
					return Color.DarkGray;
				case ConsoleColor.DarkGreen:
					return Color.DarkGreen;
				case ConsoleColor.DarkMagenta:
					return Color.DarkMagenta;
				case ConsoleColor.DarkRed:
					return Color.DarkRed;
				case ConsoleColor.DarkYellow:
					return Color.YellowGreen;
				case ConsoleColor.Gray:
					return Color.Gray;
				case ConsoleColor.Green:
					return Color.Green;
				case ConsoleColor.Magenta:
					return Color.Magenta;
				case ConsoleColor.Red:
					return Color.Red;
				case ConsoleColor.White:
					return Color.White;
				case ConsoleColor.Yellow:
					return Color.Yellow;
				default:
					return Color.Black;
			}
		}

		private string GetColourTable()
		{
			StringBuilder colourTable = new StringBuilder();
			colourTable.Append(@"{\colortbl ;");

			for (int i = 0; i <= (int)ConsoleColor.White; i++)
			{
				colourTable.Append(ToColourTableString(GetColor((ConsoleColor)i)));
			}

			colourTable.Append(@"}");

			return colourTable.ToString();
		}
	}
}
