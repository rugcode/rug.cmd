﻿/* 
 * Rug.Cmd.Terminal
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Rug.Cmd.Terminal
{
    public class Defines
    {
        static System.Text.RegularExpressions.Regex SimpleLogic
            = new System.Text.RegularExpressions.Regex(@"(?<Name>[0-9a-z]+)|(?<Not>\x21)|(?<Or>\x7C)|(?<And>\x26)",
                                                System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.ExplicitCapture | System.Text.RegularExpressions.RegexOptions.Compiled);


        private readonly Dictionary<string, bool> Lookup = new Dictionary<string, bool>();

        public IEnumerable<string> Keys { get { return Lookup.Keys; } } 

        public bool this[string key]
        {
            get 
            {
                if (Lookup.ContainsKey(key))
                    return Lookup[key];

                return false; 
            }
            set 
            {
                if (!Lookup.ContainsKey(key))
                    Lookup.Add(key, value);
                else
                    Lookup[key] = value;
            }
        }

        private bool m_Ignore = false;

        public bool Ignore
        {
            get { return m_Ignore; }
            set { m_Ignore = value; }
        }
        
        private readonly List<string> m_DefineStack = new List<string>(16);

        public bool ShouldIgnore(string condition)
        {
            if (condition.IndexOfAny(new char[] { '(', ')' }) >= 0)
            {
                // get all the conditions and operators at this level 
                //List<string> atThisLevel = new List<string>(3); 
                
                bool currentResult = true;
                bool not = false;
                int opp = 0; // 0 no opp, 1 Or, 2 And 

                int index = condition.IndexOf('(');

                string lastOp = null; 
                if (index > 1)
                {
                    // pick off any operators in front?          
                    string firstDefine = condition.Substring(0, index);

                    if (firstDefine.EndsWith("!"))
                    {
                        firstDefine = firstDefine.Substring(0, firstDefine.Length - 1);
                        not = true;
                    }

                    lastOp = firstDefine.Substring(firstDefine.Length - 1, 1);
                    firstDefine = firstDefine.Substring(0, firstDefine.Length - 1);

                    currentResult = ShouldIgnoreSimple(firstDefine);
                }
                else if (index == 1)
                {
                    lastOp = condition.Substring(0, 1);
                }
                
                if (lastOp != null)
                {
                    if (lastOp == "!")
                        not = true;
                    else if (lastOp == "|")
                        opp = 1;
                    else if (lastOp == "&")
                        opp = 2;
                }


                int start, end; 
                start = index; 
                end = condition.Length; 
                while (start < condition.Length)
                {
                    string nextSection = ParseCondition(condition, ref start, ref end);                                      
                    bool nextResult = ShouldIgnore(nextSection);

                    if (opp == 0)
                    {
                        if (not) 
                            currentResult = !nextResult; 
                        else
                            currentResult = nextResult; 
                    }
                    else if (opp == 1)
                    {
                        if (not)
                            currentResult = currentResult | !nextResult;
                        else
                            currentResult = currentResult | nextResult;
                    }
                    else if (opp == 2)
                    {
                        if (not)
                            currentResult = currentResult & !nextResult;
                        else
                            currentResult = currentResult & nextResult; 
                    }

                    end++; 
                    index = condition.IndexOf('(', end);

                    bool endsNow = !(index > end);

                    if (endsNow)
                        index = condition.Length; 

                    if (index > end)
                    {
                        string firstDefine = condition.Substring(end, index - end);

                        lastOp = firstDefine.Substring(0, 1);

                        if (lastOp == "|")
                        {
                            opp = 1;
                            firstDefine = firstDefine.Substring(0, firstDefine.Length - 1);
                        }
                        else if (lastOp == "&")
                        {
                            opp = 2;
                            firstDefine = firstDefine.Substring(0, firstDefine.Length - 1);
                        }
                        else
                            throw new Exception("Missing & or | on condition '" + firstDefine + "'");

                        if (firstDefine.Length > 0)
                        {
                            bool nextNot = false;

                            if (firstDefine.EndsWith("!"))
                            {
                                firstDefine = firstDefine.Substring(0, firstDefine.Length - 1);
                                nextNot = true;
                            }

                            string nextOp = null;

                            if (!endsNow)
                            {
                                nextOp = firstDefine.Substring(firstDefine.Length - 1, 1);
                                firstDefine = firstDefine.Substring(0, firstDefine.Length - 1);
                            }

                            if (firstDefine.Length > 0)
                            {
                                nextResult = ShouldIgnoreSimple(firstDefine);

                                if (opp == 1)
                                {
                                    if (not)
                                        currentResult = currentResult | !nextResult;
                                    else
                                        currentResult = currentResult | nextResult;
                                }
                                else if (opp == 2)
                                {
                                    if (not)
                                        currentResult = currentResult & !nextResult;
                                    else
                                        currentResult = currentResult & nextResult;
                                }
                            }

                            if (!endsNow)
                            {


                                if (nextOp == "|")
                                    opp = 1;
                                else if (nextOp == "&")
                                    opp = 2;

                                not = nextNot;
                            }
                        }
                    }

                    start = index;
                    end = condition.Length; 
                }

                return currentResult; 
            }
            else
            {
                return ShouldIgnoreSimple(condition); 
            }            
        }

        private string ParseCondition(string conditions, ref int start, ref int end)
        {
            int stack = 0;
            int index = start; 
            start = index + 1;
            end = conditions.Length;
            while (index < conditions.Length)
            {
                if (conditions[index] == '(')
                    stack++;
                else if (conditions[index] == ')')
                    stack--;

                if (stack == 0)
                {
                    end = index;
                    break;
                }

                index++;
            }

            if (end - start > 0)
                return conditions.Substring(start, end - start);
            else 
                return ""; 
        }

        private bool ShouldIgnoreSimple(string condition)
        {
            bool currentResult = true;
            bool not = false;
            bool doneWork = false;
            int opp = 0; // 0 no opp, 1 Or, 2 And 

            foreach (Match match in SimpleLogic.Matches(condition))
            {
                if (match.Groups["Name"].Success)
                {
                    string nextToken = match.Groups["Name"].Value;

                    if (opp == 1)
                    {
                        if (not)
                            currentResult = currentResult | !this[nextToken];
                        else
                            currentResult = currentResult | this[nextToken];
                    }
                    else if (opp == 2)
                    {
                        if (not)
                            currentResult = currentResult & !this[nextToken];
                        else
                            currentResult = currentResult & this[nextToken];
                    }
                    else
                    {
                        if (!doneWork)
                        {
                            if (not)
                                currentResult = !this[nextToken];
                            else
                                currentResult = this[nextToken];
                        }
                        // else THROW ERROR
                    }
                    // else THROW ERROR

                    nextToken = null;
                    not = false;
                    doneWork = true;
                    opp = 0;
                }
                else if (match.Groups["Not"].Success)
                {
                    not = !not;
                }
                else if (match.Groups["Or"].Success)
                {
                    opp = 1;
                }
                else if (match.Groups["And"].Success)
                {
                    opp = 2;
                }
            }

            return currentResult;
        }

        public static string GetOriginalCondition(string condition)
        {
            if (condition.IndexOf('/') >= 0)
                condition = condition.Substring(condition.IndexOf('/') + 1);

            return condition; 
        }

        public static string GetInverseCondition(string condition)
        {
            // make this better!
            return "!(" + condition + ")"; 
        }

        public void RecalculateIgnore()
        {
            m_Ignore = false; 
            bool currentResult = true;

            foreach (string condition in m_DefineStack)
            {
                string cond = condition;
                // chop off any commented out logic 
                if (cond.IndexOf('/') >= 0)
                    cond = cond.Substring(0, cond.IndexOf('/'));

                currentResult = ShouldIgnore(cond);

                if (!currentResult)
                    break; 
            }

            m_Ignore = !currentResult; 
        }

        public void Push(string define)
        {                   
            m_DefineStack.Add(define);
            RecalculateIgnore(); 
        }

        public string Pop()
        {
            if (m_DefineStack.Count > 0)
            {        
                int popIndex = m_DefineStack.Count - 1;
                string popped = m_DefineStack[popIndex];
                m_DefineStack.RemoveAt(popIndex);

                /*m_Ignore = false; 

                foreach (string define in m_DefineStack) 
                    if (!this[define])
                        m_Ignore = true; */
                RecalculateIgnore();

                return popped;
            }
            else
                throw new Exception("Define stack underflow"); 
        }

        public void ClearStack()
        {
            m_DefineStack.Clear();
        }

        public void ClearDefines()
        {
            Lookup.Clear();        
        }
    }
}
