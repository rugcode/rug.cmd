﻿/* 
 * Rug.Cmd.Terminal
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Rug.Cmd.Terminal
{
    public delegate void CommandEvent(string commandString);

    public class CommandSystem
    {
        #region Replacements

        public Dictionary<string, string> Replacements = new Dictionary<string, string>();

        public string DoReplacements(string input)
        {
            string output = input;

            foreach (KeyValuePair<string, string> pair in Replacements)
            {
                output = output.Replace(pair.Key, pair.Value);
            }

            return output;
        }

        #endregion 

        #region Commands

        public event CommandEvent CommandExecuted;
		public event CommandEvent DefaultCommand; 

        public Dictionary<string, ConsoleCommand> Commands = new Dictionary<string, ConsoleCommand>();

        public static readonly Regex ArgumentStripperRegex =
                new Regex(@"(?<Quoted>(\x22|\x27)[^\x22\x27]*(\x22|\x27))|(?<Word>[^\s]+)|(?<WS>[\s]+)",
                    RegexOptions.Compiled |
                    RegexOptions.IgnoreCase |
                    RegexOptions.ExplicitCapture);

        #endregion 

        #region Echo Property

        private bool m_Echo = true;

        public bool Echo
        {
            get { return m_Echo; }
            set { m_Echo = value; }
        }

        #endregion 

        #region Defines Collection Property

        private Defines m_Defines = new Defines();  

        public Defines Defines
        {
            get { return m_Defines; }
            protected set { m_Defines = value; }
        }
        
        #endregion

		private BoolSwitch MoreSwitch = new BoolSwitch("Print full help text", "Print full help text"); 
        
        public CommandSystem()
        {
            #region Install Default Commands 

            ConsoleCommand helpCommandInfo = new ConsoleCommand("HELP", "Print help screen", CommandsCommand);

			helpCommandInfo.Parser.Add("/", "More", MoreSwitch); 

            Commands.Add("HELP", helpCommandInfo);
            //Commands.Add("?", helpCommandInfo);

			/* 
            ConsoleCommand echoBitCommandInfo = new ConsoleCommand("@ECHO", "Echo commands", EchoBitCommand);
            echoBitCommandInfo.Parser.FirstArgument = new StringArgument("Echo on", "true if echo should be on", "true if echo should be on");
            Commands.Add("@ECHO", echoBitCommandInfo);

            ConsoleCommand echoCommandInfo = new ConsoleCommand("ECHO", "Write any following text to the console", EchoCommand);
            echoCommandInfo.Parser.DefaultArgument = new StringListArgument("Text", "Will be written the the console", "Will be written the the console");
            Commands.Add("ECHO", echoCommandInfo);
			*/ 

            #endregion 
        }

        #region Execute

        protected virtual void ExecuteCommand(string commandLine)
        {
			if (CommandExecuted != null)
			{
				CommandExecuted(commandLine);
			}

            ParseCommand(commandLine);
        }

        protected void ParseCommand(string input)
        {
            try
            {
                bool first = true;

                string commandName;
                List<string> @params = new List<string>(4);

                StringBuilder sb = new StringBuilder();

                ConsoleCommand foundCommand = null;

                string replaced = DoReplacements(input);

                foreach (Match match in ArgumentStripperRegex.Matches(replaced))
                {
                    string str;

                    if (first)
                    {
                        if (match.Groups["WS"].Success)
                            continue;

                        commandName = match.Value.ToUpper();

                        if (Commands.ContainsKey(commandName))
                            foundCommand = Commands[commandName];
                    }
                    else if (!match.Groups["WS"].Success)
                    {
                        if (match.Groups["Quoted"].Success)
                            @params.Add(match.Value.Substring(1, match.Value.Length - 2));
                        else
                            @params.Add(match.Value);
                    }

                    if (match.Groups["Quoted"].Success)
                    {
                        if (first && foundCommand != null)
							str = ConsoleFormatter.Format(RC.Theme[ConsoleThemeColor.Text2], match.Value);
                        else if (first)
							str = ConsoleFormatter.Format(RC.Theme[ConsoleThemeColor.TextBad], match.Value);
                        else
							str = ConsoleFormatter.Format(RC.Theme[ConsoleThemeColor.SubText2], match.Value);
                    }
                    else if (match.Groups["Word"].Success)
                    {
                        if (first && foundCommand != null)
							str = ConsoleFormatter.Format(RC.Theme[ConsoleThemeColor.TitleText], match.Value);
                        else if (first)
							str = ConsoleFormatter.Format(RC.Theme[ConsoleThemeColor.TextBad], match.Value);
                        else
							str = ConsoleFormatter.Format(RC.Theme[ConsoleThemeColor.SubText], match.Value);
                    }
                    else
                        str = match.Value;

                    sb.Append(str);
                    first = false;
                }

				if (m_Echo)
				{
					RC.WriteInterpretedLine(sb.ToString()); // + "\n");
				}

				if (foundCommand != null)
				{
					foundCommand.Execute(this, @params.ToArray());
				}
				else if (DefaultCommand != null)
				{
					DefaultCommand(input);
				}
				else
				{
					RC.WriteError(00, Rug.Cmd.Terminal.Strings.Commads_CommandNotFound);
				}
            }
            catch (Exception ex)
            {
                RC.WriteException(01, ex);
            }
        }

        #endregion 

        #region Run Command

        public virtual void RunCommand(string commandString)
        {
            ExecuteCommand(commandString);
        }

        #endregion

        #region Default Commands

        #region Echo Commands

        private void EchoBitCommand(CommandSystem context, ConsoleCommand command)
        {            
            Echo = bool.Parse(command.Parser.FirstArgument.ObjectValue.ToString());
        }

        private void EchoCommand(CommandSystem context, ConsoleCommand command)
        {
            StringBuilder sb = new StringBuilder();

            foreach (string str in command.Parser.DefaultArgument.Value)
                sb.Append(DoReplacements(str) + " ");

            RC.WriteLine(ConsoleThemeColor.TitleText, sb.ToString());
        }

        #endregion

        #region Help Commands

        private void CommandsCommand(CommandSystem context, ConsoleCommand command)
        {
			if (MoreSwitch.Defined == true)
			{
				WriteHelpToConsole();

				return; 
			}

			RC.WriteLine(ConsoleVerbosity.Silent); 
			RC.WriteLine(ConsoleVerbosity.Silent, ConsoleThemeColor.TitleText, "Commands:");

			foreach (string str in Commands.Keys)
			{
				Commands[str].WriteArgumentsUsageToConsole();				
			}
			RC.WriteLine(ConsoleVerbosity.Silent); 
        }

        public void WriteHelpToConsole()
        {
			RC.WriteLine(ConsoleVerbosity.Silent); 
			RC.WriteLine(ConsoleVerbosity.Silent, ConsoleThemeColor.TitleText, "Commands:");

			foreach (string str in Commands.Keys)
			{
				Commands[str].WriteHelpToConsole();
				RC.WriteLine(ConsoleVerbosity.Silent); 
			}

			RC.WriteLine(ConsoleVerbosity.Silent); 
        }

        #endregion

        #endregion 
    }
}
