﻿/* 
 * Rug.Cmd.Terminal
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System.Text;

namespace Rug.Cmd.Terminal
{
    public class ValuesToolkit
    {
        public static void Install(CommandSystem system)
        {
            ConsoleCommand varCommandInfo = new ConsoleCommand("VAR", "Create and set the value of a variable", VarCommand);
            varCommandInfo.Parser.FirstArgument = new StringArgument("Name", "The name of the variable", "The name of the variable to create and/or set the value of");
            varCommandInfo.Parser.DefaultArgument = new StringListArgument("Value", "Any number of strings to be used as the value", "Any strings from the remainder of the line will be concatinated");

            system.Commands.Add(varCommandInfo.Name, varCommandInfo);                        
        }

        public static void VarCommand(CommandSystem system, ConsoleCommand command)
        {
            string assignTo = command.Parser.FirstArgument.ObjectValue.ToString();

            StringBuilder sb = new StringBuilder();

            foreach (string param in command.Parser.DefaultArgument.Value)
            {
                sb.Append(param); 
            }

            string assignReplace = string.Format("[{0}]", assignTo);

            if (!system.Replacements.ContainsKey(assignReplace))
                system.Replacements.Add(assignReplace, sb.ToString()); 
            else
                system.Replacements[assignReplace] = sb.ToString(); 
        }
    }
}
