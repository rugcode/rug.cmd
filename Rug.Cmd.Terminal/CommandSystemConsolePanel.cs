﻿/* 
 * Rug.Cmd.Terminal
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using Rug.Cmd.Terminal;

namespace Rug.Cmd.AppPanels
{
    public class CommandSystemConsolePanel : Panel
    {
        CommandSystemConsole m_Console;
		private int m_CommandLineSpace = 3;

        public override bool IsSelectedTab
        {
            get
            {
                return base.IsSelectedTab;
            }
            set
            {
                base.IsSelectedTab = value;

                //if (m_Console.IsSelectedTab != value)
                //{
                    //m_Console.CommandActiveChanged();
                    m_Console.Redraw();
                //}

                m_Console.IsSelectedTab = value;
            }
        }

		public int CommandLineSpace 
		{
			get { return m_CommandLineSpace; }
			set { m_CommandLineSpace = value; } 
		}

        public CommandSystemConsolePanel(CommandSystemConsole console)
        {
            m_Console = console;
            m_Console.ContentsChanged += new EventHandler(m_Console_ContentsChanged);
			m_Console.Exiting += new EventHandler(m_Console_Exiting);
        }

		void m_Console_Exiting(object sender, EventArgs e)
		{
			AnyChange = true;
		}

        void m_Console_ContentsChanged(object sender, EventArgs e)
        {
            AnyChange = true;
        }

        public override void Init(int Top, int PanelSize)
        {
            base.Init(Top, PanelSize);                       

            RC.Sys.CursorTop = Top;            
            RC.Sys.CursorLeft = 0;
            RC.Sys.WriteLine();

			m_Console.NumberOfConsoleLines = PanelSize - m_CommandLineSpace; 

            m_Console.Setup(); 
        }

        public override void Render(int Top, int PanelSize)
        {
            if (AnyChange)
            {
                m_Console.Redraw();
                AnyChange = false;                
            }

            m_Console.RenderConsole();
            m_Console.RenderCommandLine();
        }

        public override bool HandleKeys(PanelKeyArguments args)
        {
            if (!base.HandleKeys(args))
                return m_Console.HandleKeys(args);
            else
                return true;
        }
    }
}
