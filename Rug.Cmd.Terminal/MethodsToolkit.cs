﻿/* 
 * Rug.Cmd.Terminal
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using System.Text;

namespace Rug.Cmd.Terminal
{
    public class MethodsToolkit
    {
        public static void Install(CommandSystem system)
        {
            ConsoleCommand MethodCommandInfo = new ConsoleCommand("METHOD", "Define a method", MethodCommand);
            MethodCommandInfo.Parser.FirstArgument = new StringArgument("Name", "The name of the method to define", "The name of the method to define");
            MethodCommandInfo.Parser.DefaultArgument = new StringListArgument("Arguemnts", "Any number of strings", "Any number of strings. These will be concatinated and used as the programs default arguments");

            system.Commands.Add(MethodCommandInfo.Name, MethodCommandInfo);

            ConsoleCommand CallCommandInfo = new ConsoleCommand("CALL", "Execute a method", CallCommand);
            CallCommandInfo.Parser.FirstArgument = new StringArgument("Name", "The name of the method to execute", "The name of the method to execute");
            CallCommandInfo.Parser.DefaultArgument = new StringListArgument("Arguemnts", "Any number of strings", "Any number of strings. These will be concatinated and used as the programs arguments");

            system.Commands.Add("CALL", CallCommandInfo);
        }

        public static void MethodCommand(CommandSystem system, ConsoleCommand command)
        {
            string assignTo = command.Parser.FirstArgument.ObjectValue.ToString();

            StringBuilder sb = new StringBuilder();

            foreach (string param in command.Parser.DefaultArgument.Value)
            {
                sb.Append(param);
            }

            string assignReplace = string.Format("[{0}]", assignTo);

            if (!system.Replacements.ContainsKey(assignReplace))
                system.Replacements.Add(assignReplace, sb.ToString());
            else
                system.Replacements[assignReplace] = sb.ToString(); 
        }

        public static void CallCommand(CommandSystem system, ConsoleCommand command)
        {
            string app = command.Parser.FirstArgument.ObjectValue.ToString();

            StringBuilder sb = new StringBuilder();

            foreach (string param in command.Parser.DefaultArgument.Value)
            {
                sb.Append("\"" + System.Environment.ExpandEnvironmentVariables(param) + "\" ");
            }

            try
            {
                //CmdHelper.ShellFile(System.Environment.ExpandEnvironmentVariables(app), sb.ToString(), true);
            }
            catch (Exception ex)
            {                
                RC.WriteException(003, ex);
                RC.WriteLine(ConsoleColorExt.Red, "Error running external app");
                RC.WriteLine(ConsoleColorExt.White, app + " " + sb.ToString());
            }
        }    
    }
}
