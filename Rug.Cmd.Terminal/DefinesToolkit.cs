﻿/* 
 * Rug.Cmd.Terminal
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using System.Text;

namespace Rug.Cmd.Terminal
{
    public static class DefinesToolkit
    {
        public static void Install(CommandSystem system)
        {
            ConsoleCommand defineCommandInfo = new ConsoleCommand("DEFINE", "Define a literal string to be used in conditional statements", DefineCommand);
            defineCommandInfo.Parser.FirstArgument = new StringArgument("Name", "The name of the define", "This is the literal string to be used in conditional statements like #IF");

            system.Commands.Add("DEFINE", defineCommandInfo);

            ConsoleCommand undefineCommandInfo = new ConsoleCommand("UNDEFINE", "Undefine a existing literal string", UndefineCommand);
            undefineCommandInfo.Parser.FirstArgument = new StringArgument("Name", "The name of the define", "This is the literal string to be undefined");

            system.Commands.Add("UNDEFINE", undefineCommandInfo);

            ConsoleCommand IfDefineCommandInfo = new ConsoleCommand("#IF", "'If' conditional statement", IfDefineCommand);
            IfDefineCommandInfo.Parser.DefaultArgument = new StringListArgument("Conditions", "Any number of defined literals seperated by AND/OR/NOT statements", "Any number of defined literals seperated by AND/OR/NOT statements. e.g. '#if Cmd or UI'\nMay also be followed by a THEN statment which will cause the remainder of the line to be parsed as a single command and no #IF section will be started.");

            system.Commands.Add("#IF", IfDefineCommandInfo);

            ConsoleCommand ElseDefineCommandInfo = new ConsoleCommand("#ELSE", "'else' conditional statement, to be used in conjunction with #IF", ElseDefineCommand);
            system.Commands.Add("#ELSE", ElseDefineCommandInfo);
            //system.Commands.Add("#ELSEIF", new ConsoleCommand(ElseIfDefineCommand));  

            ConsoleCommand EndIfDefineCommandInfo = new ConsoleCommand("#ENDIF", "Siginifies the end of a 'If' condtional section", EndIfDefineCommand);

            system.Commands.Add("#ENDIF", EndIfDefineCommandInfo);  
        }

        public static void DefineCommand(CommandSystem system, ConsoleCommand command)
        {
            string define = command.Parser.FirstArgument.ObjectValue.ToString();    
            
            system.Defines[define] = true; 
        }

        public static void UndefineCommand(CommandSystem system, ConsoleCommand command)
        {
            string define = command.Parser.FirstArgument.ObjectValue.ToString();  

            system.Defines[define] = false;
        }

        public static void IfDefineCommand(CommandSystem system, ConsoleCommand command)
        {
            string define = ""; //  @params[0];
            bool execThen = false; 
            int index = 0;

            string[] @params = command.Parser.DefaultArgument.Value.ToArray(); 
            //if (@params.Length > 1)
            //{
                StringBuilder sb = new StringBuilder();

                for (int e = @params.Length; index < e; index++)
                {
                    if (@params[index].Equals("THEN", StringComparison.CurrentCultureIgnoreCase))
                    {
                        execThen = true;
                        break; 
                    }
                    else if (@params[index].Equals("OR", StringComparison.CurrentCultureIgnoreCase))
                    {
                        sb.Append("|");
                    }
                    else if (@params[index].Equals("AND", StringComparison.CurrentCultureIgnoreCase))
                    {
                        sb.Append("&");
                    }
                    else if (@params[index].Equals("NOT", StringComparison.CurrentCultureIgnoreCase))
                    {
                        sb.Append("!");
                    }
                    else 
                        sb.Append(@params[index]);
                }

                define = sb.ToString();                
            //}

            if (execThen)
            {
                system.Defines.Push(define);

                if (!system.Defines.Ignore)
                {
                    sb = new StringBuilder();

                    bool first = true;

                    for (int i = index + 1, e = @params.Length; i < e; i++)
                    {
                        if (first)
                        {
                            sb.Append(@params[i] + " ");
                            first = false;
                        }
                        else
                            sb.Append(" \"" + @params[i] + "\"");
                    }

                    system.RunCommand(sb.ToString());
                }

                system.Defines.Pop();
            }
            else 
                system.Defines.Push(define);
        }

        public static void ElseDefineCommand(CommandSystem system, ConsoleCommand command)
        {
            string popped = Defines.GetOriginalCondition(system.Defines.Pop());

            string define = Defines.GetInverseCondition(popped) + "/" + popped; //  @params[0];
            
            system.Defines.Push(define);
        }

        /*
        public static void ElseIfDefineCommand(CommandSystem system, string[] @params)
        {
            string popped = Defines.GetOriginalCondition(system.Defines.Pop());

            if (@params.Length < 1)
            {
                RC.WriteError(002, "Too few parameters");
                return;
            }

            string define = ""; //  @params[0];
            bool execThen = false;
            int index = 0;

            //if (@params.Length > 1)
            //{
            StringBuilder sb = new StringBuilder();

            for (int e = @params.Length; index < e; index++)
            {
                if (@params[index].Equals("THEN", StringComparison.CurrentCultureIgnoreCase))
                {
                    execThen = true;
                    break;
                }
                else if (@params[index].Equals("OR", StringComparison.CurrentCultureIgnoreCase))
                {
                    sb.Append("|");
                }
                else if (@params[index].Equals("AND", StringComparison.CurrentCultureIgnoreCase))
                {
                    sb.Append("&");
                }
                else if (@params[index].Equals("NOT", StringComparison.CurrentCultureIgnoreCase))
                {
                    sb.Append("!");
                }
                else
                    sb.Append(@params[index]);
            }

            define = Defines.GetInverseCondition(popped) + "&" + sb.ToString() + "/" + popped;
            //}

            /*if (execThen)
            {
                system.Defines.Push(define);

                if (!system.Defines.Ignore)
                {
                    sb = new StringBuilder();

                    bool first = true;

                    for (int i = index + 1, e = @params.Length; i < e; i++)
                    {
                        if (first)
                        {
                            sb.Append(@params[i] + " ");
                            first = false;
                        }
                        else
                            sb.Append(" \"" + @params[i] + "\"");
                    }

                    system.RunCommand(sb.ToString());
                }

                system.Defines.Pop();

            }
            else* / 
                system.Defines.Push(define);
        }*/

        public static void EndIfDefineCommand(CommandSystem system, ConsoleCommand command)
        {
            system.Defines.Pop();
        }         
    }
}
