﻿/* 
 * Rug.Cmd.Terminal
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Rug.Cmd.AppPanels;

namespace Rug.Cmd.Terminal
{    
    public class CommandSystemConsole : CommandSystem, ITabable, IConsole
    {
        public event EventHandler ContentsChanged;
        public event EventHandler Exiting; 

        #region Members

        public int NumberOfConsoleLines = 30;
        public int NumberOfCommandStrings = 16;

		private ConsoleBuffer m_ConsoleBuffer = new ConsoleBuffer();

		//private List<string> m_ConsoleText = new List<string>(24);
		private List<string> m_CommandStrings = new List<string>(16);
		private int m_SelectedCommandString = -1;

		private string m_ActiveCommandString = "";
		private string m_ActiveCommandAutoComplete = "";
		private string m_ActiveCommandFormatted = "";

		private int m_Carrat = 0;
		private int m_ViewPoint = 0;
		private int m_Top = 0;		

		private long m_PageCarrat = 0;
		private long m_PageViewPoint = 0;
		private long m_LastBottomLine = 0; // this is the wrong name for this

		private bool m_ConsoleChanged = true;
		private bool m_CommandChanged = true;
		private bool m_InsertMode = false;

		private bool m_LinesAdded = false;
		private bool m_Exit = false;

		private bool m_IsSelectedTab = false;

		private bool m_IsCommandLineFloating = false;
		private int m_ContentBottom = 0;

		private char m_BackShade = ConsoleChars.GetShade(ConsoleShade.Clear);

		public char BackShade
		{
			get { return m_BackShade; }
			set { m_BackShade = value; }
		} 
		
        public bool Exit
        {
            get { return m_Exit; }        
        }

		public int Top { get { return m_Top; } }

		public int ContentBottom { get { return m_ContentBottom; } }

		public bool IsCommandLineFloating
		{
			get { return m_IsCommandLineFloating; }
			set { m_IsCommandLineFloating = value; }
		} 

        private void OnContentsChanged()
        {
			if (ContentsChanged != null)
			{
				ContentsChanged(this, EventArgs.Empty);
			}
        }

		//public ConsoleBuffer ConsoleBuffer { get { return m_ConsoleBuffer; } } 

        //ConsoleFormatter m_Formatter = new ConsoleFormatter();

        #endregion 

        #region ITabable Members

        public int TabIndex
        {
            get { return 0; }
        }

        public bool IsSelectedTab
        {
            get { return m_IsSelectedTab; }
            set { m_IsSelectedTab = value; }
        }

		public virtual bool AnyChange
		{
			get { return true; }
			set { ; }
		}

        #endregion

        #region Colour Properites

        private ConsoleColorExt m_BackgroundColor = ConsoleColorExt.Black;  

        public ConsoleColorExt BackgroundColor
        {
            get { return m_BackgroundColor; }
            set { m_BackgroundColor = value; }
        }

        private ConsoleColorExt m_ForegroundColor = ConsoleColorExt.Gray; 

        public ConsoleColorExt ForegroundColor
        {
            get { return m_ForegroundColor; }
            set { m_ForegroundColor = value; }
        }

        #endregion 

        #region Commands

        //public event CommandEvent CommandExecuted;

        //public Dictionary<string, ConsoleCommand> Commands = new Dictionary<string, ConsoleCommand>();

        public CommandSystemConsole()
        {            
            // [REC]ORD /start "Name" | /stop | /restart /delete 
            // Only one recording at a time
            //Commands.Add("REC", new ConsoleCommand(RecordCommand));
            //Commands.Add("RECORD", new ConsoleCommand(RecordCommand));

            // SAVE "Name" "Resource path" 
            //Commands.Add("SAVE", new ConsoleCommand(SaveCommand));          

            // LOAD "Name" "Resource path" 
            //Commands.Add("LOAD", new ConsoleCommand(LoadCommand));          

            // [EXEC]UTE "Path or Resource" 
            //Commands.Add("EXEC", new ConsoleCommand(ExecCommand));
            //Commands.Add("EXECUTE", new ConsoleCommand(ExecCommand));

            // [DEF]INE "Name" "Type" args... 
            //Commands.Add("DEF", new ConsoleCommand(DefineCommand));
            //Commands.Add("DEFINE", new ConsoleCommand(DefineCommand));

            // [UNDEF]INE "Name" 
            //Commands.Add("UNDEF", new ConsoleCommand(UndefineCommand));
            //Commands.Add("UNDEFINE", new ConsoleCommand(UndefineCommand));
            // SET "Name" "Value"
            //Commands.Add("SET", new ConsoleCommand(SetCommand));            
            // or 
            // @Name "Value"
            // the equals is implyed? 
            // CALL "App Path" "arguments"
            // [INV]OKE "Namespace.Type.Method" ("Name" "Value")* 
            //Commands.Add("COMMANDS", new ConsoleCommand(CommandsCommand));

            ConsoleCommand exitCommandInfo = new ConsoleCommand("EXIT", "Exit the console", ExitCommand);

            Commands.Add("EXIT", exitCommandInfo);

			m_ConsoleBuffer.BufferWidth = RC.Sys.BufferWidth - 3; 
			m_ConsoleBuffer.ContentsChanged += new EventHandler(m_ConsoleBuffer_ContentsChanged);
        }

		void m_ConsoleBuffer_ContentsChanged(object sender, EventArgs e)
		{
			int maxWidth = RC.App.BufferWidth;

			long top = m_ConsoleBuffer.TopLine, bottom = m_ConsoleBuffer.BottomLine;

			int lineCount = (int)(bottom - top);

			bool doScroll = m_PageCarrat == m_LastBottomLine; //  lineCount; 

			if (doScroll)
			{
				ScrollWithIntoView(bottom);
			}
			else
			{
				m_LinesAdded = true;
			}

            m_ConsoleChanged = true;

            OnContentsChanged();

			m_LastBottomLine = bottom;
		}

        public void Redraw()
        {
            m_CommandChanged = true;
            m_ConsoleChanged = true;

            //AnyChange(); 
        }

        public void CommandActiveChanged()
        {
            m_CommandChanged = true;

            //AnyChange();
        }

        #endregion 

        #region Setup

        public void Setup()
        {
			m_BackgroundColor = RC.Sys.BackgroundColor;
			m_ForegroundColor = RC.Sys.ForegroundColor; 

            m_Top = RC.Sys.CursorTop;

			for (int i = 0; i < NumberOfConsoleLines; i++)
			{
				RC.Sys.WriteLine();
			}

            RC.Sys.WriteLine();
        }

        #endregion 

        #region Render Methods

        public void RenderConsole()
        {
			if (!m_ConsoleChanged)
			{
				return;
			}

            RC.Sys.CursorLeft = 0;
            RC.Sys.CursorTop = m_Top;

            ConsoleColorState state = RC.Sys.ColorState;

            RC.Sys.BackgroundColor = m_BackgroundColor; 

            long start = m_PageViewPoint; // m_ConsoleText.Count - NumberOfConsoleLines;

			if (start < m_ConsoleBuffer.TopLine)
			{
				start = m_ConsoleBuffer.TopLine;
			}

            long end = start + NumberOfConsoleLines;
			int lineCount = (int)(m_ConsoleBuffer.BottomLine - m_ConsoleBuffer.TopLine);

			if (start + lineCount < end)
			{
				end = start + lineCount;
			}

			List<string> lines = m_ConsoleBuffer.GetLines(start, (int)(end - start));

			foreach (string line in lines)
			{
                RC.Sys.CursorLeft = 1;
				RC.Sys.Write(new string(m_BackShade, RC.Sys.BufferWidth - 2));
				RC.Sys.CursorLeft = 1;
//				int lineTotalWidth = RC.Sys.BufferWidth - (3 + ConsoleFormatter.StripFormat(line).Length); // - 3
				ConsoleFormatter.WriteInterpreted(RC.Sys, (m_BackShade + line));
				//RC.Sys.Write(ConsoleVerbosity.Silent, new string(' ', lineTotalWidth));
				RC.Sys.CursorTop++;
            }

			m_ContentBottom = RC.Sys.CursorTop - m_Top;

			if (m_IsCommandLineFloating == false || m_ConsoleCleared == true)
			{
				if (lineCount < NumberOfConsoleLines)
				{
					for (int i = 0, e = NumberOfConsoleLines - lineCount; i < e; i++)
					{
						RC.Sys.CursorLeft = 1;
						RC.Sys.WriteLine(new string(m_BackShade, RC.Sys.BufferWidth - 2));
					}
				}
			}

            int carrat = (int)(m_PageCarrat - start);

			if (m_PageCarrat < lineCount && carrat < NumberOfConsoleLines)
            {
                RC.Sys.CursorTop = m_Top + carrat;
                RC.Sys.CursorLeft = 1;

				if (m_LinesAdded)
				{
					RC.Sys.ForegroundThemeColor = ConsoleThemeColor.TextBad;
				}
				else
				{
					RC.Sys.ForegroundThemeColor = ConsoleThemeColor.SubText;
				}

				//RC.Sys.Write("" + ConsoleChars.GetShade(ConsoleShade.HalfLeft));
				RC.Sys.Write("[");

                RC.Sys.CursorLeft = RC.Sys.BufferWidth - 2;
                //RC.Sys.Write("" + ConsoleChars.GetShade(ConsoleShade.HalfRight));
				RC.Sys.Write("]");           
            }

            RC.Sys.ColorState = state;

			if (m_ConsoleCleared == true && m_IsCommandLineFloating == true)
			{

			}

			m_ConsoleCleared = false; 
            m_ConsoleChanged = false;
        }

        public void RenderCommandLine()
        {
            bool active = IsSelectedTab;

			if (m_CommandChanged)
			{
				RC.Sys.CursorLeft = 0;
			}

			int commandLineTop = 0; 

			if (m_IsCommandLineFloating == false)
			{
				commandLineTop = m_Top + NumberOfConsoleLines;
			}
			else
			{
				commandLineTop = m_Top + m_ContentBottom; 
			}

			RC.Sys.CursorTop = commandLineTop; 

            int maxWidth = RC.Sys.BufferWidth - 4;

            ConsoleColorState state = RC.Sys.ColorState;

            RC.Sys.BackgroundColor = BackgroundColor; 

            if (m_ActiveCommandString.Length > maxWidth)
            {
                int sub = m_ViewPoint;

                if (m_CommandChanged)
                {
                    RC.Sys.CursorLeft = 1;
					
					if (active == true)
					{
						//RC.Sys.Write(("> " + m_ActiveCommandString.Substring(sub, maxWidth)).PadRight(ConsoleExt.BufferWidth - 2, ' '));
						ConsoleFormatter.WriteInterpretedLine(RC.Sys, ("&gt;" + ConsoleFormatter.Substring(m_ActiveCommandFormatted, sub, maxWidth)) + "".PadRight(RC.Sys.BufferWidth - (3 + maxWidth), m_BackShade)); // ConsoleChars.GetShade(ConsoleShade.Dim)));
					}
					else
					{
						ConsoleFormatter.WriteInterpretedLine(RC.Sys, (m_BackShade + ConsoleFormatter.Substring(m_ActiveCommandFormatted, sub, maxWidth)) + "".PadRight(RC.Sys.BufferWidth - (3 + maxWidth), m_BackShade)); // ConsoleChars.GetShade(ConsoleShade.Dim)));
					}
                }

				RC.Sys.CursorTop = commandLineTop; 
                RC.Sys.CursorLeft = 2 + (m_Carrat - sub);

                if (m_InsertMode) 
                {
                    RC.Sys.ColorState = state.Inverse;

					if (m_Carrat < m_ActiveCommandString.Length)
					{
						RC.Sys.Write(m_ActiveCommandString.Substring(m_Carrat, 1));
					}
					else
					{
						RC.Sys.Write(new string(m_BackShade, 1));
					}

                    RC.Sys.CursorLeft = RC.Sys.CursorLeft - 1;
                }

            }
            else
            {
                if (m_CommandChanged)
                {
                    RC.Sys.CursorLeft = 1;

					if (active)
					{
						ConsoleFormatter.WriteInterpretedLine(RC.Sys, ("&gt;" + m_ActiveCommandFormatted) + "".PadRight(RC.Sys.BufferWidth - (3 + m_ActiveCommandString.Length), m_BackShade));
					}
					else
					{
						ConsoleFormatter.WriteInterpretedLine(RC.Sys, (m_BackShade + m_ActiveCommandFormatted) + "".PadRight(RC.Sys.BufferWidth - (3 + m_ActiveCommandString.Length), m_BackShade));
					}
                }

				RC.Sys.CursorTop = commandLineTop; 
                RC.Sys.CursorLeft = m_Carrat + 2;

                if (m_InsertMode)
                {
                    RC.Sys.ColorState = state.Inverse;

					if (m_Carrat < m_ActiveCommandString.Length)
					{
						RC.Sys.Write(m_ActiveCommandString.Substring(m_Carrat, 1));
					}
					else
					{
						RC.Sys.Write(new string(m_BackShade, 1));
					}

                    RC.Sys.CursorLeft = RC.Sys.CursorLeft - 1;
                }
            }

            RC.Sys.ColorState = state;

            m_CommandChanged = false;
        }

        #endregion

        #region Handle Keys

        public bool HandleKeys(PanelKeyArguments args)
        {
            bool handled = true; //  m_Exit;

			if (m_Exit)
			{
				args.Exit();
			}

            ConsoleKeyInfo info = args.Key;

			if (info.Key == ConsoleKey.Escape)
			{
				args.Exit();
			}
			else if (info.Key == ConsoleKey.Tab)
			{
				args.Tab(this, info.Modifiers == ConsoleModifiers.Shift);
			}
			else if (info.Key == ConsoleKey.Enter)
			{
				ExecuteCommand();
			}
			else if (info.Key == ConsoleKey.Delete)
			{
				HandleDelete(false);
			}
			else if (info.Key == ConsoleKey.Backspace)
			{
				HandleDelete(true);
			}
			else if (info.Key == ConsoleKey.LeftArrow)
			{
				MoveCarrat(-1);
			}
			else if (info.Key == ConsoleKey.RightArrow)
			{
				MoveCarrat(1);
			}
			else if (info.Key == ConsoleKey.Home)
			{
				MoveCarratToStart(); 
			}
			else if (info.Key == ConsoleKey.End)
			{
				MoveCarratToEnd(); 
			}
			else if (info.Key == ConsoleKey.UpArrow)
			{
				SelectCommandString(-1);
			}
			else if (info.Key == ConsoleKey.DownArrow)
			{
				SelectCommandString(1);
			}
			else if (info.Key == ConsoleKey.PageUp)
			{
				ScrollPage(-1);
			}
			else if (info.Key == ConsoleKey.PageDown)
			{
				ScrollPage(1);
			}
			else if (info.Key == ConsoleKey.Insert)
			{
				m_InsertMode = !m_InsertMode;
				m_CommandChanged = true;
				//AnyChange();
				Console.CursorVisible = !m_InsertMode;
			}
			else
			{
				HandleChar(info.KeyChar);
			}

            return handled; 
        }

		private void MoveCarratToStart()
		{
			MoveCarrat(-m_Carrat); 
		}

		private void MoveCarratToEnd()
		{		
			MoveCarrat(m_ActiveCommandString.Length - m_Carrat); 
		}

        private void MoveCarrat(int p)
        {
            m_Carrat += p;

			if (m_Carrat > m_ActiveCommandString.Length)
			{
				m_Carrat = m_ActiveCommandString.Length;
			}
			
			if (m_Carrat < 0)
			{
				m_Carrat = 0;
			}

			if (m_Carrat < m_ViewPoint)
			{
				m_ViewPoint = m_Carrat;
			}

            int maxWidth = RC.App.BufferWidth;

			if (m_ViewPoint < m_Carrat - maxWidth)
			{
				m_ViewPoint = m_Carrat - maxWidth;
			}

            m_CommandChanged = true;
            //AnyChange();
        }

        private void SelectCommandString(int p)
        {
			if (m_CommandStrings.Count == 0)
			{
				return;
			}

            m_SelectedCommandString += p;

			if (m_SelectedCommandString < 0)
			{
				m_SelectedCommandString = m_CommandStrings.Count - 1;
			}

			if (m_SelectedCommandString >= m_CommandStrings.Count)
			{
				m_SelectedCommandString = m_CommandStrings.Count - 1;
			}

            m_ActiveCommandString = m_CommandStrings[m_SelectedCommandString];
            m_Carrat = m_ActiveCommandString.Length;

			if (m_Carrat < m_ViewPoint)
			{
				m_ViewPoint = m_Carrat;
			}

            int maxWidth = RC.App.BufferWidth;

			if (m_ViewPoint < m_Carrat - maxWidth)
			{
				m_ViewPoint = m_Carrat - maxWidth;
			}

            m_ActiveCommandFormatted = ParsePartialCommand(m_ActiveCommandString, out m_ActiveCommandAutoComplete);

            m_CommandChanged = true;
            //AnyChange();
        }

        private void HandleDelete(bool backspace)
        {
            if (backspace == true)
            {
                if (m_ActiveCommandString.Length > 0)
                {
					if (m_Carrat == m_ActiveCommandString.Length)
					{
						m_ActiveCommandString = m_ActiveCommandString.Substring(0, m_ActiveCommandString.Length - 1);
					}
					else if (m_Carrat > 0)
					{
						m_ActiveCommandString = m_ActiveCommandString.Remove(m_Carrat - 1, 1);
					}

					if (m_Carrat > 0)
					{
						m_Carrat--;
					}

					if (m_Carrat < m_ViewPoint)
					{
						m_ViewPoint = m_Carrat;
					}
                }
            }
            else
            {
                if (m_ActiveCommandString.Length > 0)
                {
                    if (m_Carrat != m_ActiveCommandString.Length)
                    {
                        m_ActiveCommandString = m_ActiveCommandString.Remove(m_Carrat, 1);
                    }
                }
            }

            m_ActiveCommandFormatted = ParsePartialCommand(m_ActiveCommandString, out m_ActiveCommandAutoComplete);

            m_CommandChanged = true;
            //AnyChange();
        }

        private void HandleChar(char p)
        {
            if (m_InsertMode == false)
            {
				if (m_Carrat == m_ActiveCommandString.Length)
				{
					m_ActiveCommandString += p;
				}
				else
				{
					m_ActiveCommandString = m_ActiveCommandString.Insert(m_Carrat, "" + p);
				}
            }
            else
            {
				if (m_Carrat == m_ActiveCommandString.Length)
				{
					m_ActiveCommandString += p;
				}
				else
				{
					m_ActiveCommandString = m_ActiveCommandString.Remove(m_Carrat, 1).Insert(m_Carrat, "" + p);
				}
            }

            m_Carrat++;

            int maxWidth = RC.App.BufferWidth;

			if (m_ViewPoint < m_Carrat - maxWidth)
			{
				m_ViewPoint = m_Carrat - maxWidth;
			}

            m_ActiveCommandFormatted = ParsePartialCommand(m_ActiveCommandString, out m_ActiveCommandAutoComplete);

            m_CommandChanged = true;
            //AnyChange();
        }

        #endregion 

        #region Execute

        protected void ExecuteCommand()
        {
            ExecuteCommand(m_ActiveCommandString);
        }

        protected override void ExecuteCommand(string commandLine)
        {
            base.ExecuteCommand(commandLine);

            if (m_CommandStrings.Contains(commandLine))
            {
                m_CommandStrings.Remove(commandLine);
                m_CommandStrings.Add(commandLine);
            }
            else
            {
				if (m_CommandStrings.Count >= NumberOfCommandStrings)
				{
					m_CommandStrings.RemoveAt(0);
				}

                m_CommandStrings.Add(commandLine);
            }

            m_SelectedCommandString = -1;
            m_ActiveCommandString = "";
            m_ActiveCommandFormatted = "";//  ParsePartialCommand(m_ActiveCommandString, out m_ActiveCommandAutoComplete);

            m_Carrat = 0;
            m_ViewPoint = 0;
            m_CommandChanged = true;
            //AnyChange();
        }

        #endregion 

        #region Write Methods

		private void ScrollPage(int p)
		{
			long top = m_ConsoleBuffer.TopLine, bottom = m_ConsoleBuffer.BottomLine;
			int lineCount = (int)(bottom - top);

			m_PageCarrat += p;

			if (m_PageCarrat > bottom)
			{
				m_PageCarrat = bottom;
			}

			if (m_PageCarrat < top)
			{
				m_PageCarrat = top;
			}

			if (m_PageCarrat == bottom)
			{
				m_LinesAdded = false;
			}

			if (m_PageCarrat < m_PageViewPoint)
			{
				m_PageViewPoint = m_PageCarrat;
			}

			if (m_PageViewPoint < m_PageCarrat - NumberOfConsoleLines)
			{
				m_PageViewPoint = m_PageCarrat - NumberOfConsoleLines;
			}

			m_ConsoleChanged = true;
			//AnyChange();
		}

        public void ScrollWithIntoView(long line)
        {
			long top = m_ConsoleBuffer.TopLine, bottom = m_ConsoleBuffer.BottomLine;

			int lineCount = (int)(bottom - top);

			m_PageCarrat = line;

			if (m_PageCarrat > bottom)
			{
				m_PageCarrat = bottom;
			}

			if (m_PageCarrat < top)
			{
				m_PageCarrat = top;
			}

			if (m_PageCarrat == bottom)
			{
				m_LinesAdded = false;
			}

			if (m_PageCarrat < m_PageViewPoint)
			{
				m_PageViewPoint = m_PageCarrat;
			}

			if (m_PageViewPoint < m_PageCarrat - NumberOfConsoleLines)
			{
				m_PageViewPoint = m_PageCarrat - NumberOfConsoleLines;
			}

            m_ConsoleChanged = true;
            //AnyChange();
        }

		/* 
        public void Flush()
        {
            int maxWidth = RC.App.BufferWidth;
			
			int lineCount = (int)(m_ConsoleBuffer.BottomLine - m_ConsoleBuffer.TopLine);

			bool doScroll = m_PageCarrat == lineCount; 

			m_LineCountPosition
            m_ConsoleText.AddRange(ConsoleFormatter.SplitLines(m_Formatter.ToString(), maxWidth));

            m_Formatter.Clear();
               
            if (doScroll) 
                ScrollWithIntoView(m_ConsoleText.Count);
            else
                m_LinesAdded = true;

            m_ConsoleChanged = true;
            AnyChange();
        }
		*/ 
		/* 
        public void WriteFormmatted(string formatted)
        {
            Flush();
            int maxWidth = RC.App.BufferWidth;

            bool doScroll = m_PageCarrat == m_ConsoleText.Count; 

            m_ConsoleText.AddRange(ConsoleFormatter.SplitLines(formatted, maxWidth));

            if (doScroll)
                ScrollWithIntoView(m_ConsoleText.Count);
            else
                m_LinesAdded = true;

            m_ConsoleChanged = true;
            AnyChange();
        }
		*/ 

		/* 
        public void Write(ConsoleColorExt color, string str)
        {
            if (color == ConsoleColorExt.Inhreit)
                color = m_ForegroundColor; 

            m_Formatter.Write(color, str); 
        }
		*/

		/* 
		public void Write(string str)
		{
			m_Formatter.Write(m_ForegroundColor, str);
		}

		public void WriteLine(string str)
		{
			m_Formatter.WriteLine(m_ForegroundColor, str);

			Flush();
		}

		public void WriteLine(ConsoleColorExt color, string str)
		{
			if (color == ConsoleColorExt.Inhreit)
				color = m_ForegroundColor; 

			m_Formatter.WriteLine(color, str);

			Flush();
		}
		*/
		#endregion

		#region Commands
		// " \x22
        // ' \x27 

        bool m_First = false;
		private bool m_ConsoleCleared;	

        private string ReplaceArgumentWithColour(Match match)
        {
            string str;
            
            if (match.Groups["Quoted"].Success)
                str = ConsoleFormatter.Format(m_ConsoleBuffer.Theme[ConsoleThemeColor.SubText2], match.Value);
            else if (match.Groups["Word"].Success)
            {
                if (m_First)
					str = ConsoleFormatter.Format(m_ConsoleBuffer.Theme[ConsoleThemeColor.TitleText], match.Value);
                else
					str = ConsoleFormatter.Format(m_ConsoleBuffer.Theme[ConsoleThemeColor.SubText], match.Value);
            }
            else 
                str = match.Value;

            m_First = false;

            return str; 
        }

        private string ParsePartialCommand(string input, out string autoComplete)
        {
            autoComplete = input;                      
            m_First = true;

            return ArgumentStripperRegex.Replace(input, new MatchEvaluator(ReplaceArgumentWithColour));
        }

        private void ExitCommand(CommandSystem context, ConsoleCommand exitCommand)
        {
            m_Exit = true;

            if (Exiting != null)
                Exiting(this, EventArgs.Empty); 
        }

        #endregion

        #region Run Command 

        public override void RunCommand(string commandString)
        {
            string existing = m_ActiveCommandString; 

            m_ActiveCommandString = commandString;

            base.RunCommand(m_ActiveCommandString);

            m_ActiveCommandString = existing;
        }

        #endregion 
    
		#region IConsole Members

		string IConsole.ApplicationBuildReportPrefix
		{
			get
			{
				return m_ConsoleBuffer.ApplicationBuildReportPrefix;
			}
			set
			{
				m_ConsoleBuffer.ApplicationBuildReportPrefix = value;
			}
		}

		ConsoleVerbosity IConsole.Verbosity
		{
			get
			{
				return m_ConsoleBuffer.Verbosity; 
			}
			set
			{
				m_ConsoleBuffer.Verbosity = value;
			}
		}

		bool IConsole.DefaultPromptAnswer
		{
			get
			{
				return m_ConsoleBuffer.DefaultPromptAnswer;
			}
			set
			{
				m_ConsoleBuffer.DefaultPromptAnswer = value;
			}
		}

		bool IConsole.UseDefaultPromptAnswer
		{
			get
			{
				return m_ConsoleBuffer.UseDefaultPromptAnswer;
			}
			set
			{
				m_ConsoleBuffer.UseDefaultPromptAnswer = value;
			}
		}

		bool IConsole.WarningsAsErrors
		{
			get
			{
				return m_ConsoleBuffer.WarningsAsErrors;
			}
			set
			{
				m_ConsoleBuffer.WarningsAsErrors = value;
			}
		}

		bool IConsole.ReportWarnings
		{
			get
			{
				return m_ConsoleBuffer.ReportWarnings;
			}
			set
			{
				m_ConsoleBuffer.ReportWarnings = value;
			}
		}

		bool IConsole.IsBuildMode
		{
			get
			{
				return m_ConsoleBuffer.IsBuildMode;
			}
			set
			{
				m_ConsoleBuffer.IsBuildMode = value;
			}
		}

		bool IConsole.CanManipulateBuffer
		{
			get
			{
				return m_ConsoleBuffer.CanManipulateBuffer;
			}
		}

		int IConsole.BufferWidth
		{
			get
			{
				return m_ConsoleBuffer.BufferWidth;
			}
			set
			{
				m_ConsoleBuffer.BufferWidth = value;
			}
		}

		int IConsole.BufferHeight
		{
			get
			{
				return m_ConsoleBuffer.BufferHeight;
			}
			set
			{
				m_ConsoleBuffer.BufferHeight = value;
			}
		}

		int IConsole.CursorLeft
		{
			get
			{
				return m_ConsoleBuffer.CursorLeft;
			}
			set
			{
				m_ConsoleBuffer.CursorLeft = value;
			}
		}

		int IConsole.CursorTop
		{
			get
			{
				return m_ConsoleBuffer.CursorTop;
			}
			set
			{
				m_ConsoleBuffer.CursorTop = value;
			}
		}

		int IConsole.CursorSize
		{
			get
			{
				return m_ConsoleBuffer.CursorSize;
			}
			set
			{
				m_ConsoleBuffer.CursorSize = value;
			}
		}

		string IConsole.Title
		{
			get
			{
				return m_ConsoleBuffer.Title;
			}
			set
			{
				m_ConsoleBuffer.Title = value;
			}
		}

		Colors.ConsoleColorTheme IConsole.Theme
		{
			get
			{
				return m_ConsoleBuffer.Theme;
			}
			set
			{
				m_ConsoleBuffer.Theme = value;
			}
		}

		ConsoleColorExt IConsole.ForegroundColor
		{
			get
			{
				return m_ConsoleBuffer.ForegroundColor;
			}
			set
			{
				m_ConsoleBuffer.ForegroundColor = value;
			}
		}

		ConsoleColorExt IConsole.BackgroundColor
		{
			get
			{
				return m_ConsoleBuffer.BackgroundColor;
			}
			set
			{
				m_ConsoleBuffer.BackgroundColor = value;
			}
		}

		ConsoleThemeColor IConsole.ForegroundThemeColor
		{
			set
			{
				m_ConsoleBuffer.ForegroundThemeColor = value;
			}
		}

		ConsoleThemeColor IConsole.BackgroundThemeColor
		{
			set
			{
				m_ConsoleBuffer.BackgroundThemeColor = value;
			}
		}

		ConsoleColorState IConsole.ColorState
		{
			get
			{
				return m_ConsoleBuffer.ColorState;
			}
			set
			{
				m_ConsoleBuffer.ColorState = value;
			}
		}

		void IConsole.ResetColor()
		{
			m_ConsoleBuffer.ResetColor();
		}

		void IConsole.SetCursorPosition(int left, int top)
		{
			m_ConsoleBuffer.SetCursorPosition(left, top);
		}

		void IConsole.Clear()
		{
			m_ConsoleBuffer.Clear();
		}

		bool IConsole.KeyAvailable
		{
			get { return m_ConsoleBuffer.KeyAvailable; }
		}

		int IConsole.Read()
		{
			return m_ConsoleBuffer.Read();
		}

		ConsoleKeyInfo IConsole.ReadKey()
		{
			return m_ConsoleBuffer.ReadKey();
		}

		ConsoleKeyInfo IConsole.ReadKey(bool intercept)
		{
			return m_ConsoleBuffer.ReadKey(intercept);
		}

		string IConsole.ReadLine()
		{
			return m_ConsoleBuffer.ReadLine();
		}

		ConsoleKeyInfo IConsole.PromptForKey(string message, bool intercept, bool throwException)
		{
			return m_ConsoleBuffer.PromptForKey(message, intercept, throwException);
		}

		string IConsole.PromptForLine(string message, bool throwException)
		{
			return m_ConsoleBuffer.PromptForLine(message, throwException);
		}

		bool IConsole.ShouldWrite(ConsoleVerbosity verbosity)
		{
			return m_ConsoleBuffer.ShouldWrite(verbosity);
		}

		void IConsole.Write(string str)
		{
			m_ConsoleBuffer.Write(str);
		}

		void IConsole.Write(ConsoleVerbosity level, string str)
		{
			m_ConsoleBuffer.Write(level, str);
		}

		void IConsole.Write(ConsoleVerbosity level, ConsoleColorExt colour, string str)
		{
			m_ConsoleBuffer.Write(level, colour, str);
		}

		void IConsole.Write(ConsoleVerbosity level, ConsoleThemeColor colour, string str)
		{
			m_ConsoleBuffer.Write(level, colour, str);
		}

		void IConsole.Write(ConsoleColorExt colour, string str)
		{
			m_ConsoleBuffer.Write(colour, str);
		}

		void IConsole.Write(ConsoleThemeColor colour, string str)
		{
			m_ConsoleBuffer.Write(colour, str);
		}

		void IConsole.WriteLine(ConsoleVerbosity level)
		{
			m_ConsoleBuffer.WriteLine(level);
		}

		void IConsole.WriteLine()
		{
			m_ConsoleBuffer.WriteLine();
		}

		void IConsole.WriteLine(string str)
		{
			m_ConsoleBuffer.WriteLine(str);
		}

		void IConsole.WriteLine(ConsoleVerbosity level, string str)
		{
			m_ConsoleBuffer.WriteLine(level, str);
		}

		void IConsole.WriteLine(ConsoleVerbosity level, ConsoleColorExt colour, string str)
		{
			m_ConsoleBuffer.WriteLine(level, colour, str);
		}

		void IConsole.WriteLine(ConsoleColorExt colour, string str)
		{
			m_ConsoleBuffer.WriteLine(colour, str);
		}

		void IConsole.WriteLine(ConsoleVerbosity level, ConsoleThemeColor colour, string str)
		{
			m_ConsoleBuffer.WriteLine(level, colour, str);
		}

		void IConsole.WriteLine(ConsoleThemeColor colour, string str)
		{
			m_ConsoleBuffer.WriteLine(colour, str);
		}

		void IConsole.WriteWrapped(ConsoleColorExt colour, string message, int paddingLeft, int paddingRight)
		{
			m_ConsoleBuffer.WriteWrapped(colour, message, paddingLeft, paddingRight);
		}

		void IConsole.WriteWrapped(ConsoleThemeColor colour, string message, int paddingLeft, int paddingRight)
		{
			m_ConsoleBuffer.WriteWrapped(colour, message, paddingLeft, paddingRight);
		}

		void IConsole.WritePrompt(string str)
		{
			m_ConsoleBuffer.WritePrompt(str);
		}

		void IConsole.WritePrompt(ConsoleColorExt colour, string str)
		{
			m_ConsoleBuffer.WritePrompt(colour, str);
		}

		void IConsole.WritePrompt(ConsoleThemeColor colour, string str)
		{
			m_ConsoleBuffer.WritePrompt(colour, str);
		}

		void IConsole.WriteError(int id, string str)
		{
			m_ConsoleBuffer.WriteError(id, str);
		}

		void IConsole.WriteError(int id, string sourceFile, int line, int character, string str)
		{
			m_ConsoleBuffer.WriteError(id, sourceFile, line, character, str);
		}

		void IConsole.WriteError(ConsoleColorExt colour, int id, string str)
		{
			m_ConsoleBuffer.WriteError(colour, id, str);
		}

		void IConsole.WriteError(ConsoleThemeColor colour, int id, string str)
		{
			m_ConsoleBuffer.WriteError(colour, id, str);
		}

		void IConsole.WriteWarning(int id, string str)
		{
			m_ConsoleBuffer.WriteWarning(id, str);
		}

		void IConsole.WriteWarning(ConsoleColorExt colour, int id, string str)
		{
			m_ConsoleBuffer.WriteWarning(colour, id, str);
		}

		void IConsole.WriteWarning(ConsoleThemeColor colour, int id, string str)
		{
			m_ConsoleBuffer.WriteWarning(colour, id, str);
		}

		void IConsole.WriteWarning(ConsoleColorExt colour, int id, string sourceFile, int line, int character, string str)
		{
			m_ConsoleBuffer.WriteWarning(colour, id, sourceFile, line, character, str);
		}

		void IConsole.WriteWarning(ConsoleThemeColor colour, int id, string sourceFile, int line, int character, string str)
		{
			m_ConsoleBuffer.WriteWarning(colour, id, sourceFile, line, character, str);
		}

		void IConsole.WriteException(int id, Exception ex)
		{
			m_ConsoleBuffer.WriteException(id, ex);
		}

		void IConsole.WriteException(int id, string sourceFile, int line, int character, Exception ex)
		{
			m_ConsoleBuffer.WriteException(id, sourceFile, line, character, ex);
		}

		void IConsole.WriteException(int id, string title, Exception ex)
		{
			m_ConsoleBuffer.WriteException(id, title, ex);
		}

		void IConsole.WriteException(int id, string sourceFile, int line, int character, string title, Exception ex)
		{
			m_ConsoleBuffer.WriteException(id, title, ex);
		}

		void IConsole.WriteStackTrace(string trace)
		{
			m_ConsoleBuffer.WriteStackTrace(trace);
		}

		void IConsole.WriteMessage(ConsoleMessage type, ConsoleColorExt colour, string str)
		{
			m_ConsoleBuffer.WriteMessage(type, colour, str);
		}

		void IConsole.WriteMessage(ConsoleMessage type, ConsoleThemeColor colour, string str)
		{
			m_ConsoleBuffer.WriteMessage(type, colour, str);
		}

		void IConsole.WriteMessage(ConsoleMessage type, ConsoleColorExt colour, int errorId, string str)
		{
			m_ConsoleBuffer.WriteMessage(type, colour, errorId, str);
		}

		void IConsole.WriteMessage(ConsoleMessage type, ConsoleColorExt colour, int errorId, string sourceFile, int line, int character, string str)
		{
			m_ConsoleBuffer.WriteMessage(type, colour, errorId, sourceFile, line, character, str);
		}

		void IConsole.WriteMessage(ConsoleMessage type, ConsoleThemeColor colour, int errorId, string str)
		{
			m_ConsoleBuffer.WriteMessage(type, colour, errorId, str);
		}

		void IConsole.WriteMessage(ConsoleMessage type, ConsoleThemeColor colour, int errorId, string sourceFile, int line, int character, string str)
		{
			m_ConsoleBuffer.WriteMessage(type, colour, errorId, sourceFile, line, character, str);
		}

		void IConsole.WriteInterpreted(string buffer)
		{
			m_ConsoleBuffer.WriteInterpreted(buffer);
		}

		void IConsole.WriteInterpreted(string buffer, int paddingLeft, int paddingRight)
		{
			m_ConsoleBuffer.WriteInterpreted(buffer, paddingLeft, paddingRight);
		}

		void IConsole.WriteInterpreted(ConsoleColorExt colour, string buffer, int paddingLeft, int paddingRight)
		{
			m_ConsoleBuffer.WriteInterpreted(colour, buffer, paddingLeft, paddingRight);
		}

		void IConsole.WriteInterpreted(ConsoleThemeColor colour, string buffer, int paddingLeft, int paddingRight)
		{
			m_ConsoleBuffer.WriteInterpreted(colour, buffer, paddingLeft, paddingRight);
		}

		void IConsole.WriteInterpretedLine(string buffer)
		{
			m_ConsoleBuffer.WriteInterpretedLine(buffer);
		}

		#endregion

		public void Clear()
		{
			m_ConsoleBuffer.Clear();
			AnyChange = true;
			m_ConsoleChanged = true;
			//m_ConsoleCleared = true; 
			//RC.Sys.Clear(); 
			
		}
	}
}
