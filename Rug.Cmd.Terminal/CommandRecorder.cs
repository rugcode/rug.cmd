﻿/* 
 * Rug.Cmd.Terminal
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Rug.Cmd.Terminal
{
    public class CommandRecorder : IDisposable
    {
        CommandSystem m_Console;
        private bool m_Attached;

		FileStream m_Stream;		
		StreamWriter m_Writer;
		private string m_FilePath;

        public bool Attached
        {
            get { return m_Attached; }        
        }

        public CommandRecorder(CommandSystem console, string path)
        {
            m_Console = console;

			m_FilePath = path; 

			m_Stream = new FileStream(m_FilePath, FileMode.Create, FileAccess.Write);

			m_Writer = new StreamWriter(m_Stream);
        }

        public void Attach()
        {
            if (!m_Attached)
            {
                m_Console.CommandExecuted += new CommandEvent(m_Console_CommandExecuted);
                m_Attached = true;
            }
        }

        public void Detach()
        {
			if (m_Attached)
			{
				m_Console.CommandExecuted -= new CommandEvent(m_Console_CommandExecuted);
				m_Attached = false;
			}
        }

        public void Dispose()
        {
            if (m_Attached)
                Detach();

			m_Writer.Dispose();
			m_Stream.Close();
			m_Stream.Dispose();
        }


        void m_Console_CommandExecuted(string commandString)
        {
			m_Writer.WriteLine(commandString);
			m_Writer.Flush();
        }

    }
}
