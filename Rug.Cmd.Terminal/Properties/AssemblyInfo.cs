﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Rug.Cmd.Terminal")]
[assembly: AssemblyDescription("Rug.Cmd.Terminal (https://bitbucket.org/rugcode/rug.cmd/).")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Rugland Digital Systems")]
[assembly: AssemblyProduct("Rug.Cmd")]
[assembly: AssemblyCopyright("Copyright © Phill Tew 2008")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ed238441-be37-4d84-9cc9-19476a7b45da")]

[assembly: AssemblyVersion("1.3.1.*")]
[assembly: AssemblyFileVersion("1.3.1.0")]