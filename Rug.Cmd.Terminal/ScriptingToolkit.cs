﻿/* 
 * Rug.Cmd.Terminal
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System.IO;

namespace Rug.Cmd.Terminal
{
    public class ScriptingToolkit
    {
        public static void Install(CommandSystem system)
        {
            ConsoleCommand execCommand = new ConsoleCommand("EXEC", "Execute a script file", ExecCommand);
            execCommand.Parser.FirstArgument = new StringArgument("Script file path", "Path to the script file to execute", "Path to the script file to execute");
            system.Commands.Add("EXEC", execCommand);
        }

        public static void ExecCommand(CommandSystem system, ConsoleCommand execCommand)
        {
            Execute(system, execCommand.Parser.FirstArgument.ObjectValue.ToString()); 
        }

        public static void Execute(CommandSystem system, string pathToScript)
        {
            if (File.Exists(pathToScript))
            {
                string[] lines = File.ReadAllLines(pathToScript);

                foreach (string line in lines)
                {
                    string lineTrimmed = line.Trim();
                    if (lineTrimmed.Length > 0 &&
                        !lineTrimmed.StartsWith("--"))
                    {
                        if (lineTrimmed.StartsWith("#") ||
                            !system.Defines.Ignore)
                            system.RunCommand(line);
                    }
                }

                RC.WriteLine("Done"); 
            }
            else
                RC.WriteError(003, "File not found");
        }
    }
}
