﻿/* 
 * Rug.Cmd.Panels
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;

namespace Rug.Cmd.AppPanels
{
    public abstract class Panel : ITabable
    {
        public event EventHandler ContentsChanged; 

        public enum PanelBorderStyle { BackShade, Single, Double, Solid }

        private bool m_RedrawBorder;
            
        public bool RedrawBorder
        {
            get { return m_RedrawBorder; }
            set { m_RedrawBorder = value; }
        }

        private int m_TabIndex;

        public int TabIndex
        {
            get { return m_TabIndex; }
            set { m_TabIndex = value; }
        }

        private char m_BackShade = ConsoleChars.GetShade(ConsoleShade.Clear);

        public char BackShade
        {
            get { return m_BackShade; }
            set { m_BackShade = value; }
        }

        private ConsoleColorExt m_BackColor = ConsoleColorExt.DarkGray;

        public ConsoleColorExt BackColor
        {
            get { return m_BackColor; }
            set { m_BackColor = value; }
        }

		private ConsoleColorExt m_BorderColor = ConsoleColorExt.DarkGray;

		public ConsoleColorExt BorderColor
		{
			get { return m_BorderColor; }
			set { m_BorderColor = value; }
		}

        private PanelBorderStyle m_BorderStyle = PanelBorderStyle.Single; 

        public PanelBorderStyle BorderStyle
        {
            get { return m_BorderStyle; }
            set { m_BorderStyle = value; }
        }

		private bool m_InvertBorderColors = false;

		public bool InvertBorderColors
		{
			get { return m_InvertBorderColors; }
			set { m_InvertBorderColors = value; }
		} 

        private ConsoleColorExt m_SelectedForeColor = ConsoleColorExt.White;

        public ConsoleColorExt SelectedForeColor
        {
            get { return m_SelectedForeColor; }
            set { m_SelectedForeColor = value; }
        }

        private ConsoleColorExt m_SelectedBackColor = ConsoleColorExt.Gray;

        public ConsoleColorExt SelectedBackColor
        {
            get { return m_SelectedBackColor; }
            set { m_SelectedBackColor = value; }
        }

        private string m_Name;

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        private ITabable m_Parent;

        public ITabable Parent
        {
            get { return m_Parent; }
            set { m_Parent = value; }
        }

        private bool m_IsSelectedTab = false;

        public virtual bool IsSelectedTab
        {
            get { return m_IsSelectedTab; }
            set
            { 
                m_IsSelectedTab = value;
                RedrawBorder = true; 
            }
        }

        private bool m_AnyChange = true;   

        public virtual bool AnyChange
        {
            get { return m_AnyChange; }
            set 
            {
                m_AnyChange = value;

                if (value) ContentsHaveChanged();
            }
        }

        public void ContentsHaveChanged()
        {
            if (ContentsChanged != null)
                ContentsChanged(this, EventArgs.Empty); 
        }
            
        public virtual void Init(int Top, int PanelSize)
        {
            RedrawBorder = false;

			ConsoleColorState state = RC.Sys.ColorState; 

            RC.Sys.CursorTop = Top;
            RC.Sys.CursorLeft = 0;

            ConsoleColorExt fore = m_BackColor;
			ConsoleColorExt back = m_BorderColor; //  RC.Sys.BackgroundColor;
			ConsoleColorExt content = RC.Sys.BackgroundColor;

			if (m_InvertBorderColors == true)
			{
				fore = m_BorderColor;
				back = m_BackColor; 
			}

			if (m_IsSelectedTab == true)
			{
				if (m_InvertBorderColors == false)
				{
					fore = SelectedBackColor;
				}
				else
				{
					back = SelectedBackColor; 
				}
			}

            if (BorderStyle == PanelBorderStyle.BackShade)
            {
				RC.Sys.BackgroundColor = fore;
				RC.Sys.ForegroundColor = back;

				string lineString = new string(m_BackShade, RC.Sys.BufferWidth);

                for (int i = 0; i < PanelSize; i++)
                {
					RC.Sys.Write(back, lineString);
                }
            }
            else if ((BorderStyle == PanelBorderStyle.Single) ||
                    (BorderStyle == PanelBorderStyle.Double))
            {
                char[] chars = (BorderStyle == PanelBorderStyle.Single) ? ConsoleChars.SingleLines : ConsoleChars.DoubleLines;  

                string lineStringTop =
                    chars[(int)LineChars.TopLeft] +
                    "".PadRight(RC.Sys.BufferWidth - 2, chars[(int)LineChars.TopCenter]) +
                    chars[(int)LineChars.TopRight];

                string lineString =
                    "".PadRight(RC.Sys.BufferWidth - 2, m_BackShade);

                string lineStringBottom =
                    chars[(int)LineChars.BottomLeft] +
                    "".PadRight(RC.Sys.BufferWidth - 2, chars[(int)LineChars.BottomCenter]) +
                    chars[(int)LineChars.BottomRight];

                RC.Sys.BackgroundColor = fore;

                RC.Sys.Write(back, lineStringTop);

                RC.Sys.BackgroundColor = back;

                for (int i = 2; i < PanelSize; i++)
                {
                    RC.Sys.BackgroundColor = fore;
                    RC.Sys.Write(back, "" + chars[(int)LineChars.MidLeft]);
					RC.Sys.BackgroundColor = content;
                    RC.Sys.Write(fore, lineString);
                    RC.Sys.BackgroundColor = fore;
                    RC.Sys.Write(back, "" + chars[(int)LineChars.MidRight]);
                    RC.Sys.BackgroundColor = back;
                }

                RC.Sys.BackgroundColor = fore;

                RC.Sys.Write(back, lineStringBottom);
            }
            else 
            {
                char @char = ConsoleChars.GetShade(ConsoleShade.Opaque);

                string lineStringTop = "".PadRight(RC.Sys.BufferWidth, @char);

                string lineString = "".PadRight(RC.Sys.BufferWidth - 2, m_BackShade);

                string lineStringBottom = "".PadRight(RC.Sys.BufferWidth, @char);

                RC.Sys.BackgroundColor = fore;

                RC.Sys.Write(back, lineStringTop);

                RC.Sys.BackgroundColor = back;

                for (int i = 2; i < PanelSize; i++)
                {
                    RC.Sys.BackgroundColor = fore;
                    RC.Sys.Write(back, "" + @char);
					RC.Sys.BackgroundColor = content;
                    RC.Sys.Write(fore, lineString);
                    RC.Sys.BackgroundColor = fore;
                    RC.Sys.Write(back, "" + @char);
                    RC.Sys.BackgroundColor = back;
                }

                RC.Sys.BackgroundColor = fore;

                RC.Sys.Write(back, lineStringBottom);                
            }

			//RC.Sys.BackgroundColor = content;

			RC.Sys.ColorState = state;
        }

        public virtual bool HandleKeys(PanelKeyArguments args)
        {
            bool handled = false;

            ConsoleKeyInfo info = args.Key;

            if (info.Key == ConsoleKey.Tab)
            {
                if (this.m_Parent != null)
                    args.Tab(m_Parent, info.Modifiers == ConsoleModifiers.Shift);
                else
                    args.Tab(this, info.Modifiers == ConsoleModifiers.Shift);

                handled = true;
            }
            
            return handled; 
        }


        public abstract void Render(int Top, int PanelSize);       
    }
}
