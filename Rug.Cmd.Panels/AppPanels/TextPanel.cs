﻿/* 
 * Rug.Cmd.Panels
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

namespace Rug.Cmd.AppPanels
{
    public class TextPanel : ScrollingPanel<string>
    {        
        private string m_Text;

        public virtual string Text
        {
            get { return m_Text; }
            set
            {
                m_Text = value.TrimEnd();

                SetText(Text);
            }
        }

        protected virtual void SetText(string text)
        {
            Items.Clear();

            Items.AddRange(ConsoleFormatter.SplitLines(text, RC.Sys.BufferWidth - 6));

            AnyChange = true;
        }

        public override void Init(int Top, int PanelSize)
        {
            AnyChange = true;
            base.Init(Top, PanelSize);
        }

        protected override int SpaceItem(string Item, int Index, bool Selected, int Top, int Remaining)
        {
            return Top + 1;
        }

        protected override int RenderItem(string Item, int Index, bool Selected, int Top, int Remaining)
        {
            RC.Sys.CursorLeft = 3;
            RC.Sys.CursorTop = Top++;
            // RC.Sys.WriteInterpreted(Item, 0, 8);
            RC.Sys.Write(ConsoleFormatter.StripFormat(Item).PadRight(RC.Sys.BufferWidth - 6));
            return Top;
        }

        protected override int UpdateItem(string Item, int Index, bool Selected, int Top, int Remaining)
        {
            RC.Sys.CursorLeft = 3;
            RC.Sys.CursorTop = Top++;
            // RC.Sys.WriteInterpreted(Item, 0, 8);
            RC.Sys.Write(ConsoleFormatter.StripFormat(Item).PadRight(RC.Sys.BufferWidth - 6));
            return Top;
        }
    }
}
