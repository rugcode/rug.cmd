﻿/* 
 * Rug.Cmd.Panels
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using System.Threading;

namespace Rug.Cmd.AppPanels
{
    public abstract class PanelsBaseApp
    {
        private PanelController m_Controller;
        private TabControler m_Tabs;
        private PanelHeader m_Header;
		private ITabable m_Tab;

        private bool m_PanelChanged = false;
        private bool m_ShouldExit = false;
        private int m_ApplicationHeight = -1;
		private bool m_ShowHeader = true;

        public TabControler Tabs { get { return m_Tabs; } }
        public PanelController Controller { get { return m_Controller; } }
        public PanelHeader Header { get { return m_Header; } }

		public bool ShowHeader { get { return m_ShowHeader; } set { m_ShowHeader = value; } } 

        public int ApplicationHeight
        {
            get
            {
                if (m_ApplicationHeight == -1)
                    m_ApplicationHeight = Console.WindowHeight;

                return m_ApplicationHeight;
            }
            set { m_ApplicationHeight = value; }
        }

        public bool ShouldExit
        {
            get { return m_ShouldExit; }
            set { m_ShouldExit = value; }
        }

        public PanelsBaseApp() { }

        public abstract void CreatePanels();
        public abstract void OnStart();
        public abstract void OnExit();

        public virtual void Init()
        {
            try
            {
                Console.Clear();
                
                m_Tabs = new TabControler();
                m_Controller = new PanelController(ApplicationHeight - 2);
                Controller.ContentsChanged +=new EventHandler(Controller_ContentsChanged);
                Controller.TabIndex = 1;

                m_Header = new PanelHeader(Controller);
                
                Header.TabIndex = 0;
                Tabs.Add(Header);
                Header.IsSelectedTab = true;                
                Tabs.Add(Controller);

                CreatePanels();

				if (m_ShowHeader == true)
				{
					Header.Setup();
				}

                Controller.SelectedPanel = 0;
                Controller.Setup();

                bool exit = false;    

                PanelKeyArguments args = new PanelKeyArguments();

                m_Tab = Tabs.DefaultTab;

                OnStart();

                while (!exit)
                {
                    if (ShouldExit)
                    {
                        exit = true;
                        continue;
                    }

                    while (Console.KeyAvailable)
                    {
                        args.Key = Console.ReadKey(true);

                        bool handled = m_Tab.HandleKeys(args);

						if (args.ExitEvent)
						{
							exit = true;
						}
						else if (args.TabEvent && m_ShowHeader == true)
						{
							ITabable last = m_Tab;

							m_Tab = Tabs.NextTab(args.TabSource, args.ShiftTab);

							if (m_Tab == null)
							{
								m_Tab = last;
							}
						}
                    }

                    if (!exit)
                    {
						if (m_ShowHeader == true)
						{
							Header.Render();
						}

                        Controller.Render();

                        args.Reset();

						while (!m_PanelChanged && !Console.KeyAvailable)
						{
							Thread.CurrentThread.Join(100);
						}

                        m_PanelChanged = false; 
                    }
                }
            }
            catch (Exception ex)
            {
                RC.App.WriteException(1, ex);
            }
            finally
            {
                Console.Clear();

                OnExit(); 
            }
        }

		public void MoveNextTab()
		{
			ITabable last = m_Tab;

			m_Tab = Tabs.NextTab(m_Tab, false);

			if (m_Tab == null)
			{
				m_Tab = last;
			}
		}

		public void MovePreviousTab()
		{
			ITabable last = m_Tab;

			m_Tab = Tabs.NextTab(m_Tab, true);

			if (m_Tab == null)
			{
				m_Tab = last;
			}
		}

        void Controller_ContentsChanged(object sender, EventArgs e)
        {
            m_PanelChanged = true; 
        }
    }
}
