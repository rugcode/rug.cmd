﻿/* 
 * Rug.Cmd.Panels
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using System.Collections.Generic;

namespace Rug.Cmd.AppPanels
{
    public class PanelController : ITabable
    {
        public event EventHandler ContentsChanged; 

        // private TabControler m_TabControler; 
        private bool m_RedrawBackground = true; 
        private bool m_Changed = true; 
        private int m_Top;

        private int m_TabIndex = 0;

        public int TabIndex
        {
            get { return m_TabIndex; }
            set { m_TabIndex = value; }
        }

        private bool m_IsSelectedTab = false;

        public bool IsSelectedTab
        {
            get { return m_IsSelectedTab; }
            set 
            { 
                m_IsSelectedTab = value;
                m_RedrawBackground = true;
            }
        }

        private bool m_AnyChange = true;   

        public bool AnyChange
        {
            get { return m_AnyChange; }
            set { m_AnyChange = value; }
        }

        private List<Panel> m_Panels = new List<Panel>(8);

        public List<Panel> Panels
        {
            get { return m_Panels; }           
        }

        public void Add(Panel panel)
        {
            panel.Parent = this;
            panel.ContentsChanged += new EventHandler(panel_ContentsChanged);
            //m_TabControler.Add(panel);
            m_Panels.Add(panel);
        }

        void panel_ContentsChanged(object sender, EventArgs e)
        {
            if (m_Panels.Count == 0)
                return;
            
            if (m_SelectedPanel < 0)
                return;

            if (sender != m_Panels[m_SelectedPanel])
                return;
            
            if (ContentsChanged != null)
                ContentsChanged(sender, e); 
        }

        public void Remove(Panel panel)
        {
            if (m_Panels.Contains(panel))
            {
                panel.Parent = null;
                panel.ContentsChanged -= new EventHandler(panel_ContentsChanged);
                //m_TabControler.Removed(panel);
                m_Panels.Remove(panel);
            }
        }

        public bool Contains(Panel panel)
        {
            return m_Panels.Contains(panel); 
        }

        public bool MakeVisible(Panel panel)
        {
            if (Contains(panel))
            {
                int index = m_Panels.IndexOf(panel);

                if (index != SelectedPanel)
                    SelectedPanel = index; 

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool MakeVisible(int move)
        {
            int index = SelectedPanel + move; 
            int count = m_Panels.Count;

            while (index >= count) index -= count;
            while (index < 0) index += count;

            SelectedPanel = index;

            m_Panels[SelectedPanel].AnyChange = true; 

            return true; 
        }

        private int m_PanelSize;

        public int PanelSize
        {
            get { return m_PanelSize; }
        }

        private int m_SelectedPanel = -1;    

        public int SelectedPanel
        {
            get { return m_SelectedPanel; }
            set 
            { 
                m_SelectedPanel = value;
                m_Changed = true;
                m_RedrawBackground = true;
            }
        }

        public PanelController(int PanelSize)
        {
            //this.m_TabControler = TabControler; 
            this.m_PanelSize = PanelSize; 
        }

        public void Setup()
        {
            m_Top = RC.Sys.CursorTop;

			for (int i = 0; i < PanelSize; i++)
			{
				RC.Sys.WriteLine();
			}

            //foreach (Panel panel in m_Panels)
            //    panel.Init(); 
        }

        public bool HandleKeys(PanelKeyArguments args)
        {
			if (m_SelectedPanel == -1)
			{
				return false;
			}

            return m_Panels[m_SelectedPanel].HandleKeys(args);
        }

        public void Render()
        {         
            if (m_SelectedPanel == -1)
            {
                if (m_Changed || m_AnyChange)
                {
                    RC.Sys.CursorTop = m_Top;
                    RC.Sys.CursorLeft = 0;

					for (int i = 0; i < PanelSize; i++)
					{
						RC.Sys.WriteLine(" ".PadRight(RC.Sys.BufferWidth - 1, ConsoleChars.GetShade(ConsoleShade.Dim)));
					}

                    m_Changed = false;
                    m_AnyChange = false;
                }

            }
            else
            {
                Panel panel = m_Panels[m_SelectedPanel];

				if (panel.IsSelectedTab != IsSelectedTab)
				{
					panel.IsSelectedTab = IsSelectedTab;
				}
			
                if (m_RedrawBackground)
                {                    
                    panel.Init(m_Top, PanelSize);
                    m_RedrawBackground = false;
                }

                if (m_AnyChange)
                {
                    panel.AnyChange = true;
                    m_AnyChange = false; 
                }

                panel.Render(m_Top, PanelSize);
            }
        }
    }
}
