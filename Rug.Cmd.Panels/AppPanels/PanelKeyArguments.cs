﻿/* 
 * Rug.Cmd.Panels
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;

namespace Rug.Cmd.AppPanels
{
    public class PanelKeyArguments
    {
        private ConsoleKeyInfo m_Key;

        public ConsoleKeyInfo Key
        {
            get { return m_Key; }
            set { m_Key = value; }
        }

        private bool m_TabEvent = false;

        public bool TabEvent
        {
            get { return m_TabEvent; }
        }

        private bool m_ShiftTab = false;

        public bool ShiftTab
        {
            get { return m_ShiftTab; }
        }

        private ITabable m_TabSource = null;

        public ITabable TabSource
        {
            get { return m_TabSource; }        
        }

        private bool m_ExitEvent = false;

        public bool ExitEvent
        {
            get { return m_ExitEvent; }
        }

        public void Exit()
        {
            m_ExitEvent = true; 
        }

        public void Tab(ITabable TabSource, bool Shift)
        {
            m_TabEvent = true;
            m_TabSource = TabSource;
            m_ShiftTab = Shift; 
            
        }

        public void Reset()
        {
            m_TabEvent = false;
            m_ExitEvent = false;
            m_ShiftTab = false;
            m_TabSource = null;
            m_Key = default(ConsoleKeyInfo); 
        }
    }
}
