﻿/* 
 * Rug.Cmd.Panels
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using System.Collections.Generic;

namespace Rug.Cmd.AppPanels
{
    public abstract class ScrollingPanel<T> : Panel
    {
        private List<T> m_Items = new List<T>();

        public List<T> Items
        {
            get { return m_Items; }
            set { m_Items = value; }
        }

        private List<int> m_UpdatedIndexes = new List<int>(); 

        public List<int> UpdatedIndexes
        {
            get { return m_UpdatedIndexes; }
            set { m_UpdatedIndexes = value; }
        }

        private int m_SelectedIndex = 0;

        public virtual int SelectedIndex
        {
            get { return m_SelectedIndex; }
            set { m_SelectedIndex = value; }
        }

        private int m_TopIndex = 0;

        public int TopIndex
        {
            get { return m_TopIndex; }
            set { m_TopIndex = value; }
        }

        private int m_BottomIndex = int.MaxValue;

        public int BottomIndex
        {
            get { return m_BottomIndex; }
            set { m_BottomIndex = value; }
        }

/*        private bool m_AnyChange = true;

        public bool AnyChange
        {
            get { return m_AnyChange; }
            set { m_AnyChange = value; }
        }*/

        private bool m_ItemsChanged = false;    

        public bool ItemsChanged
        {
            get { return m_ItemsChanged; }
            set { m_ItemsChanged = value; }
        }

        private bool m_SelectionChanged = false;    

        public bool SelectionChanged
        {
            get { return m_SelectionChanged; }
            set { m_SelectionChanged = value; }
        }

        public virtual void UpdateItem(T Item)
        {
            if (m_Items.Contains(Item))
            {
                int index = m_Items.IndexOf(Item);

                m_ItemsChanged = true;

				if (!m_UpdatedIndexes.Contains(index))
				{
					m_UpdatedIndexes.Add(index);
				}

                ContentsHaveChanged();
            }
            else
                AnyChange = true; 
        }

        public override bool HandleKeys(PanelKeyArguments args)
        {
            bool handled = false;

            ConsoleKeyInfo info = args.Key;

            if (base.HandleKeys(args))
            {                
                handled = true; 
            }
            else if (info.Key == ConsoleKey.UpArrow)
            {
                /*if (m_SelectedIndex == 0)
                {
                    if (this.Parent != null)
                        args.Tab(this.Parent, true); 
                    else
                        args.Tab(this, true); 
                }
                else*/ 
                {
                    ScrollPage(-1);
                    handled = true;
                }
            }
            else if (info.Key == ConsoleKey.DownArrow)
            {
                ScrollPage(1);
                handled = true;
            }

            return handled; 
        }

        private void ScrollPage(int move)
        {
            int oldIndex = m_SelectedIndex;
            int oldTop = m_TopIndex;

            int newIndex = m_SelectedIndex + move;


			if (newIndex >= this.m_Items.Count)
			{
				newIndex = this.m_Items.Count - 1;
			}

			if (newIndex < 0)
			{
				newIndex = 0;
			}

			if (newIndex < m_TopIndex)
			{
				m_TopIndex = newIndex;
			}

			if (m_TopIndex < newIndex - ((m_BottomIndex - 1) - m_TopIndex))
			{
				m_TopIndex = newIndex - ((m_BottomIndex - 1) - m_TopIndex);
			}

            if (oldTop == m_TopIndex)
            {
                if (oldIndex != newIndex)
                {
                    //m_UpdatedIndexes.Add(oldIndex);

                    int top = move > 0 ? oldIndex : oldIndex + move;
                    int bottom = move > 0 ? oldIndex + move : oldIndex;

                    for (; top <= bottom; top++)
                    {
                        m_UpdatedIndexes.Add(top);
                        m_ItemsChanged = true;
                        m_SelectionChanged = true;

                        SelectedIndex = newIndex; 
                    }
                }
            }
            else 
            {
                if (oldIndex != newIndex)
                {
                    m_SelectionChanged = true;

                    SelectedIndex = newIndex;
                }

                AnyChange = true;
            }
        }


        public override void Render(int Top, int PanelSize)
        {
            if (AnyChange)
            {
                AnyChange = false;

                RenderEntierPanel(Top, PanelSize);         
            }
            else if (ItemsChanged)
            {
                m_ItemsChanged = false;

                RenderJustUpdated(Top, PanelSize);
            }
        }

        private void RenderEntierPanel(int Top, int PanelSize)
        {
            int line = Top + 1;
            int maxLines = Top + (PanelSize - 1);

            for (int i = TopIndex, e = m_Items.Count; i < e; i++)
            {
                T item = m_Items[i];

				if (line >= maxLines)
				{
					break;
				}

                line = RenderItem(item, i, i == m_SelectedIndex, line, maxLines - line);

                m_BottomIndex = i;
            }

			RC.Sys.BackgroundColor = BackColor; // ConsoleColorExt.Black;

            if (line < maxLines)
            {
                m_BottomIndex = int.MaxValue;

                while (line < maxLines)
                {
                    RC.Sys.CursorLeft = 2;
                    RC.Sys.CursorTop = line++;
                    RC.Sys.Write(BackColor, "".PadRight(RC.Sys.BufferWidth - 4, ' '));
                }
            }
        }

        private void RenderJustUpdated(int Top, int PanelSize)
        {
			if (m_UpdatedIndexes.Count == 0)
			{
				return;
			}

            m_UpdatedIndexes.Sort();

            int line = Top + 1;
            int maxLines = Top + (PanelSize - 1);

            for (int i = TopIndex, e = m_Items.Count; i < e; i++)
            {
				if (m_UpdatedIndexes.Count == 0)
				{
					return;
				}

                T item = m_Items[i];

				if (line >= maxLines)
				{
					break;
				}

				if (m_UpdatedIndexes.Contains(i))
				{
					if (m_SelectionChanged)
					{
						line = RenderItem(item, i, i == m_SelectedIndex, line, maxLines - line);
					}
					else
					{
						line = UpdateItem(item, i, i == m_SelectedIndex, line, maxLines - line);
					}

					m_UpdatedIndexes.Remove(i);
				}
				else
				{
					//line = RenderItem(item, i, i == m_SelectedIndex, line, maxLines - line);
					line = SpaceItem(item, i, i == m_SelectedIndex, line, maxLines - line);
				}
            }

            m_UpdatedIndexes.Clear();
            m_ItemsChanged = false;
            m_SelectionChanged = false; 
        }

        protected abstract int SpaceItem(T Item, int Index, bool Selected, int Top, int Remaining); 
        protected abstract int RenderItem(T Item, int Index, bool Selected, int Top, int Remaining);
        protected abstract int UpdateItem(T Item, int Index, bool Selected, int Top, int Remaining); 
    }
}
