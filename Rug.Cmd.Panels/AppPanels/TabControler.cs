﻿/* 
 * Rug.Cmd.Panels
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System.Collections.Generic;

namespace Rug.Cmd.AppPanels
{
    public class TabControler
    {
        private bool m_Sorted = false; 
        private List<ITabable> m_Tabs = new List<ITabable>();
        private static TabComp m_SortComp = new TabComp();

        public TabControler()
        {
            
        }

        public void Add(ITabable tab)
        {
            m_Tabs.Add(tab); 
            m_Sorted = false; 
        }

        public void Removed(ITabable tab)
        {
            m_Tabs.Remove(tab);
            m_Sorted = false; 
        }

        private void SortIfNeeded()
        {
            if (!m_Sorted)
            {
                m_Tabs.Sort(m_SortComp);
                m_Sorted = true;
            }
        }

        public ITabable DefaultTab
        {
            get
            {
                SortIfNeeded();

                return m_Tabs[0];
            }
        }

        public ITabable NextTab(ITabable current, bool shift)
        {
            SortIfNeeded();
            
            if (m_Tabs.Contains(current))
            {
                int index = m_Tabs.IndexOf(current); 

                if (index == 0 && shift) 
                {
                    index = m_Tabs.Count - 1;
                }
                else if (index == m_Tabs.Count - 1 && !shift) 
                {
                    index = 0; 
                }
                else 
                {
                    if (shift) 
                        index--;
                    else 
                        index++; 
                }

				current.AnyChange = true; 
                current.IsSelectedTab = false;
                m_Tabs[index].IsSelectedTab = true;
				m_Tabs[index].AnyChange = true; 

                return m_Tabs[index]; 
            }

            return current; 
        }

        private class TabComp : IComparer<ITabable>
        {

            #region IComparer<ITabable> Members

            public int Compare(ITabable x, ITabable y)
            {
                return x.TabIndex - y.TabIndex; 
            }

            #endregion
        }
    }
}
