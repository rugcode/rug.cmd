﻿/* 
 * Rug.Cmd.Panels
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;

namespace Rug.Cmd.AppPanels
{
    public class PanelHeader : ITabable
    {
        public event EventHandler ContentsChanged;

        private int m_Top;
        private int m_LastSelected = -1;

        private PanelController m_Controler;

        public PanelController Controler
        {
            get { return m_Controler; }            
        }

        private char m_BackShade = ConsoleChars.GetShade(ConsoleShade.Clear);

        public char BackShade
        {
            get { return m_BackShade; }
            set { m_BackShade = value; }
        }

        private ConsoleColorExt m_BackColor = ConsoleColorExt.DarkGray;

        public ConsoleColorExt BackColor
        {
            get { return m_BackColor; }
            set { m_BackColor = value; }
        }

        private ConsoleColorExt m_ForeColor = ConsoleColorExt.Black;

        public ConsoleColorExt ForeColor
        {
            get { return m_ForeColor; }
            set { m_ForeColor = value; }
        }

        private ConsoleColorExt m_SelectedForeColor = ConsoleColorExt.Black;

        public ConsoleColorExt SelectedForeColor
        {
            get { return m_SelectedForeColor; }
            set { m_SelectedForeColor = value; }
        }

        private ConsoleColorExt m_SelectedBackColor = ConsoleColorExt.Gray;

        public ConsoleColorExt SelectedBackColor
        {
            get { return m_SelectedBackColor; }
            set { m_SelectedBackColor = value; }
        }

		private ConsoleColorExt m_ActiveBackColor = ConsoleColorExt.DarkGray;

		public ConsoleColorExt ActiveBackColor
		{
			get { return m_ActiveBackColor; }
			set { m_ActiveBackColor = value; }
		}

		private ConsoleColorExt m_ActiveForeColor = ConsoleColorExt.Black;

		public ConsoleColorExt ActiveForeColor
		{
			get { return m_ActiveForeColor; }
			set { m_ActiveForeColor = value; }
		}

        private bool m_Changed = true; 

        public bool Changed
        {
            get { return m_Changed; }
            set { m_Changed = value; }
        }

		public virtual bool AnyChange
		{
			get { return m_Changed; }
			set { m_Changed = value; } 
		}

        private bool m_IsSelectedTab = false;   

        public bool IsSelectedTab
        {
            get { return m_IsSelectedTab; }
            set 
            {
                m_IsSelectedTab = value;
                m_Changed = true; 
            }
        }

        public PanelHeader(PanelController controler)
        {
            m_Controler = controler; 
        }

        public void Setup()
        {
            m_Top = RC.Sys.CursorTop;
            RC.Sys.WriteLine(); 
        }

        public void Render()
        {
            if (!m_Changed && m_LastSelected == m_Controler.SelectedPanel)
                return;

            m_Changed = false; 
            m_LastSelected = m_Controler.SelectedPanel;

            RC.Sys.CursorTop = m_Top;
            RC.Sys.CursorLeft = 0;			

            ConsoleColorState state = RC.Sys.ColorState;

            Panel active = null; 
            
            if (m_Controler.Panels.Count > 0)
                active = m_Controler.Panels[m_LastSelected];

			RC.Sys.BackgroundColor = m_BackColor;
			RC.Sys.ForegroundColor = m_ForeColor;

			RC.Sys.Write(new string(m_BackShade, 3)); 

            foreach (Panel panel in m_Controler.Panels)
            {
                if (panel == active)
                {
                    if (IsSelectedTab)
                    {
                        RC.Sys.BackgroundColor = m_SelectedBackColor;
                        RC.Sys.ForegroundColor = m_SelectedForeColor;                        
                    }
                    else 
                    {
						RC.Sys.BackgroundColor = m_ActiveBackColor;
						RC.Sys.ForegroundColor = m_ActiveForeColor;
                    }

                    //RC.Sys.Write(ConsoleChars.GetShade(ConsoleShade.HalfLeft) + panel.Name + ConsoleChars.GetShade(ConsoleShade.HalfRight));
					RC.Sys.Write(" " + panel.Name + " "); 
                }
                else
                {
                    RC.Sys.BackgroundColor = m_ForeColor;
                    RC.Sys.ForegroundColor = m_BackColor;
					RC.Sys.Write(m_BackShade + panel.Name + m_BackShade); 
                }

				RC.Sys.BackgroundColor = m_BackColor;
				RC.Sys.ForegroundColor = m_ForeColor;

				RC.Sys.Write(new string(m_BackShade, 1)); 
            }

			RC.Sys.BackgroundColor = m_BackColor;
			RC.Sys.ForegroundColor = m_ForeColor;

			RC.Sys.Write(new string(m_BackShade, RC.Sys.BufferWidth - RC.Sys.CursorLeft)); 

            RC.Sys.ColorState = state; 
        }

        #region ITabable Members

        private int m_TabIndex = 0;

        public int TabIndex
        {
            get { return m_TabIndex; }
            set { m_TabIndex = value; }
        }

        public bool HandleKeys(PanelKeyArguments args)
        {
            bool handled = false;

            ConsoleKeyInfo info = args.Key;

            if (info.Key == ConsoleKey.Tab)
            {
                args.Tab(this, info.Modifiers == ConsoleModifiers.Shift);
                handled = true;
            }
            /*else if (info.Key == ConsoleKey.DownArrow)
            {
                args.Tab(this, false);
                handled = true;
            }*/ 
            else if (info.Key == ConsoleKey.LeftArrow)
            {
                m_Changed = true; 
                Controler.MakeVisible(-1);
                handled = true;
            }
            else if (info.Key == ConsoleKey.RightArrow)
            {
                m_Changed = true; 
                Controler.MakeVisible(1);
                handled = true;
            }
            
            return handled;
        }

        #endregion
	}
}
