﻿/* 
 * Rug.Cmd.Panels
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using System.Collections.Generic;

namespace Rug.Cmd.Gui
{
    public class ConsoleControlCollection : IEnumerable<ConsoleControl>
    {
        private List<ConsoleControl> m_Inner = new List<ConsoleControl>();

        private ConsoleContainer m_Ower;

        internal ConsoleControlCollection(ConsoleContainer container)
        {
            if (container == null)
                throw new ArgumentNullException("container");

            m_Ower = container; 
        }

        public bool Contians(ConsoleControl control)
        {
            return m_Inner.Contains(control); 
        }

        public void Add(ConsoleControl control)
        {
            if (control.m_Parent != m_Ower && control.m_Parent != null)
                control.m_Parent.Controls.Remove(control); 

            if (!m_Inner.Contains(control))
                m_Inner.Add(control);

            control.m_Parent = m_Ower;
            control.CalculateClientLocation(); 
        }

        public void Remove(ConsoleControl control)
        {
            if (m_Inner.Contains(control))
            {
                m_Inner.Remove(control);
                control.m_Parent = null; 
            }
        }

        public void Clear()
        {
            foreach (ConsoleControl ctrl in m_Inner)
            {
                if (ctrl is ConsoleContainer)
                    (ctrl as ConsoleContainer).Controls.Clear(); 

                ctrl.m_Parent = null; 
            }

            m_Inner.Clear(); 
        }

        public ConsoleControl this[int index]
        {
            get { return m_Inner[index]; } 
        }

        #region IEnumerable<ConsoleControl> Members

        public IEnumerator<ConsoleControl> GetEnumerator()
        {
            return (IEnumerator<ConsoleControl>)m_Inner.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return (m_Inner as System.Collections.IEnumerable).GetEnumerator(); 
        }

        #endregion
    }
}
