﻿/* 
 * Rug.Cmd.Panels
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using System.Collections.Generic;

namespace Rug.Cmd.Gui
{    
    public static class ConsoleBoxHelper
    {       
        #region Simple Draw Box

        public static void DrawBox(int x, int y, int width, int height,
                            ConsoleDrawMode drawMode, ConsoleLineStyle lineStyle, bool invertColours)
        {
            if (invertColours)
                DrawBox(x, y, width, height, RC.ForegroundColor, RC.BackgroundColor, drawMode, lineStyle, true, ConsoleShade.Dim);
            else
                DrawBox(x, y, width, height, RC.BackgroundColor, RC.ForegroundColor, drawMode, lineStyle, true, ConsoleShade.Dim);                
        }

        public static void DrawBox(int x, int y, int width, int height,
                            ConsoleColorExt boxBackColour, ConsoleColorExt boxForeColour,
                            ConsoleDrawMode drawMode, ConsoleLineStyle lineStyle,
                            bool hasShadow, ConsoleShade shadowStyle)
        {
            DrawBox(RC.App, x, y, width, height,
                    boxBackColour, boxForeColour,
                    drawMode, lineStyle,
                    hasShadow, shadowStyle);
        }

        public static void DrawBox(IConsole console, int x, int y, int width, int height,
                                    ConsoleColorExt boxBackColour, ConsoleColorExt boxForeColour,
                                    ConsoleDrawMode drawMode, ConsoleLineStyle lineStyle, 
                                    bool hasShadow, ConsoleShade shadowStyle)
        {
            ConsoleColorState state = console.ColorState;

            try
            {
                char shade = ConsoleChars.GetShade(shadowStyle);

                char[] charSet = lineStyle == ConsoleLineStyle.Double ? ConsoleChars.DoubleLines : ConsoleChars.SingleLines;

                string empty = new string(charSet[4], width);

                switch (drawMode)
                {
                    case ConsoleDrawMode.Overlay:
                        console.SetCursorPosition(x, y);

                        console.BackgroundColor = boxBackColour;
                        console.ForegroundColor = boxForeColour;

                        console.Write(string.Format("{0}{1}{2}", charSet[0], new string(charSet[1], width), charSet[2]));

                        for (int i = 0; i < height; i++)
                        {
                            console.SetCursorPosition(x, y + i + 1);
                            console.Write(string.Format("{0}{1}{2}{3}", charSet[3], empty, charSet[5], shade));
                        }

                        console.SetCursorPosition(x, y + height + 1);

                        console.Write(string.Format("{0}{1}{2}{3}", charSet[6], new string(charSet[7], width), charSet[8], shade));

                        console.SetCursorPosition(x + 1, y + height + 2);

                        console.Write(string.Format("{0}", new string(shade, width + 2)));

                        console.ColorState = state;

                        break;
                    case ConsoleDrawMode.Replace:
                        for (int i = 0; i < y; i++)
                            console.WriteLine();

                        string prefix = new string(' ', x);

                        console.ColorState = state;

                        console.Write(prefix);

                        console.BackgroundColor = boxBackColour;
                        console.ForegroundColor = boxForeColour;

                        console.WriteLine(string.Format("{0}{1}{2}", charSet[0], new string(charSet[1], width), charSet[2]));

                        for (int i = 0; i < height; i++)
                        {
                            console.ColorState = state;

                            console.Write(prefix);

                            console.BackgroundColor = boxBackColour;
                            console.ForegroundColor = boxForeColour;

                            console.WriteLine(string.Format("{0}{1}{2}{3}", charSet[3], empty, charSet[5], shade));
                        }

                        console.ColorState = state;

                        console.Write(prefix);

                        console.BackgroundColor = boxBackColour;
                        console.ForegroundColor = boxForeColour;

                        console.WriteLine(string.Format("{0}{1}{2}{3}", charSet[6], new string(charSet[7], width), charSet[8], shade));

                        console.ColorState = state;

                        console.Write(prefix + " ");

                        console.BackgroundColor = boxBackColour;
                        console.ForegroundColor = boxForeColour;

                        console.WriteLine(string.Format("{0}", new string(shade, width + 2)));

                        break;
                    default:
                        break;

                }
            }
            catch
            {

            }
            finally
            {
                console.ColorState = state; 
            }
        }

        #endregion 

        #region Message Boxes 

        public static void ErrorBox(string title, string message, int yPosition, ConsoleDrawMode drawMode)
        {
            ConsoleBoxHelper.MessageBox(RC.App, title, message, yPosition,
                             ConsoleColorExt.Black, ConsoleColorExt.Red,
                             ConsoleColorExt.DarkRed,
                             ConsoleColorExt.Black, ConsoleColorExt.White, drawMode);
        }

        public static void ErrorBox(IConsole console, string title, string message, int yPosition, ConsoleDrawMode drawMode)
        {
            ConsoleBoxHelper.MessageBox(console, title, message, yPosition,
                             ConsoleColorExt.Black, ConsoleColorExt.Red,
                             ConsoleColorExt.DarkRed,
                             ConsoleColorExt.Black, ConsoleColorExt.White, drawMode);
        }

        public static void ErrorBox(string message, int yPosition, ConsoleDrawMode drawMode)
        {
            ConsoleBoxHelper.MessageBox(RC.App, Panels.Strings.ConsoleBoxHelper_Error, message, yPosition,
                             ConsoleColorExt.Black, ConsoleColorExt.Red,
                             ConsoleColorExt.DarkRed,
                             ConsoleColorExt.Black, ConsoleColorExt.White, drawMode);
        }

        public static void ErrorBox(IConsole console, string message, int yPosition, ConsoleDrawMode drawMode)
        {
            ConsoleBoxHelper.MessageBox(console, Panels.Strings.ConsoleBoxHelper_Error, message, yPosition,
                             ConsoleColorExt.Black, ConsoleColorExt.Red,
                             ConsoleColorExt.DarkRed,
                             ConsoleColorExt.Black, ConsoleColorExt.White, drawMode);
        }

        public static void ErrorBox(string title, string message, ConsoleDrawMode drawMode)
        {
            ConsoleBoxHelper.MessageBox(RC.App, title, message, 0,
                             ConsoleColorExt.Black, ConsoleColorExt.Red,
                             ConsoleColorExt.DarkRed,
                             ConsoleColorExt.Black, ConsoleColorExt.White, drawMode);
        }

        public static void ErrorBox(IConsole console, string title, string message, ConsoleDrawMode drawMode)
        {
            ConsoleBoxHelper.MessageBox(console, title, message, 0,
                             ConsoleColorExt.Black, ConsoleColorExt.Red,
                             ConsoleColorExt.DarkRed,
                             ConsoleColorExt.Black, ConsoleColorExt.White, drawMode);
        }

        public static void ErrorBox(string message, ConsoleDrawMode drawMode)
        {
            ConsoleBoxHelper.MessageBox(RC.App, Panels.Strings.ConsoleBoxHelper_Error, message, 0,
                             ConsoleColorExt.Black, ConsoleColorExt.Red,
                             ConsoleColorExt.DarkRed,
                             ConsoleColorExt.Black, ConsoleColorExt.White, drawMode);
        }

        public static void ErrorBox(IConsole console, string message, ConsoleDrawMode drawMode)
        {
            ConsoleBoxHelper.MessageBox(console, Panels.Strings.ConsoleBoxHelper_Error, message, 0,
                             ConsoleColorExt.Black, ConsoleColorExt.Red,
                             ConsoleColorExt.DarkRed,
                             ConsoleColorExt.Black, ConsoleColorExt.White, drawMode);
        }

        public static void MessageBox(string message, ConsoleColorExt boxBackColour, ConsoleColorExt boxForeColour, ConsoleDrawMode drawMode)
        {
            MessageBox(RC.App, null, message, 1, boxBackColour, boxForeColour, boxForeColour, boxBackColour, boxForeColour, drawMode);
        }

        public static void MessageBox(IConsole console, string message, ConsoleColorExt boxBackColour, ConsoleColorExt boxForeColour, ConsoleDrawMode drawMode)
        {
            MessageBox(console, null, message, 1, boxBackColour, boxForeColour, boxForeColour, boxBackColour, boxForeColour, drawMode);
        }

        public static void MessageBox(string title, string message,
                              ConsoleColorExt boxBackColour, ConsoleColorExt boxForeColour,
                              ConsoleDrawMode drawMode)
        {
            MessageBox(RC.App, title, message, 1, boxBackColour, boxForeColour, boxForeColour, boxBackColour, boxForeColour, drawMode);
        }

        public static void MessageBox(IConsole console, string title, string message,
                      ConsoleColorExt boxBackColour, ConsoleColorExt boxForeColour,
                      ConsoleDrawMode drawMode)
        {
            MessageBox(console, title, message, 1, boxBackColour, boxForeColour, boxForeColour, boxBackColour, boxForeColour, drawMode);
        }

        public static void MessageBox(string title, string message, int yPosition,
                                      ConsoleColorExt titleBackColour, ConsoleColorExt titleForeColour,
                                      ConsoleColorExt messageColour,
                                      ConsoleColorExt boxBackColour, ConsoleColorExt boxForeColour,
                                      ConsoleDrawMode drawMode) 
        {
            MessageBox(RC.App, title, message, yPosition,
                          titleBackColour, titleForeColour,
                          messageColour,
                          boxBackColour, boxForeColour,
                          drawMode);
        }

        public static void MessageBox(IConsole console, 
                                      string title, string message, int yPosition,
                                      ConsoleColorExt titleBackColour, ConsoleColorExt titleForeColour,
                                      ConsoleColorExt messageColour,
                                      ConsoleColorExt boxBackColour, ConsoleColorExt boxForeColour,
                                      ConsoleDrawMode drawMode) 
        {
            if (title != null && (title.IndexOf(Environment.NewLine) >= 0 || title.IndexOf('\n') >= 0))
                throw new Exception(Panels.Strings.ConsoleBoxHelper_MessageBox_LineBreakError);

            int maxWidth = console.BufferWidth - 5;

            if (message.IndexOf(Environment.NewLine) < 0 && message.Length + 2 < maxWidth)
            {
                #region Simple, no line breaks and no wrapping overflow

                int paddingLeft, paddingTop = 1;

                int maxLength = message.Length; 
                
                if (title != null)
                {
                    if (title.Length > maxLength)
                        maxLength = title.Length; 
                }

                int offLeft = (maxWidth - maxLength + 2) / 2;

                if (offLeft > 2) 
                {
                    paddingLeft = 2;
                    offLeft -= 1;
                }
                else 
                    paddingLeft = 1;

                int startingRow = console.CursorTop;

                if (title == null)
                {
                    DrawBox(offLeft, yPosition, message.Length + (paddingLeft * 2), 1 + (paddingTop * 2),
                            boxBackColour, boxForeColour,
                            drawMode, ConsoleLineStyle.Single, true, ConsoleShade.Dim);
                }
                else
                {
                    DrawBox(offLeft, yPosition, message.Length + (paddingLeft * 2), 3 + (paddingTop * 2),
                            boxBackColour, boxForeColour,
                            drawMode, ConsoleLineStyle.Single, true, ConsoleShade.Dim);
                }

                int textOffset = 1;

                if (title != null)
                {
                    int off = (maxLength - title.Length) / 2;

                    switch (drawMode)
                    {
                        case ConsoleDrawMode.Replace:
                            console.SetCursorPosition(offLeft + 1 + off + paddingLeft, startingRow + yPosition + textOffset + paddingTop);
                            break;
                        case ConsoleDrawMode.Overlay:
                            console.SetCursorPosition(offLeft + 1 + off + paddingLeft, yPosition + textOffset + paddingTop);
                            break;
                        default:
                            break;
                    }

                    console.BackgroundColor = titleBackColour;
                    console.ForegroundColor = titleForeColour;

                    textOffset += 2;

                    console.Write(title);
                }

                switch (drawMode)
                {
                    case ConsoleDrawMode.Replace:
                        console.SetCursorPosition(offLeft + 1 + paddingLeft, startingRow + yPosition + textOffset + paddingTop);
                        break;
                    case ConsoleDrawMode.Overlay:
                        console.SetCursorPosition(offLeft + 1 + paddingLeft, yPosition + textOffset + paddingTop);
                        break;
                    default:
                        break;
                }

                console.BackgroundColor = boxBackColour;
                console.ForegroundColor = messageColour;

                console.Write(message);

                #endregion 
            }
            else
            {
                #region Complex, may have line breaks and/or wrapped overflowing text

                int paddingLeft, paddingTop = 1;
                
                int maxLength = 0; 

                List<string> lines = new List<string>(message.Split(new string[] { Environment.NewLine, "\n" }, StringSplitOptions.None));

                if (title != null)
                    lines.Insert(0, title); 

                int i = 0;
                int wrapWidth = (maxWidth / 4 * 3); 

                while (i < lines.Count)
                {                    
                    if (lines[i].Length > maxWidth)
                    {
                        string temp = lines[i];
                        
                        // rollback to the nearest word break
                        int index = maxWidth;
                        while (index > 0 && temp[index] != ' ')
                        {
                            index--; 
                        }

                        if (index == 0 || index < wrapWidth)
                            index = maxWidth;

                        lines[i] = temp.Substring(0, index).Trim();
                        lines.Insert(i + 1, temp.Substring(index).Trim());
                    }

                    if (lines[i].Length > maxLength)
                        maxLength = lines[i].Length;  

                    i++;
                }

                int offLeft = (maxWidth - maxLength) / 2;

                if (offLeft > 2)
                {
                    paddingLeft = 2;
                    offLeft -= 1;
                }
                else
                    paddingLeft = 1;

                int startingRow = console.CursorTop;

                DrawBox(offLeft, yPosition, maxLength + (paddingLeft * 2), lines.Count + (paddingTop * 2),
                                boxBackColour, boxForeColour,
                                drawMode, ConsoleLineStyle.Single, true, ConsoleShade.Dim);

                console.BackgroundColor = boxBackColour;
                console.ForegroundColor = messageColour;

                i = 0; 
                while (i < lines.Count)
                {
                    if (i == 0 && title != null)
                    {
                        int off = (maxLength - lines[i].Length) / 2;
 
                        switch (drawMode)
                        {
                            case ConsoleDrawMode.Replace:
                                console.SetCursorPosition(offLeft + 1 + off + paddingLeft, startingRow + yPosition + 1 + i + paddingTop);
                                break;
                            case ConsoleDrawMode.Overlay:
                                console.SetCursorPosition(offLeft + 1 + off + paddingLeft, yPosition + 1 + i + paddingTop);
                                break;
                            default:
                                break;
                        }

                        console.BackgroundColor = titleBackColour;
                        console.ForegroundColor = titleForeColour;
                    }
                    else
                    {
                        switch (drawMode)
                        {
                            case ConsoleDrawMode.Replace:
                                console.SetCursorPosition(offLeft + 1 + paddingLeft, startingRow + yPosition + 1 + i + paddingTop);
                                break;
                            case ConsoleDrawMode.Overlay:
                                console.SetCursorPosition(offLeft + 1 + paddingLeft, yPosition + 1 + i + paddingTop);
                                break;
                            default:
                                break;
                        }                        
                    }

                    console.Write(lines[i]);

                    // set the colours back again if this was the title 
                    if (i == 0 && title != null)
                    {
                        console.BackgroundColor = boxBackColour;
                        console.ForegroundColor = messageColour;
                    }
                    i++; 
                }
                #endregion 
            }
        }

        #endregion
    }
}
