﻿/* 
 * Rug.Cmd.Panels
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using System.Drawing;

namespace Rug.Cmd.Gui
{
    public abstract class ConsoleControl
    {
        private static readonly Point NotSet = new Point(-1, -1); 

        internal ConsoleContainer m_Parent = null;

        public ConsoleContainer Parent { get { return m_Parent; } }
 
        private Point m_Location = new Point(0, 0);

        private Point m_ClientLocation = NotSet;

        public Point Location 
        { 
            get { return m_Location; } 
            set 
            {                 
                m_Location = value;

                if (m_Location.X < 0 || m_Location.Y < 0)
                    throw new Exception(Panels.Strings.ConsoleControl_CtrlOutOfBounds); 
 
                CalculateClientLocation();
            } 
        } 

        public Point ClientLocation
        {
            get
            {
                if (m_ClientLocation == NotSet)
                    CalculateClientLocation();

                return m_ClientLocation;
            }
        }

        private string m_Name;
        public string Name { get { return m_Name; } set { m_Name = value; } }

        internal void CalculateClientLocation()
        {
            if (m_Parent != null)
            {
                m_ClientLocation = m_Parent.ClientLocation;

                m_ClientLocation.Offset(m_Location);
            }
            else
                m_ClientLocation = m_Location;
        }

        public abstract void Render(); 
    }
}
