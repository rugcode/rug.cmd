﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rug.Cmd.Gui
{
    public enum ConsoleProgressBarTextAlignment { None = 0, Center, Left, Right, Above, Below }
    public enum ConsoleProgressBarTextFormat { Value, ValueOfMax, Percent, Decimal }
    public enum ConsoleProgressBarCaps { Brackets, GreaterThanLessThan, SqrBrackets, Arrows, BlocksSeperated, BlocksJoined, Blocks, Clear }
 
    public class ConsoleProgressBar : ConsoleControl
    {
        public ConsoleColorExt ForeColor = ConsoleColorExt.Inhreit;
        public ConsoleColorExt BackColor = ConsoleColorExt.Inhreit;

        public ConsoleColorExt BarDimForeColor = ConsoleColorExt.Inhreit;
        public ConsoleColorExt BarDimBackColor = ConsoleColorExt.Inhreit;

        public ConsoleColorExt BarLitForeColor = ConsoleColorExt.Inhreit;
        public ConsoleColorExt BarLitBackColor = ConsoleColorExt.Inhreit;

        public ConsoleShade BarLitShade = ConsoleShade.Clear;
        public ConsoleShade BarDimShade = ConsoleShade.Dim;

        public ConsoleProgressBarCaps Caps = ConsoleProgressBarCaps.BlocksJoined;

        public int TextPadding = 0;

        public string Message = ""; 

        public int Value = 0;
        public int Maximum = 1;

        public int Width = 80; 

        public string CustomFormat = "{0}";

        public ConsoleProgressBarTextAlignment TextAlignment = ConsoleProgressBarTextAlignment.Right;
        public ConsoleProgressBarTextFormat TextFormat = ConsoleProgressBarTextFormat.Percent; 

        public override void Render()
        {
            if (Maximum < 1)
                return; 
            
            string labelText = null;

            if (TextAlignment != ConsoleProgressBarTextAlignment.None)
            {
                string innerText = "";

                if (TextPadding > 0)
                    innerText = new string(' ', TextPadding); 

                switch (TextFormat)
                {
                    case ConsoleProgressBarTextFormat.Value:
                        innerText += Value.ToString().PadLeft(Maximum.ToString().Length, ' ');
                        break;
                    case ConsoleProgressBarTextFormat.ValueOfMax:
                        innerText += string.Format(Strings.ConsoleProgressBar_ValueOfMax, Value.ToString().PadLeft(Maximum.ToString().Length, ' '), Maximum.ToString());
                        break;
                    case ConsoleProgressBarTextFormat.Percent:
                        double percent = (100 / (double)Maximum) * Value;

                        innerText += percent.ToString("N1").PadLeft(5, ' ') + "%";
                        break;
                    case ConsoleProgressBarTextFormat.Decimal:
                        double @decimal = (1 / (double)Maximum) * Value;

                        innerText += @decimal.ToString("N3").PadLeft(Maximum.ToString("N3").Length, ' ');
                        break;
                    default:
                        break;
                }

                labelText = string.Format(CustomFormat, innerText);
            }

            int labelLength = labelText != null ? labelText.Length : 0; 

            int barTotalInt = Width - (2 + labelLength);
            double barTotal = (double)barTotalInt; 

            double position = (barTotal / Maximum) * Value;

            int index = (int)position;
            
            if (index > barTotalInt) 
                index = barTotalInt; 

            ConsoleColor fore = Console.ForegroundColor;
            ConsoleColor back = Console.BackgroundColor;

            ConsoleExt.SetCursorPosition(this.ClientLocation.X, this.ClientLocation.Y);

            if (ForeColor != ConsoleColorExt.Inhreit)
                Console.ForegroundColor = (ConsoleColor)(int)(ForeColor);
            if (BackColor != ConsoleColorExt.Inhreit)
                Console.BackgroundColor = (ConsoleColor)(int)(BackColor);

            switch (Caps)
            {
                case ConsoleProgressBarCaps.Brackets:
                    Console.Write('(');
                    break;
                case ConsoleProgressBarCaps.GreaterThanLessThan:
                    Console.Write('<');
                    break;
                case ConsoleProgressBarCaps.SqrBrackets:
                    Console.Write('[');
                    break;
                case ConsoleProgressBarCaps.Arrows:
                    Console.Write(ConsoleChars.GetArrow(ConsoleArrows.Left));
                    break;
                case ConsoleProgressBarCaps.BlocksSeperated:
                    Console.Write(ConsoleChars.GetShade(ConsoleShade.HalfLeft));
                    break;
                case ConsoleProgressBarCaps.BlocksJoined:
                    Console.Write(ConsoleChars.GetShade(ConsoleShade.HalfRight));
                    break;
                case ConsoleProgressBarCaps.Blocks:
                    Console.Write(ConsoleChars.GetShade(ConsoleShade.Opaque));
                    break;
                case ConsoleProgressBarCaps.Clear:
                    Console.Write(' ');
                    break;
                default:
                    break;
            }
            

            string TrimmedMessage = Message.Length > barTotalInt ? Message.Substring(0, barTotalInt) : Message;

            if (index > 0)
            {
                if (BarLitForeColor != ConsoleColorExt.Inhreit)
                    Console.ForegroundColor = (ConsoleColor)(int)(BarLitForeColor);
                if (BarLitBackColor != ConsoleColorExt.Inhreit)
                    Console.BackgroundColor = (ConsoleColor)(int)(BarLitBackColor);

                if (TrimmedMessage.Length < index)
                {
                    Console.Write(TrimmedMessage);

                    Console.Write(new string(ConsoleChars.GetShade(BarLitShade), index - TrimmedMessage.Length));
                }
                else if (TrimmedMessage.Length == index)
                    Console.Write(TrimmedMessage);
                else if (TrimmedMessage.Length > index)
                {
                    Console.Write(TrimmedMessage.Substring(0, index));
                }

                if (index < barTotalInt)
                {
                    if (BarDimForeColor != ConsoleColorExt.Inhreit)
                        Console.ForegroundColor = (ConsoleColor)(int)(BarDimForeColor);
                    if (BarDimBackColor != ConsoleColorExt.Inhreit)
                        Console.BackgroundColor = (ConsoleColor)(int)(BarDimBackColor);

                    int remains = barTotalInt - index;

                    if (TrimmedMessage.Length <= index)
                    {
                        Console.Write(new string(ConsoleChars.GetShade(BarDimShade), remains));
                    }
                    else if (TrimmedMessage.Length > index)
                    {
                        string end = TrimmedMessage.Substring(index);

                        Console.Write(end);

                        Console.Write(new string(ConsoleChars.GetShade(BarDimShade), remains - end.Length));
                    }
                }
            }
            else 
            {
                if (BarDimForeColor != ConsoleColorExt.Inhreit)
                    Console.ForegroundColor = (ConsoleColor)(int)(BarDimForeColor);
                if (BarDimBackColor != ConsoleColorExt.Inhreit)
                    Console.BackgroundColor = (ConsoleColor)(int)(BarDimBackColor);

                Console.Write(TrimmedMessage);

                Console.Write(new string(ConsoleChars.GetShade(BarDimShade), barTotalInt - TrimmedMessage.Length));
            }

            if (ForeColor != ConsoleColorExt.Inhreit)
                Console.ForegroundColor = (ConsoleColor)(int)(ForeColor);
            else 
                Console.ForegroundColor = fore; 

            if (BackColor != ConsoleColorExt.Inhreit)
                Console.BackgroundColor = (ConsoleColor)(int)(BackColor);
            else 
                Console.BackgroundColor = back;

            switch (Caps)
            {
                case ConsoleProgressBarCaps.Brackets:
                    Console.Write(')');
                    break;
                case ConsoleProgressBarCaps.GreaterThanLessThan:
                    Console.Write('>');
                    break;
                case ConsoleProgressBarCaps.SqrBrackets:
                    Console.Write(']');
                    break;
                case ConsoleProgressBarCaps.Arrows:
                    Console.Write(ConsoleChars.GetArrow(ConsoleArrows.Right));
                    break;
                case ConsoleProgressBarCaps.BlocksSeperated:
                    Console.Write(ConsoleChars.GetShade(ConsoleShade.HalfRight));
                    break;
                case ConsoleProgressBarCaps.BlocksJoined:
                    Console.Write(ConsoleChars.GetShade(ConsoleShade.HalfLeft));
                    break;
                case ConsoleProgressBarCaps.Blocks:
                    Console.Write(ConsoleChars.GetShade(ConsoleShade.Opaque));
                    break;
                case ConsoleProgressBarCaps.Clear:
                    Console.Write(' ');
                    break;
                default:
                    break;
            }

            if (TextAlignment != ConsoleProgressBarTextAlignment.None)
            {
                // calcuate the real position
                ConsoleExt.SetCursorPosition(this.ClientLocation.X + (Width - labelLength), this.ClientLocation.Y);

                if (ForeColor != ConsoleColorExt.Inhreit)
                    Console.ForegroundColor = (ConsoleColor)(int)(ForeColor);
                if (BackColor != ConsoleColorExt.Inhreit)
                    Console.BackgroundColor = (ConsoleColor)(int)(BackColor);

                Console.Write(labelText);

                if (ForeColor != ConsoleColorExt.Inhreit)
                    Console.ForegroundColor = fore;
                if (BackColor != ConsoleColorExt.Inhreit)
                    Console.BackgroundColor = back;
            }
        }

    }
}
