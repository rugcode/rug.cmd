﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("ThemeBuilder")]
[assembly: AssemblyDescription("ThemeBuilder (https://bitbucket.org/rugcode/rug.cmd/).")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Rugland Digital Systems")]
[assembly: AssemblyProduct("Rug.Cmd")]
[assembly: AssemblyCopyright("Copyright © Phill Tew 2008")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("1a7fa265-f0da-49f6-8c17-745d04beeb82")]

[assembly: AssemblyVersion("1.3.1.*")]
[assembly: AssemblyFileVersion("1.3.1.0")]
