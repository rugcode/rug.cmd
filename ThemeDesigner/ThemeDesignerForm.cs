/* 
 * ThemeDesigner
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using System.IO;
using System.Windows.Forms;
using Rug.Cmd;
using Rug.Cmd.Colors;

namespace ThemeDesigner
{
    public partial class ThemeDesignerForm : Form
    {
        public delegate void ChangeThemeEvent(ConsoleColor background, ConsoleColorTheme Theme);

        public event ChangeThemeEvent ThemeChanged;

        private ConsoleColorThemeDirectory m_Themes = new ConsoleColorThemeDirectory();

        private string m_LoadedPath = null; 

        public ThemeDesignerForm()
        {
            InitializeComponent();                                 
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
            this.m_AppBackground.Theme = RC.Theme;

            LoadNoneDefault_Click(null, EventArgs.Empty); 
        }

        private void SetCurrentTheme(ConsoleColorTheme theme)
        {
            if (RC.Theme == theme)
                return; 

            RC.Theme = theme;

            this.m_PanelBackground.Theme = RC.Theme;
            this.m_TitleText.Theme = RC.Theme;
            this.m_Text.Theme = RC.Theme;
            this.m_SubText.Theme = RC.Theme;
            this.m_TitleText1.Theme = RC.Theme;
            this.m_Text1.Theme = RC.Theme;
            this.m_SubText1.Theme = RC.Theme;
            this.m_TitleText2.Theme = RC.Theme;
            this.m_Text2.Theme = RC.Theme;
            this.m_SubText2.Theme = RC.Theme;
            this.m_TitleText3.Theme = RC.Theme;
            this.m_Text3.Theme = RC.Theme;
            this.m_SubText3.Theme = RC.Theme;
            this.m_WarningColor1.Theme = RC.Theme;
            this.m_WarningColor2.Theme = RC.Theme;
            this.m_ErrorColor1.Theme = RC.Theme;
            this.m_ErrorColor2.Theme = RC.Theme;
            this.m_PromptColor1.Theme = RC.Theme;
            this.m_PromptColor2.Theme = RC.Theme;

            this.m_SubTextBad.Theme = RC.Theme;
            this.m_TextBad.Theme = RC.Theme;
            this.m_SubTextNutral.Theme = RC.Theme;
            this.m_TextNutral.Theme = RC.Theme;
            this.m_SubTextGood.Theme = RC.Theme;
            this.m_TextGood1.Theme = RC.Theme;

            this.m_PanelSubText.Theme = RC.Theme;
            this.m_PanelText.Theme = RC.Theme;
            this.m_PanelTitleText.Theme = RC.Theme;
            this.m_PanelEdges.Theme = RC.Theme;
            this.m_ControlText.Theme = RC.Theme;

            ThemeChanged((ConsoleColor)RC.Theme[ConsoleThemeColor.AppBackground], RC.Theme);
        }

        private void OnBackColorChange(ConsoleColor background)
        {
            if (ThemeChanged != null)
            {
                ThemeChanged(background, null); 
            }
        }

        private void m_AppBackground_Changed(object sender, System.EventArgs e)
        {
            if (m_Themes != null)
            {
                m_Themes[(ConsoleColor)RC.Theme[ConsoleThemeColor.AppBackground]] = RC.Theme;
                SetCurrentTheme(m_Themes[m_AppBackground.SelectedColor]);
            }
        }

        private void ColourSelector_Changed(object sender, EventArgs e)
        {
            ThemeChanged((ConsoleColor)RC.Theme[ConsoleThemeColor.AppBackground], RC.Theme); 
        }

        private void SetPathToUntitled()
        {
            saveFileDialog1.InitialDirectory = new FileInfo(openFileDialog1.FileName).DirectoryName;
            saveFileDialog1.FileName = "Untitled.ctheme";

            openFileDialog1.InitialDirectory = saveFileDialog1.InitialDirectory;
            openFileDialog1.FileName = "Untitled.ctheme";

            m_LoadedPath = null;
        }

        private void LoadNoneDefault_Click(object sender, EventArgs e)
        {
            SetPathToUntitled();

            int length = (int)ConsoleColor.White + 1;

            for (int i = 0; i < length; i++)
            {
                ConsoleColor fore = ConsoleColor.White;

                if (i > (int)ConsoleColor.DarkYellow)
                    fore = ConsoleColor.Black;

                m_Themes[(ConsoleColor)i] = ConsoleColorTheme.Load(fore, (ConsoleColor)i, ConsoleColorDefaultThemes.HighContrast);
            }

            SetCurrentTheme(m_Themes[m_AppBackground.SelectedColor]);             
        }

        private void LoadSimpleDefault_Click(object sender, EventArgs e)
        {
            SetPathToUntitled();

            int length = (int)ConsoleColor.White + 1;

            for (int i = 0; i < length; i++)
            {
                ConsoleColor fore = ConsoleColor.White;

                if (i > (int)ConsoleColor.DarkYellow)
                    fore = ConsoleColor.Black;

                m_Themes[(ConsoleColor)i] = ConsoleColorTheme.Load(fore, (ConsoleColor)i, ConsoleColorDefaultThemes.Simple);
            }

            SetCurrentTheme(m_Themes[m_AppBackground.SelectedColor]);
        }

        private void LoadColorfulDefault_Click(object sender, EventArgs e)
        {
            SetPathToUntitled();

            int length = (int)ConsoleColor.White + 1;

            for (int i = 0; i < length; i++)
            {
                ConsoleColor fore = ConsoleColor.White;

                if (i > (int)ConsoleColor.DarkYellow)
                    fore = ConsoleColor.Black;

                m_Themes[(ConsoleColor)i] = ConsoleColorTheme.Load(fore, (ConsoleColor)i, ConsoleColorDefaultThemes.Colorful);
            }

            SetCurrentTheme(m_Themes[m_AppBackground.SelectedColor]);
        }

        private void OpenFile_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = openFileDialog1.ShowDialog();

                if (result == DialogResult.OK)
                {
                    m_LoadedPath = openFileDialog1.FileName;

                    saveFileDialog1.InitialDirectory = new FileInfo(m_LoadedPath).DirectoryName;
                    saveFileDialog1.FileName = new FileInfo(m_LoadedPath).Name;

                    ConsoleColorThemeDirectory @new = ConsoleColorThemeDirectory.Read(m_LoadedPath);

                    m_Themes = @new;

                    SetCurrentTheme(m_Themes[m_AppBackground.SelectedColor]);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not load Console Theme '" + openFileDialog1.FileName + "'\n" + ex.Message);
            }
        }


        private void SaveFile_Click(object sender, EventArgs e)
        {
            if (Helper.IsNullOrEmpty(m_LoadedPath))
            {
                SaveFileAs_Click(sender, e);
            }
            else
            {
                try
                {
                    m_Themes.Write(m_LoadedPath);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Could not save Console Theme '" + saveFileDialog1.FileName + "'\n" + ex.Message);
                }
            }
        }

        private void SaveFileAs_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = saveFileDialog1.ShowDialog();

                if (result == DialogResult.OK)
                {
                    m_Themes.Write(saveFileDialog1.FileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not save Console Theme '" + saveFileDialog1.FileName + "'\n" + ex.Message);
            }

        }

        private void CloseFile_Click(object sender, EventArgs e)
        {
            LoadNoneDefault_Click(sender, e); 
        }


        private void Exit_Click(object sender, EventArgs e)
        {
            this.Close(); 
        }
    }
}