namespace ThemeDesigner
{
    partial class ThemeDesignerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.defaultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mutedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colourfulToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blankToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.m_ControlText = new ThemeDesigner.ColourSelector();
            this.label7 = new System.Windows.Forms.Label();
            this.m_ControlBackground = new ThemeDesigner.ColourSelector();
            this.m_PanelSubText = new ThemeDesigner.ColourSelector();
            this.m_PanelText = new ThemeDesigner.ColourSelector();
            this.label5 = new System.Windows.Forms.Label();
            this.m_PanelTitleText = new ThemeDesigner.ColourSelector();
            this.label4 = new System.Windows.Forms.Label();
            this.m_PanelEdges = new ThemeDesigner.ColourSelector();
            this.label22 = new System.Windows.Forms.Label();
            this.m_SubTextBad = new ThemeDesigner.ColourSelector();
            this.m_TextBad = new ThemeDesigner.ColourSelector();
            this.m_SubTextNutral = new ThemeDesigner.ColourSelector();
            this.label24 = new System.Windows.Forms.Label();
            this.m_TextNutral = new ThemeDesigner.ColourSelector();
            this.label26 = new System.Windows.Forms.Label();
            this.m_SubTextGood = new ThemeDesigner.ColourSelector();
            this.m_TextGood1 = new ThemeDesigner.ColourSelector();
            this.m_TitleText = new ThemeDesigner.ColourSelector();
            this.m_Text = new ThemeDesigner.ColourSelector();
            this.m_PromptColor2 = new ThemeDesigner.ColourSelector();
            this.m_SubText = new ThemeDesigner.ColourSelector();
            this.m_PromptColor1 = new ThemeDesigner.ColourSelector();
            this.m_WarningColor2 = new ThemeDesigner.ColourSelector();
            this.m_WarningColor1 = new ThemeDesigner.ColourSelector();
            this.m_TitleText1 = new ThemeDesigner.ColourSelector();
            this.m_Text1 = new ThemeDesigner.ColourSelector();
            this.m_ErrorColor2 = new ThemeDesigner.ColourSelector();
            this.m_ErrorColor1 = new ThemeDesigner.ColourSelector();
            this.m_SubText1 = new ThemeDesigner.ColourSelector();
            this.m_SubText3 = new ThemeDesigner.ColourSelector();
            this.m_PanelBackground = new ThemeDesigner.ColourSelector();
            this.m_Text3 = new ThemeDesigner.ColourSelector();
            this.m_TitleText3 = new ThemeDesigner.ColourSelector();
            this.m_TitleText2 = new ThemeDesigner.ColourSelector();
            this.m_Text2 = new ThemeDesigner.ColourSelector();
            this.m_SubText2 = new ThemeDesigner.ColourSelector();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.m_AppBackground = new ThemeDesigner.ColourSelector();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Application Background";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 891);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Panel Background";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.defaultsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(390, 24);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.closeToolStripMenuItem,
            this.toolStripSeparator2,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.OpenFile_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.CloseFile_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(109, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.SaveFile_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.saveAsToolStripMenuItem.Text = "Save as";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.SaveFileAs_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(109, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.Exit_Click);
            // 
            // defaultsToolStripMenuItem
            // 
            this.defaultsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mutedToolStripMenuItem,
            this.colourfulToolStripMenuItem,
            this.blankToolStripMenuItem1});
            this.defaultsToolStripMenuItem.Name = "defaultsToolStripMenuItem";
            this.defaultsToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.defaultsToolStripMenuItem.Text = "Defaults";
            // 
            // mutedToolStripMenuItem
            // 
            this.mutedToolStripMenuItem.Name = "mutedToolStripMenuItem";
            this.mutedToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.mutedToolStripMenuItem.Text = "Muted";
            this.mutedToolStripMenuItem.Click += new System.EventHandler(this.LoadSimpleDefault_Click);
            // 
            // colourfulToolStripMenuItem
            // 
            this.colourfulToolStripMenuItem.Name = "colourfulToolStripMenuItem";
            this.colourfulToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.colourfulToolStripMenuItem.Text = "Colourful";
            this.colourfulToolStripMenuItem.Click += new System.EventHandler(this.LoadColorfulDefault_Click);
            // 
            // blankToolStripMenuItem1
            // 
            this.blankToolStripMenuItem1.Name = "blankToolStripMenuItem1";
            this.blankToolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.blankToolStripMenuItem1.Text = "Blank";
            this.blankToolStripMenuItem1.Click += new System.EventHandler(this.LoadNoneDefault_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Text Style";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(2, 407);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Error Colours";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(2, 489);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Warning Colours";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 102);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(63, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Text Style 1";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(2, 204);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "Text Style 2";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(2, 301);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(63, 13);
            this.label17.TabIndex = 32;
            this.label17.Text = "Text Style 3";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(2, 571);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(78, 13);
            this.label20.TabIndex = 39;
            this.label20.Text = "Prompt Colours";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.AutoScroll = true;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.m_ControlText);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.m_ControlBackground);
            this.panel1.Controls.Add(this.m_PanelSubText);
            this.panel1.Controls.Add(this.m_PanelText);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.m_PanelTitleText);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.m_PanelEdges);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.m_SubTextBad);
            this.panel1.Controls.Add(this.m_TextBad);
            this.panel1.Controls.Add(this.m_SubTextNutral);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.m_TextNutral);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.m_SubTextGood);
            this.panel1.Controls.Add(this.m_TextGood1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.m_TitleText);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.m_Text);
            this.panel1.Controls.Add(this.m_PromptColor2);
            this.panel1.Controls.Add(this.m_SubText);
            this.panel1.Controls.Add(this.m_PromptColor1);
            this.panel1.Controls.Add(this.m_WarningColor2);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.m_WarningColor1);
            this.panel1.Controls.Add(this.m_TitleText1);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.m_Text1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.m_ErrorColor2);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.m_ErrorColor1);
            this.panel1.Controls.Add(this.m_SubText1);
            this.panel1.Controls.Add(this.m_SubText3);
            this.panel1.Controls.Add(this.m_PanelBackground);
            this.panel1.Controls.Add(this.m_Text3);
            this.panel1.Controls.Add(this.m_TitleText3);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.m_TitleText2);
            this.panel1.Controls.Add(this.m_Text2);
            this.panel1.Controls.Add(this.m_SubText2);
            this.panel1.Location = new System.Drawing.Point(6, 70);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(377, 205);
            this.panel1.TabIndex = 41;
            // 
            // m_ControlText
            // 
            this.m_ControlText.AutoSize = true;
            this.m_ControlText.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_ControlText.Location = new System.Drawing.Point(5, 1123);
            this.m_ControlText.Name = "m_ControlText";
            this.m_ControlText.SelectedColor = System.ConsoleColor.Black;
            this.m_ControlText.Size = new System.Drawing.Size(351, 22);
            this.m_ControlText.TabIndex = 60;
            this.m_ControlText.Theme = null;
            this.m_ControlText.ThemeColor = Rug.Cmd.ConsoleThemeColor.ControlText;
            this.m_ControlText.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(2, 1079);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 59;
            this.label7.Text = "Control Colours";
            // 
            // m_ControlBackground
            // 
            this.m_ControlBackground.AutoSize = true;
            this.m_ControlBackground.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_ControlBackground.Location = new System.Drawing.Point(5, 1095);
            this.m_ControlBackground.Name = "m_ControlBackground";
            this.m_ControlBackground.SelectedColor = System.ConsoleColor.Black;
            this.m_ControlBackground.Size = new System.Drawing.Size(351, 22);
            this.m_ControlBackground.TabIndex = 58;
            this.m_ControlBackground.Theme = null;
            this.m_ControlBackground.ThemeColor = Rug.Cmd.ConsoleThemeColor.ControlBackground;
            this.m_ControlBackground.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_PanelSubText
            // 
            this.m_PanelSubText.AutoSize = true;
            this.m_PanelSubText.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_PanelSubText.Location = new System.Drawing.Point(5, 1044);
            this.m_PanelSubText.Name = "m_PanelSubText";
            this.m_PanelSubText.SelectedColor = System.ConsoleColor.Black;
            this.m_PanelSubText.Size = new System.Drawing.Size(351, 22);
            this.m_PanelSubText.TabIndex = 57;
            this.m_PanelSubText.Theme = null;
            this.m_PanelSubText.ThemeColor = Rug.Cmd.ConsoleThemeColor.PanelSubText;
            this.m_PanelSubText.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_PanelText
            // 
            this.m_PanelText.AutoSize = true;
            this.m_PanelText.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_PanelText.Location = new System.Drawing.Point(5, 1016);
            this.m_PanelText.Name = "m_PanelText";
            this.m_PanelText.SelectedColor = System.ConsoleColor.Black;
            this.m_PanelText.Size = new System.Drawing.Size(351, 22);
            this.m_PanelText.TabIndex = 56;
            this.m_PanelText.Theme = null;
            this.m_PanelText.ThemeColor = Rug.Cmd.ConsoleThemeColor.PanelText;
            this.m_PanelText.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(2, 972);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 13);
            this.label5.TabIndex = 55;
            this.label5.Text = "Panel Text Style";
            // 
            // m_PanelTitleText
            // 
            this.m_PanelTitleText.AutoSize = true;
            this.m_PanelTitleText.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_PanelTitleText.Location = new System.Drawing.Point(5, 988);
            this.m_PanelTitleText.Name = "m_PanelTitleText";
            this.m_PanelTitleText.SelectedColor = System.ConsoleColor.Black;
            this.m_PanelTitleText.Size = new System.Drawing.Size(351, 22);
            this.m_PanelTitleText.TabIndex = 54;
            this.m_PanelTitleText.Theme = null;
            this.m_PanelTitleText.ThemeColor = Rug.Cmd.ConsoleThemeColor.PanelTitleText;
            this.m_PanelTitleText.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 931);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 53;
            this.label4.Text = "Panel Edges";
            // 
            // m_PanelEdges
            // 
            this.m_PanelEdges.AutoSize = true;
            this.m_PanelEdges.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_PanelEdges.Location = new System.Drawing.Point(5, 947);
            this.m_PanelEdges.Name = "m_PanelEdges";
            this.m_PanelEdges.SelectedColor = System.ConsoleColor.Black;
            this.m_PanelEdges.Size = new System.Drawing.Size(351, 22);
            this.m_PanelEdges.TabIndex = 52;
            this.m_PanelEdges.Theme = null;
            this.m_PanelEdges.ThemeColor = Rug.Cmd.ConsoleThemeColor.PanelEdges;
            this.m_PanelEdges.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(2, 817);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(50, 13);
            this.label22.TabIndex = 51;
            this.label22.Text = "Bad Text";
            // 
            // m_SubTextBad
            // 
            this.m_SubTextBad.AutoSize = true;
            this.m_SubTextBad.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_SubTextBad.Location = new System.Drawing.Point(5, 861);
            this.m_SubTextBad.Name = "m_SubTextBad";
            this.m_SubTextBad.SelectedColor = System.ConsoleColor.Black;
            this.m_SubTextBad.Size = new System.Drawing.Size(351, 22);
            this.m_SubTextBad.TabIndex = 50;
            this.m_SubTextBad.Theme = null;
            this.m_SubTextBad.ThemeColor = Rug.Cmd.ConsoleThemeColor.SubTextBad;
            this.m_SubTextBad.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_TextBad
            // 
            this.m_TextBad.AutoSize = true;
            this.m_TextBad.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_TextBad.Location = new System.Drawing.Point(5, 833);
            this.m_TextBad.Name = "m_TextBad";
            this.m_TextBad.SelectedColor = System.ConsoleColor.Black;
            this.m_TextBad.Size = new System.Drawing.Size(351, 22);
            this.m_TextBad.TabIndex = 49;
            this.m_TextBad.Theme = null;
            this.m_TextBad.ThemeColor = Rug.Cmd.ConsoleThemeColor.TextBad;
            this.m_TextBad.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_SubTextNutral
            // 
            this.m_SubTextNutral.AutoSize = true;
            this.m_SubTextNutral.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_SubTextNutral.Location = new System.Drawing.Point(5, 779);
            this.m_SubTextNutral.Name = "m_SubTextNutral";
            this.m_SubTextNutral.SelectedColor = System.ConsoleColor.Black;
            this.m_SubTextNutral.Size = new System.Drawing.Size(351, 22);
            this.m_SubTextNutral.TabIndex = 47;
            this.m_SubTextNutral.Theme = null;
            this.m_SubTextNutral.ThemeColor = Rug.Cmd.ConsoleThemeColor.SubTextNutral;
            this.m_SubTextNutral.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(2, 735);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(59, 13);
            this.label24.TabIndex = 46;
            this.label24.Text = "Nutral Text";
            // 
            // m_TextNutral
            // 
            this.m_TextNutral.AutoSize = true;
            this.m_TextNutral.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_TextNutral.Location = new System.Drawing.Point(5, 751);
            this.m_TextNutral.Name = "m_TextNutral";
            this.m_TextNutral.SelectedColor = System.ConsoleColor.Black;
            this.m_TextNutral.Size = new System.Drawing.Size(351, 22);
            this.m_TextNutral.TabIndex = 45;
            this.m_TextNutral.Theme = null;
            this.m_TextNutral.ThemeColor = Rug.Cmd.ConsoleThemeColor.TextNutral;
            this.m_TextNutral.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(2, 653);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(57, 13);
            this.label26.TabIndex = 43;
            this.label26.Text = "Good Text";
            // 
            // m_SubTextGood
            // 
            this.m_SubTextGood.AutoSize = true;
            this.m_SubTextGood.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_SubTextGood.Location = new System.Drawing.Point(5, 697);
            this.m_SubTextGood.Name = "m_SubTextGood";
            this.m_SubTextGood.SelectedColor = System.ConsoleColor.Black;
            this.m_SubTextGood.Size = new System.Drawing.Size(351, 22);
            this.m_SubTextGood.TabIndex = 42;
            this.m_SubTextGood.Theme = null;
            this.m_SubTextGood.ThemeColor = Rug.Cmd.ConsoleThemeColor.SubTextGood;
            this.m_SubTextGood.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_TextGood1
            // 
            this.m_TextGood1.AutoSize = true;
            this.m_TextGood1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_TextGood1.Location = new System.Drawing.Point(5, 669);
            this.m_TextGood1.Name = "m_TextGood1";
            this.m_TextGood1.SelectedColor = System.ConsoleColor.Black;
            this.m_TextGood1.Size = new System.Drawing.Size(351, 22);
            this.m_TextGood1.TabIndex = 41;
            this.m_TextGood1.Theme = null;
            this.m_TextGood1.ThemeColor = Rug.Cmd.ConsoleThemeColor.TextGood;
            this.m_TextGood1.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_TitleText
            // 
            this.m_TitleText.AutoSize = true;
            this.m_TitleText.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_TitleText.Location = new System.Drawing.Point(5, 19);
            this.m_TitleText.Name = "m_TitleText";
            this.m_TitleText.SelectedColor = System.ConsoleColor.Black;
            this.m_TitleText.Size = new System.Drawing.Size(351, 22);
            this.m_TitleText.TabIndex = 2;
            this.m_TitleText.Theme = null;
            this.m_TitleText.ThemeColor = Rug.Cmd.ConsoleThemeColor.TitleText;
            this.m_TitleText.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_Text
            // 
            this.m_Text.AutoSize = true;
            this.m_Text.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_Text.Location = new System.Drawing.Point(5, 47);
            this.m_Text.Name = "m_Text";
            this.m_Text.SelectedColor = System.ConsoleColor.Black;
            this.m_Text.Size = new System.Drawing.Size(351, 22);
            this.m_Text.TabIndex = 3;
            this.m_Text.Theme = null;
            this.m_Text.ThemeColor = Rug.Cmd.ConsoleThemeColor.Text;
            this.m_Text.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_PromptColor2
            // 
            this.m_PromptColor2.AutoSize = true;
            this.m_PromptColor2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_PromptColor2.Location = new System.Drawing.Point(5, 615);
            this.m_PromptColor2.Name = "m_PromptColor2";
            this.m_PromptColor2.SelectedColor = System.ConsoleColor.Black;
            this.m_PromptColor2.Size = new System.Drawing.Size(351, 22);
            this.m_PromptColor2.TabIndex = 38;
            this.m_PromptColor2.Theme = null;
            this.m_PromptColor2.ThemeColor = Rug.Cmd.ConsoleThemeColor.PromptColor2;
            this.m_PromptColor2.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_SubText
            // 
            this.m_SubText.AutoSize = true;
            this.m_SubText.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_SubText.Location = new System.Drawing.Point(5, 75);
            this.m_SubText.Name = "m_SubText";
            this.m_SubText.SelectedColor = System.ConsoleColor.Black;
            this.m_SubText.Size = new System.Drawing.Size(351, 22);
            this.m_SubText.TabIndex = 4;
            this.m_SubText.Theme = null;
            this.m_SubText.ThemeColor = Rug.Cmd.ConsoleThemeColor.SubText;
            this.m_SubText.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_PromptColor1
            // 
            this.m_PromptColor1.AutoSize = true;
            this.m_PromptColor1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_PromptColor1.Location = new System.Drawing.Point(5, 587);
            this.m_PromptColor1.Name = "m_PromptColor1";
            this.m_PromptColor1.SelectedColor = System.ConsoleColor.Black;
            this.m_PromptColor1.Size = new System.Drawing.Size(351, 22);
            this.m_PromptColor1.TabIndex = 37;
            this.m_PromptColor1.Theme = null;
            this.m_PromptColor1.ThemeColor = Rug.Cmd.ConsoleThemeColor.PromptColor1;
            this.m_PromptColor1.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_WarningColor2
            // 
            this.m_WarningColor2.AutoSize = true;
            this.m_WarningColor2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_WarningColor2.Location = new System.Drawing.Point(5, 533);
            this.m_WarningColor2.Name = "m_WarningColor2";
            this.m_WarningColor2.SelectedColor = System.ConsoleColor.Black;
            this.m_WarningColor2.Size = new System.Drawing.Size(351, 22);
            this.m_WarningColor2.TabIndex = 35;
            this.m_WarningColor2.Theme = null;
            this.m_WarningColor2.ThemeColor = Rug.Cmd.ConsoleThemeColor.WarningColor2;
            this.m_WarningColor2.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_WarningColor1
            // 
            this.m_WarningColor1.AutoSize = true;
            this.m_WarningColor1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_WarningColor1.Location = new System.Drawing.Point(5, 505);
            this.m_WarningColor1.Name = "m_WarningColor1";
            this.m_WarningColor1.SelectedColor = System.ConsoleColor.Black;
            this.m_WarningColor1.Size = new System.Drawing.Size(351, 22);
            this.m_WarningColor1.TabIndex = 15;
            this.m_WarningColor1.Theme = null;
            this.m_WarningColor1.ThemeColor = Rug.Cmd.ConsoleThemeColor.WarningColor1;
            this.m_WarningColor1.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_TitleText1
            // 
            this.m_TitleText1.AutoSize = true;
            this.m_TitleText1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_TitleText1.Location = new System.Drawing.Point(5, 118);
            this.m_TitleText1.Name = "m_TitleText1";
            this.m_TitleText1.SelectedColor = System.ConsoleColor.Black;
            this.m_TitleText1.Size = new System.Drawing.Size(351, 22);
            this.m_TitleText1.TabIndex = 17;
            this.m_TitleText1.Theme = null;
            this.m_TitleText1.ThemeColor = Rug.Cmd.ConsoleThemeColor.TitleText1;
            this.m_TitleText1.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_Text1
            // 
            this.m_Text1.AutoSize = true;
            this.m_Text1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_Text1.Location = new System.Drawing.Point(5, 146);
            this.m_Text1.Name = "m_Text1";
            this.m_Text1.SelectedColor = System.ConsoleColor.Black;
            this.m_Text1.Size = new System.Drawing.Size(351, 22);
            this.m_Text1.TabIndex = 18;
            this.m_Text1.Theme = null;
            this.m_Text1.ThemeColor = Rug.Cmd.ConsoleThemeColor.Text1;
            this.m_Text1.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_ErrorColor2
            // 
            this.m_ErrorColor2.AutoSize = true;
            this.m_ErrorColor2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_ErrorColor2.Location = new System.Drawing.Point(5, 451);
            this.m_ErrorColor2.Name = "m_ErrorColor2";
            this.m_ErrorColor2.SelectedColor = System.ConsoleColor.Black;
            this.m_ErrorColor2.Size = new System.Drawing.Size(351, 22);
            this.m_ErrorColor2.TabIndex = 6;
            this.m_ErrorColor2.Theme = null;
            this.m_ErrorColor2.ThemeColor = Rug.Cmd.ConsoleThemeColor.ErrorColor2;
            this.m_ErrorColor2.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_ErrorColor1
            // 
            this.m_ErrorColor1.AutoSize = true;
            this.m_ErrorColor1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_ErrorColor1.Location = new System.Drawing.Point(5, 423);
            this.m_ErrorColor1.Name = "m_ErrorColor1";
            this.m_ErrorColor1.SelectedColor = System.ConsoleColor.Black;
            this.m_ErrorColor1.Size = new System.Drawing.Size(351, 22);
            this.m_ErrorColor1.TabIndex = 5;
            this.m_ErrorColor1.Theme = null;
            this.m_ErrorColor1.ThemeColor = Rug.Cmd.ConsoleThemeColor.ErrorColor1;
            this.m_ErrorColor1.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_SubText1
            // 
            this.m_SubText1.AutoSize = true;
            this.m_SubText1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_SubText1.Location = new System.Drawing.Point(5, 174);
            this.m_SubText1.Name = "m_SubText1";
            this.m_SubText1.SelectedColor = System.ConsoleColor.Black;
            this.m_SubText1.Size = new System.Drawing.Size(351, 22);
            this.m_SubText1.TabIndex = 19;
            this.m_SubText1.Theme = null;
            this.m_SubText1.ThemeColor = Rug.Cmd.ConsoleThemeColor.SubText1;
            this.m_SubText1.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_SubText3
            // 
            this.m_SubText3.AutoSize = true;
            this.m_SubText3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_SubText3.Location = new System.Drawing.Point(5, 373);
            this.m_SubText3.Name = "m_SubText3";
            this.m_SubText3.SelectedColor = System.ConsoleColor.Black;
            this.m_SubText3.Size = new System.Drawing.Size(351, 22);
            this.m_SubText3.TabIndex = 31;
            this.m_SubText3.Theme = null;
            this.m_SubText3.ThemeColor = Rug.Cmd.ConsoleThemeColor.SubText3;
            this.m_SubText3.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_PanelBackground
            // 
            this.m_PanelBackground.AutoSize = true;
            this.m_PanelBackground.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_PanelBackground.Location = new System.Drawing.Point(5, 907);
            this.m_PanelBackground.Name = "m_PanelBackground";
            this.m_PanelBackground.SelectedColor = System.ConsoleColor.Black;
            this.m_PanelBackground.Size = new System.Drawing.Size(351, 22);
            this.m_PanelBackground.TabIndex = 1;
            this.m_PanelBackground.Theme = null;
            this.m_PanelBackground.ThemeColor = Rug.Cmd.ConsoleThemeColor.PanelBackground;
            this.m_PanelBackground.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_Text3
            // 
            this.m_Text3.AutoSize = true;
            this.m_Text3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_Text3.Location = new System.Drawing.Point(5, 345);
            this.m_Text3.Name = "m_Text3";
            this.m_Text3.SelectedColor = System.ConsoleColor.Black;
            this.m_Text3.Size = new System.Drawing.Size(351, 22);
            this.m_Text3.TabIndex = 30;
            this.m_Text3.Theme = null;
            this.m_Text3.ThemeColor = Rug.Cmd.ConsoleThemeColor.Text3;
            this.m_Text3.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_TitleText3
            // 
            this.m_TitleText3.AutoSize = true;
            this.m_TitleText3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_TitleText3.Location = new System.Drawing.Point(5, 317);
            this.m_TitleText3.Name = "m_TitleText3";
            this.m_TitleText3.SelectedColor = System.ConsoleColor.Black;
            this.m_TitleText3.Size = new System.Drawing.Size(351, 22);
            this.m_TitleText3.TabIndex = 29;
            this.m_TitleText3.Theme = null;
            this.m_TitleText3.ThemeColor = Rug.Cmd.ConsoleThemeColor.TitleText3;
            this.m_TitleText3.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_TitleText2
            // 
            this.m_TitleText2.AutoSize = true;
            this.m_TitleText2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_TitleText2.Location = new System.Drawing.Point(5, 220);
            this.m_TitleText2.Name = "m_TitleText2";
            this.m_TitleText2.SelectedColor = System.ConsoleColor.Black;
            this.m_TitleText2.Size = new System.Drawing.Size(351, 22);
            this.m_TitleText2.TabIndex = 23;
            this.m_TitleText2.Theme = null;
            this.m_TitleText2.ThemeColor = Rug.Cmd.ConsoleThemeColor.TitleText2;
            this.m_TitleText2.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_Text2
            // 
            this.m_Text2.AutoSize = true;
            this.m_Text2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_Text2.Location = new System.Drawing.Point(5, 248);
            this.m_Text2.Name = "m_Text2";
            this.m_Text2.SelectedColor = System.ConsoleColor.Black;
            this.m_Text2.Size = new System.Drawing.Size(351, 22);
            this.m_Text2.TabIndex = 24;
            this.m_Text2.Theme = null;
            this.m_Text2.ThemeColor = Rug.Cmd.ConsoleThemeColor.Text2;
            this.m_Text2.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // m_SubText2
            // 
            this.m_SubText2.AutoSize = true;
            this.m_SubText2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_SubText2.Location = new System.Drawing.Point(5, 276);
            this.m_SubText2.Name = "m_SubText2";
            this.m_SubText2.SelectedColor = System.ConsoleColor.Black;
            this.m_SubText2.Size = new System.Drawing.Size(351, 22);
            this.m_SubText2.TabIndex = 25;
            this.m_SubText2.Theme = null;
            this.m_SubText2.ThemeColor = Rug.Cmd.ConsoleThemeColor.SubText2;
            this.m_SubText2.Changed += new System.EventHandler(this.ColourSelector_Changed);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "ctheme";
            this.saveFileDialog1.Filter = "Console Themes|*.ctheme|All files|*.*";
            this.saveFileDialog1.Title = "Save Theme";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "Untitled.ctheme";
            this.openFileDialog1.Filter = "Console Themes|*.ctheme|All files|*.*";
            this.openFileDialog1.Title = "Open Theme";
            // 
            // m_AppBackground
            // 
            this.m_AppBackground.AutoSize = true;
            this.m_AppBackground.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.m_AppBackground.Location = new System.Drawing.Point(12, 42);
            this.m_AppBackground.Name = "m_AppBackground";
            this.m_AppBackground.SelectedColor = System.ConsoleColor.Black;
            this.m_AppBackground.Size = new System.Drawing.Size(351, 22);
            this.m_AppBackground.TabIndex = 0;
            this.m_AppBackground.Theme = null;
            this.m_AppBackground.ThemeColor = Rug.Cmd.ConsoleThemeColor.AppBackground;
            this.m_AppBackground.Changed += new System.EventHandler(this.m_AppBackground_Changed);
            // 
            // ThemeDesignerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 283);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_AppBackground);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(406, 900);
            this.MinimumSize = new System.Drawing.Size(406, 310);
            this.Name = "ThemeDesignerForm";
            this.Text = "Console Theme Designer";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ColourSelector m_AppBackground;
        private ColourSelector m_PanelBackground;
        private ColourSelector m_TitleText;
        private ColourSelector m_Text;
        private ColourSelector m_SubText;
        private ColourSelector m_ErrorColor1;
        private ColourSelector m_ErrorColor2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private ColourSelector m_WarningColor1;
        private System.Windows.Forms.Label label11;
        private ColourSelector m_SubText1;
        private ColourSelector m_Text1;
        private ColourSelector m_TitleText1;
        private System.Windows.Forms.Label label14;
        private ColourSelector m_SubText2;
        private ColourSelector m_Text2;
        private ColourSelector m_TitleText2;
        private System.Windows.Forms.Label label17;
        private ColourSelector m_SubText3;
        private ColourSelector m_Text3;
        private ColourSelector m_TitleText3;
        private ColourSelector m_WarningColor2;
        private System.Windows.Forms.Label label20;
        private ColourSelector m_PromptColor2;
        private ColourSelector m_PromptColor1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label26;
        private ColourSelector m_SubTextBad;
        private ColourSelector m_TextBad;
        private ColourSelector m_SubTextNutral;
        private ColourSelector m_TextNutral;
        private ColourSelector m_SubTextGood;
        private ColourSelector m_TextGood1;
        private System.Windows.Forms.ToolStripMenuItem defaultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mutedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem colourfulToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blankToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private ColourSelector m_PanelSubText;
        private ColourSelector m_PanelText;        
        private ColourSelector m_PanelTitleText;
        private ColourSelector m_PanelEdges;
        private ColourSelector m_ControlText;

        private ColourSelector m_ControlBackground;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;

    }
}