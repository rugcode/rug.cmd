/* 
 * ThemeDesigner
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using System.Windows.Forms;
using Rug.Cmd;
using Rug.Cmd.Colors;

namespace ThemeDesigner
{
	public partial class ColourSelector : UserControl
	{
		public event EventHandler Changed; 

		private ConsoleColorTheme m_Theme;

		public ConsoleColorTheme Theme
		{   
			get { return m_Theme; }
			set 
			{ 
				m_Theme = value;
				
				if (m_Theme != null)
					SelectedColor = (ConsoleColor)m_Theme[ThemeColor]; 
			}
		}

		private ConsoleThemeColor m_ThemeColor;

		public ConsoleThemeColor ThemeColor
		{
			get { return m_ThemeColor; }
			set
			{
				m_ThemeColor = value;

				if (m_Theme != null)
					SelectedColor = (ConsoleColor)m_Theme[ThemeColor]; 
			}
		}

		private ConsoleColor m_SelectedColor;

		public ConsoleColor SelectedColor
		{
			get 
			{ 
				return m_SelectedColor; 
			}
			set 
			{ 
				m_SelectedColor = value;

				if (m_ThemeColor != ConsoleThemeColor.AppBackground)
				{
					if (m_Theme == null)
						return;

					if ((ConsoleColor)Theme[ThemeColor] != m_SelectedColor)
					{
						Theme[ThemeColor] = (ConsoleColorExt)m_SelectedColor;
						OnChange();
					}
				}
				else
				{
					OnChange();
				}

				UnselectAll();                               

				switch (m_SelectedColor)
				{   
					case ConsoleColor.Black:
						this.BlackBox.BackgroundImage = ThemeDesigner.Properties.Resources.WhiteCross;
						break;
					case ConsoleColor.Blue:
						this.BlueBox.BackgroundImage = ThemeDesigner.Properties.Resources.BlackCross;
						break;
					case ConsoleColor.Cyan:
						this.CyanBox.BackgroundImage = ThemeDesigner.Properties.Resources.BlackCross;
						break;
					case ConsoleColor.DarkBlue:
						this.DarkBlueBox.BackgroundImage = ThemeDesigner.Properties.Resources.WhiteCross;
						break;
					case ConsoleColor.DarkCyan:
						this.DarkCyanBox.BackgroundImage = ThemeDesigner.Properties.Resources.WhiteCross;
						break;
					case ConsoleColor.DarkGray:
						this.DarkGray.BackgroundImage = ThemeDesigner.Properties.Resources.WhiteCross;
						break;
					case ConsoleColor.DarkGreen:
						this.DarkGreenBox.BackgroundImage = ThemeDesigner.Properties.Resources.WhiteCross;
						break;
					case ConsoleColor.DarkMagenta:
						this.DarkMagentaBox.BackgroundImage = ThemeDesigner.Properties.Resources.WhiteCross;
						break;
					case ConsoleColor.DarkRed:
						this.DarkRedBox.BackgroundImage = ThemeDesigner.Properties.Resources.WhiteCross;
						break;
					case ConsoleColor.DarkYellow:
						this.DarkYellowBox.BackgroundImage = ThemeDesigner.Properties.Resources.WhiteCross;
						break;
					case ConsoleColor.Gray:
						this.GrayBox.BackgroundImage = ThemeDesigner.Properties.Resources.BlackCross;
						break;
					case ConsoleColor.Green:
						this.GreenBox.BackgroundImage = ThemeDesigner.Properties.Resources.BlackCross;
						break;
					case ConsoleColor.Magenta:
						this.MagentaBox.BackgroundImage = ThemeDesigner.Properties.Resources.BlackCross;
						break;
					case ConsoleColor.Red:
						this.RedBox.BackgroundImage = ThemeDesigner.Properties.Resources.BlackCross;
						break;
					case ConsoleColor.White:
						this.WhiteBox.BackgroundImage = ThemeDesigner.Properties.Resources.BlackCross;
						break;
					case ConsoleColor.Yellow:
						this.YellowBox.BackgroundImage = ThemeDesigner.Properties.Resources.BlackCross;
						break;
					default:
						break;
				}
			}
		}
	

		public ColourSelector()
		{
			InitializeComponent();

			UnselectAll(); 
		}

		private void UnselectAll()
		{           
			this.WhiteBox.BackgroundImage = null;
			this.YellowBox.BackgroundImage = null;
			this.MagentaBox.BackgroundImage = null;
			this.RedBox.BackgroundImage = null;
			this.CyanBox.BackgroundImage = null;
			this.GreenBox.BackgroundImage = null;
			this.BlueBox.BackgroundImage = null;
			this.DarkGray.BackgroundImage = null;
			this.GrayBox.BackgroundImage = null;
			this.DarkYellowBox.BackgroundImage = null;
			this.DarkMagentaBox.BackgroundImage = null;
			this.DarkRedBox.BackgroundImage = null;
			this.DarkCyanBox.BackgroundImage = null;
			this.DarkGreenBox.BackgroundImage = null;
			this.DarkBlueBox.BackgroundImage = null;
			this.BlackBox.BackgroundImage = null;
		}

		private void OnChange()
		{
			if (Changed != null)
				Changed(this, EventArgs.Empty); 
		}

		private void BlackBox_Click(object sender, EventArgs e)
		{
			SelectedColor = ConsoleColor.Black;
		}

		private void DarkBlueBox_Click(object sender, EventArgs e)
		{
			SelectedColor = ConsoleColor.DarkBlue;
		}

		private void DarkGreenBox_Click(object sender, EventArgs e)
		{
			SelectedColor = ConsoleColor.DarkGreen;
		}

		private void DarkCyanBox_Click(object sender, EventArgs e)
		{
			SelectedColor = ConsoleColor.DarkCyan;
		}

		private void DarkRedBox_Click(object sender, EventArgs e)
		{
			SelectedColor = ConsoleColor.DarkRed;
		}

		private void DarkMagentaBox_Click(object sender, EventArgs e)
		{
			SelectedColor = ConsoleColor.DarkMagenta;
		}

		private void DarkYellowBox_Click(object sender, EventArgs e)
		{
			SelectedColor = ConsoleColor.DarkYellow;
		}

		private void GrayBox_Click(object sender, EventArgs e)
		{
			SelectedColor = ConsoleColor.Gray;
		}

		private void DarkGray_Click(object sender, EventArgs e)
		{
			SelectedColor = ConsoleColor.DarkGray;
		}

		private void BlueBox_Click(object sender, EventArgs e)
		{
			SelectedColor = ConsoleColor.Blue;
		}

		private void GreenBox_Click(object sender, EventArgs e)
		{
			SelectedColor = ConsoleColor.Green;
		}

		private void CyanBox_Click(object sender, EventArgs e)
		{
			SelectedColor = ConsoleColor.Cyan;
		}

		private void RedBox_Click(object sender, EventArgs e)
		{
			SelectedColor = ConsoleColor.Red;
		}

		private void MagentaBox_Click(object sender, EventArgs e)
		{
			SelectedColor = ConsoleColor.Magenta;
		}

		private void YellowBox_Click(object sender, EventArgs e)
		{
			SelectedColor = ConsoleColor.Yellow;
		}

		private void WhiteBox_Click(object sender, EventArgs e)
		{
			SelectedColor = ConsoleColor.White;
		}
	}
}
