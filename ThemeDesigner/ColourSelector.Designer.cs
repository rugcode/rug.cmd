namespace ThemeDesigner
{
    partial class ColourSelector
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.WhiteBox = new System.Windows.Forms.PictureBox();
            this.YellowBox = new System.Windows.Forms.PictureBox();
            this.MagentaBox = new System.Windows.Forms.PictureBox();
            this.RedBox = new System.Windows.Forms.PictureBox();
            this.CyanBox = new System.Windows.Forms.PictureBox();
            this.GreenBox = new System.Windows.Forms.PictureBox();
            this.BlueBox = new System.Windows.Forms.PictureBox();
            this.DarkGray = new System.Windows.Forms.PictureBox();
            this.GrayBox = new System.Windows.Forms.PictureBox();
            this.DarkYellowBox = new System.Windows.Forms.PictureBox();
            this.DarkMagentaBox = new System.Windows.Forms.PictureBox();
            this.DarkRedBox = new System.Windows.Forms.PictureBox();
            this.DarkCyanBox = new System.Windows.Forms.PictureBox();
            this.DarkGreenBox = new System.Windows.Forms.PictureBox();
            this.DarkBlueBox = new System.Windows.Forms.PictureBox();
            this.BlackBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.WhiteBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.YellowBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MagentaBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RedBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CyanBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GreenBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BlueBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DarkGray)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrayBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DarkYellowBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DarkMagentaBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DarkRedBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DarkCyanBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DarkGreenBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DarkBlueBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BlackBox)).BeginInit();
            this.SuspendLayout();
            // 
            // WhiteBox
            // 
            this.WhiteBox.BackColor = System.Drawing.Color.White;
            this.WhiteBox.BackgroundImage = global::ThemeDesigner.Properties.Resources.BlackCross;
            this.WhiteBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.WhiteBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.WhiteBox.Location = new System.Drawing.Point(330, 1);
            this.WhiteBox.Margin = new System.Windows.Forms.Padding(1);
            this.WhiteBox.Name = "WhiteBox";
            this.WhiteBox.Size = new System.Drawing.Size(20, 20);
            this.WhiteBox.TabIndex = 27;
            this.WhiteBox.TabStop = false;
            this.WhiteBox.Click += new System.EventHandler(this.WhiteBox_Click);
            // 
            // YellowBox
            // 
            this.YellowBox.BackColor = System.Drawing.Color.Yellow;
            this.YellowBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.YellowBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.YellowBox.Location = new System.Drawing.Point(308, 1);
            this.YellowBox.Margin = new System.Windows.Forms.Padding(1);
            this.YellowBox.Name = "YellowBox";
            this.YellowBox.Size = new System.Drawing.Size(20, 20);
            this.YellowBox.TabIndex = 26;
            this.YellowBox.TabStop = false;
            this.YellowBox.Click += new System.EventHandler(this.YellowBox_Click);
            // 
            // MagentaBox
            // 
            this.MagentaBox.BackColor = System.Drawing.Color.Magenta;
            this.MagentaBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.MagentaBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.MagentaBox.Location = new System.Drawing.Point(286, 1);
            this.MagentaBox.Margin = new System.Windows.Forms.Padding(1);
            this.MagentaBox.Name = "MagentaBox";
            this.MagentaBox.Size = new System.Drawing.Size(20, 20);
            this.MagentaBox.TabIndex = 25;
            this.MagentaBox.TabStop = false;
            this.MagentaBox.Click += new System.EventHandler(this.MagentaBox_Click);
            // 
            // RedBox
            // 
            this.RedBox.BackColor = System.Drawing.Color.Red;
            this.RedBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.RedBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.RedBox.Location = new System.Drawing.Point(264, 1);
            this.RedBox.Margin = new System.Windows.Forms.Padding(1);
            this.RedBox.Name = "RedBox";
            this.RedBox.Size = new System.Drawing.Size(20, 20);
            this.RedBox.TabIndex = 24;
            this.RedBox.TabStop = false;
            this.RedBox.Click += new System.EventHandler(this.RedBox_Click);
            // 
            // CyanBox
            // 
            this.CyanBox.BackColor = System.Drawing.Color.Cyan;
            this.CyanBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.CyanBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.CyanBox.Location = new System.Drawing.Point(242, 1);
            this.CyanBox.Margin = new System.Windows.Forms.Padding(1);
            this.CyanBox.Name = "CyanBox";
            this.CyanBox.Size = new System.Drawing.Size(20, 20);
            this.CyanBox.TabIndex = 23;
            this.CyanBox.TabStop = false;
            this.CyanBox.Click += new System.EventHandler(this.CyanBox_Click);
            // 
            // GreenBox
            // 
            this.GreenBox.BackColor = System.Drawing.Color.Lime;
            this.GreenBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.GreenBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.GreenBox.Location = new System.Drawing.Point(220, 1);
            this.GreenBox.Margin = new System.Windows.Forms.Padding(1);
            this.GreenBox.Name = "GreenBox";
            this.GreenBox.Size = new System.Drawing.Size(20, 20);
            this.GreenBox.TabIndex = 22;
            this.GreenBox.TabStop = false;
            this.GreenBox.Click += new System.EventHandler(this.GreenBox_Click);
            // 
            // BlueBox
            // 
            this.BlueBox.BackColor = System.Drawing.Color.Blue;
            this.BlueBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.BlueBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.BlueBox.Location = new System.Drawing.Point(198, 1);
            this.BlueBox.Margin = new System.Windows.Forms.Padding(1);
            this.BlueBox.Name = "BlueBox";
            this.BlueBox.Size = new System.Drawing.Size(20, 20);
            this.BlueBox.TabIndex = 21;
            this.BlueBox.TabStop = false;
            this.BlueBox.Click += new System.EventHandler(this.BlueBox_Click);
            // 
            // DarkGray
            // 
            this.DarkGray.BackColor = System.Drawing.Color.Gray;
            this.DarkGray.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.DarkGray.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DarkGray.Location = new System.Drawing.Point(176, 1);
            this.DarkGray.Margin = new System.Windows.Forms.Padding(1);
            this.DarkGray.Name = "DarkGray";
            this.DarkGray.Size = new System.Drawing.Size(20, 20);
            this.DarkGray.TabIndex = 20;
            this.DarkGray.TabStop = false;
            this.DarkGray.Click += new System.EventHandler(this.DarkGray_Click);
            // 
            // GrayBox
            // 
            this.GrayBox.BackColor = System.Drawing.Color.LightGray;
            this.GrayBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.GrayBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.GrayBox.Location = new System.Drawing.Point(154, 1);
            this.GrayBox.Margin = new System.Windows.Forms.Padding(1);
            this.GrayBox.Name = "GrayBox";
            this.GrayBox.Size = new System.Drawing.Size(20, 20);
            this.GrayBox.TabIndex = 19;
            this.GrayBox.TabStop = false;
            this.GrayBox.Click += new System.EventHandler(this.GrayBox_Click);
            // 
            // DarkYellowBox
            // 
            this.DarkYellowBox.BackColor = System.Drawing.Color.Olive;
            this.DarkYellowBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.DarkYellowBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DarkYellowBox.Location = new System.Drawing.Point(132, 1);
            this.DarkYellowBox.Margin = new System.Windows.Forms.Padding(1);
            this.DarkYellowBox.Name = "DarkYellowBox";
            this.DarkYellowBox.Size = new System.Drawing.Size(20, 20);
            this.DarkYellowBox.TabIndex = 18;
            this.DarkYellowBox.TabStop = false;
            this.DarkYellowBox.Click += new System.EventHandler(this.DarkYellowBox_Click);
            // 
            // DarkMagentaBox
            // 
            this.DarkMagentaBox.BackColor = System.Drawing.Color.DarkMagenta;
            this.DarkMagentaBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.DarkMagentaBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DarkMagentaBox.Location = new System.Drawing.Point(110, 1);
            this.DarkMagentaBox.Margin = new System.Windows.Forms.Padding(1);
            this.DarkMagentaBox.Name = "DarkMagentaBox";
            this.DarkMagentaBox.Size = new System.Drawing.Size(20, 20);
            this.DarkMagentaBox.TabIndex = 17;
            this.DarkMagentaBox.TabStop = false;
            this.DarkMagentaBox.Click += new System.EventHandler(this.DarkMagentaBox_Click);
            // 
            // DarkRedBox
            // 
            this.DarkRedBox.BackColor = System.Drawing.Color.DarkRed;
            this.DarkRedBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.DarkRedBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DarkRedBox.Location = new System.Drawing.Point(88, 1);
            this.DarkRedBox.Margin = new System.Windows.Forms.Padding(1);
            this.DarkRedBox.Name = "DarkRedBox";
            this.DarkRedBox.Size = new System.Drawing.Size(20, 20);
            this.DarkRedBox.TabIndex = 16;
            this.DarkRedBox.TabStop = false;
            this.DarkRedBox.Click += new System.EventHandler(this.DarkRedBox_Click);
            // 
            // DarkCyanBox
            // 
            this.DarkCyanBox.BackColor = System.Drawing.Color.DarkCyan;
            this.DarkCyanBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.DarkCyanBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DarkCyanBox.Location = new System.Drawing.Point(66, 1);
            this.DarkCyanBox.Margin = new System.Windows.Forms.Padding(1);
            this.DarkCyanBox.Name = "DarkCyanBox";
            this.DarkCyanBox.Size = new System.Drawing.Size(20, 20);
            this.DarkCyanBox.TabIndex = 6;
            this.DarkCyanBox.TabStop = false;
            this.DarkCyanBox.Click += new System.EventHandler(this.DarkCyanBox_Click);
            // 
            // DarkGreenBox
            // 
            this.DarkGreenBox.BackColor = System.Drawing.Color.DarkGreen;
            this.DarkGreenBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.DarkGreenBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DarkGreenBox.Location = new System.Drawing.Point(44, 1);
            this.DarkGreenBox.Margin = new System.Windows.Forms.Padding(1);
            this.DarkGreenBox.Name = "DarkGreenBox";
            this.DarkGreenBox.Size = new System.Drawing.Size(20, 20);
            this.DarkGreenBox.TabIndex = 5;
            this.DarkGreenBox.TabStop = false;
            this.DarkGreenBox.Click += new System.EventHandler(this.DarkGreenBox_Click);
            // 
            // DarkBlueBox
            // 
            this.DarkBlueBox.BackColor = System.Drawing.Color.DarkBlue;
            this.DarkBlueBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.DarkBlueBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.DarkBlueBox.Location = new System.Drawing.Point(22, 1);
            this.DarkBlueBox.Margin = new System.Windows.Forms.Padding(1);
            this.DarkBlueBox.Name = "DarkBlueBox";
            this.DarkBlueBox.Size = new System.Drawing.Size(20, 20);
            this.DarkBlueBox.TabIndex = 4;
            this.DarkBlueBox.TabStop = false;
            this.DarkBlueBox.Click += new System.EventHandler(this.DarkBlueBox_Click);
            // 
            // BlackBox
            // 
            this.BlackBox.BackColor = System.Drawing.Color.Black;
            this.BlackBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.BlackBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.BlackBox.Location = new System.Drawing.Point(0, 1);
            this.BlackBox.Margin = new System.Windows.Forms.Padding(1);
            this.BlackBox.Name = "BlackBox";
            this.BlackBox.Size = new System.Drawing.Size(20, 20);
            this.BlackBox.TabIndex = 1;
            this.BlackBox.TabStop = false;
            this.BlackBox.Click += new System.EventHandler(this.BlackBox_Click);
            // 
            // ColourSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.WhiteBox);
            this.Controls.Add(this.YellowBox);
            this.Controls.Add(this.MagentaBox);
            this.Controls.Add(this.RedBox);
            this.Controls.Add(this.CyanBox);
            this.Controls.Add(this.GreenBox);
            this.Controls.Add(this.BlueBox);
            this.Controls.Add(this.DarkGray);
            this.Controls.Add(this.GrayBox);
            this.Controls.Add(this.DarkYellowBox);
            this.Controls.Add(this.DarkMagentaBox);
            this.Controls.Add(this.DarkRedBox);
            this.Controls.Add(this.DarkCyanBox);
            this.Controls.Add(this.DarkGreenBox);
            this.Controls.Add(this.DarkBlueBox);
            this.Controls.Add(this.BlackBox);
            this.Name = "ColourSelector";
            this.Size = new System.Drawing.Size(351, 22);
            ((System.ComponentModel.ISupportInitialize)(this.WhiteBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.YellowBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MagentaBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RedBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CyanBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GreenBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BlueBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DarkGray)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GrayBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DarkYellowBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DarkMagentaBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DarkRedBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DarkCyanBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DarkGreenBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DarkBlueBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BlackBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox BlackBox;
        private System.Windows.Forms.PictureBox DarkCyanBox;
        private System.Windows.Forms.PictureBox DarkGreenBox;
        private System.Windows.Forms.PictureBox DarkBlueBox;
        private System.Windows.Forms.PictureBox WhiteBox;
        private System.Windows.Forms.PictureBox YellowBox;
        private System.Windows.Forms.PictureBox MagentaBox;
        private System.Windows.Forms.PictureBox RedBox;
        private System.Windows.Forms.PictureBox CyanBox;
        private System.Windows.Forms.PictureBox GreenBox;
        private System.Windows.Forms.PictureBox BlueBox;
        private System.Windows.Forms.PictureBox DarkGray;
        private System.Windows.Forms.PictureBox GrayBox;
        private System.Windows.Forms.PictureBox DarkYellowBox;
        private System.Windows.Forms.PictureBox DarkMagentaBox;
        private System.Windows.Forms.PictureBox DarkRedBox;
    }
}
