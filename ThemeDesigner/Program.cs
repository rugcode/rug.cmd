/* 
 * ThemeDesigner
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Rug.Cmd;
using Rug.Cmd.Colors;

namespace ThemeDesigner
{
    class Program
    {
        #region Private Static Members
        
        static private bool m_Exit = false;
        static private bool m_Changed = false;

        static private ConsoleColor m_Background;
        static private ConsoleColor m_Foreground;
        static private ConsoleColorTheme m_Theme;

        static private string m_TestTitle = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam.";
        static private string m_TestShort = "Lorem ipsum dolor sit amet\nconsectetuer adipiscing elit, sed diam.";        

        static private string m_Text;
        
        #endregion

        [STAThread]
        static void Main(string[] args)
        {
            StringBuilder sb = new StringBuilder();

            AddEnumToString(sb, ConsoleThemeColor.TitleText, "Main title text");
            AddEnumToString(sb, ConsoleThemeColor.Text, " refusa continuar payar custosi traductores. It solmen va esser necessi far\n uniform Angles, quam un skeptic Cambridge amico dit me que");
            AddEnumToString(sb, ConsoleThemeColor.SubText, " - Por scientie, musica, sport etc\n - It solmen va esser necessi far uniform grammatica\n - nonummy nibh euismod tincidunt ut laoreet");
            AddSpaceToString(sb);

            AddEnumToString(sb, ConsoleThemeColor.TitleText1, "Main title text 1");
            AddEnumToString(sb, ConsoleThemeColor.Text1, " refusa continuar payar custosi traductores. It solmen va esser necessi far\n uniform Angles, quam un skeptic Cambridge amico dit me que");
            AddEnumToString(sb, ConsoleThemeColor.SubText1, " - Por scientie, musica, sport etc\n - It solmen va esser necessi far uniform grammatica\n - nonummy nibh euismod tincidunt ut laoreet");
            AddSpaceToString(sb);

            AddEnumToString(sb, ConsoleThemeColor.TitleText2, "Main title text 2");
            AddEnumToString(sb, ConsoleThemeColor.Text2, " refusa continuar payar custosi traductores. It solmen va esser necessi far\n uniform Angles, quam un skeptic Cambridge amico dit me que");
            AddEnumToString(sb, ConsoleThemeColor.SubText2, " - Por scientie, musica, sport etc\n - It solmen va esser necessi far uniform grammatica\n - nonummy nibh euismod tincidunt ut laoreet");
            AddSpaceToString(sb);

            AddEnumToString(sb, ConsoleThemeColor.TitleText3, "Main title text 3");
            AddEnumToString(sb, ConsoleThemeColor.Text3, " refusa continuar payar custosi traductores. It solmen va esser necessi far\n uniform Angles, quam un skeptic Cambridge amico dit me que");
            AddEnumToString(sb, ConsoleThemeColor.SubText3, " - Por scientie, musica, sport etc\n - It solmen va esser necessi far uniform grammatica\n - nonummy nibh euismod tincidunt ut laoreet");
            AddSpaceToString(sb);

            AddEnumToString(sb, ConsoleThemeColor.TextGood, m_TestShort);
            AddEnumToString(sb, ConsoleThemeColor.SubTextGood, m_TestTitle);
            AddSpaceToString(sb);

            AddEnumToString(sb, ConsoleThemeColor.TextNutral, m_TestShort);
            AddEnumToString(sb, ConsoleThemeColor.SubTextNutral, m_TestTitle);
            AddSpaceToString(sb);

            AddEnumToString(sb, ConsoleThemeColor.TextBad, m_TestShort);
            AddEnumToString(sb, ConsoleThemeColor.SubTextBad, m_TestTitle);
            AddSpaceToString(sb);

            AddEnumToString(sb, ConsoleThemeColor.ErrorColor1, "Error or Exception title message");
            AddEnumToString(sb, ConsoleThemeColor.ErrorColor2, m_TestShort);
            AddSpaceToString(sb);

            AddEnumToString(sb, ConsoleThemeColor.WarningColor1, "Warning title message");
            AddEnumToString(sb, ConsoleThemeColor.WarningColor2, m_TestShort);
            AddSpaceToString(sb);

            AddEnumToString(sb, ConsoleThemeColor.PromptColor1, "Prompt title message");
            AddEnumToString(sb, ConsoleThemeColor.PromptColor2, m_TestShort);
            AddSpaceToString(sb);

            m_Text = sb.ToString(); 

            m_Background = Console.BackgroundColor;
            m_Foreground = Console.ForegroundColor;

            m_Theme = ConsoleColorTheme.Load(m_Foreground, m_Background, ConsoleColorDefaultThemes.None);

            RC.Theme = m_Theme;

            Thread thread = new Thread(ThreadStart);
            thread.SetApartmentState(ApartmentState.STA);
            
            thread.Start();

            bool first = true; 

            while (m_Exit == false)
            {
                while (!first && m_Changed == false)
                {
                    Thread.CurrentThread.Join(100); 
                }
                
                first = false; 

                m_Changed = false;

                RC.Theme = m_Theme;
                RC.BackgroundThemeColor = ConsoleThemeColor.AppBackground; 

                Console.BackgroundColor = (ConsoleColor)m_Background;                               
                Console.WriteLine();
                Console.Clear();                

                CmdHelper.WriteSimpleBanner("Application Title", ' ', RC.Theme[ConsoleThemeColor.AppBackground], RC.Theme[ConsoleThemeColor.TitleText]);

                RC.WriteLine(ConsoleVerbosity.Silent); 

                RC.WriteInterpreted(m_Text, 2, 2);

                Rug.Cmd.CmdHelper.WriteRuglandLogo(50, RC.CursorTop - 7, 3, 2, false, ConsoleShade.Opaque);

                RC.BackgroundThemeColor = ConsoleThemeColor.PanelBackground;
                RC.ForegroundThemeColor = ConsoleThemeColor.PanelEdges; 

                RC.Write(ConsoleChars.GetLineChar(ConsoleLineStyle.Double, LineChars.TopLeft) + 
                        new string(ConsoleChars.GetLineChar(ConsoleLineStyle.Double, LineChars.TopCenter), RC.BufferWidth - 2) + 
                        ConsoleChars.GetLineChar(ConsoleLineStyle.Double, LineChars.TopRight));

                for (int i = 0; i < 8; i++)
                {
                    RC.Write(ConsoleChars.GetLineChar(ConsoleLineStyle.Double, LineChars.MidLeft) +
                            new string(ConsoleChars.GetLineChar(ConsoleLineStyle.Double, LineChars.MidCenter), RC.BufferWidth - 2) +
                            ConsoleChars.GetLineChar(ConsoleLineStyle.Double, LineChars.MidRight));
                }

                int tempTop = RC.CursorTop;

                RC.CursorTop = RC.CursorTop - 9;

                RC.CursorLeft = 1;
                RC.ForegroundThemeColor = ConsoleThemeColor.PanelTitleText;
                RC.Write("Panel Title Text Colour");

                RC.CursorTop = RC.CursorTop + 3;
                RC.CursorLeft = 3;
                RC.ForegroundThemeColor = ConsoleThemeColor.PanelText;
                RC.Write("refusa continuar payar custosi traductores. It solmen va esser necessi far");

                RC.CursorTop = RC.CursorTop + 1;
                RC.CursorLeft = 3;
                RC.ForegroundThemeColor = ConsoleThemeColor.PanelSubText;
                RC.Write("uniform Angles, quam un skeptic Cambridge amico dit me que");

                RC.CursorLeft = 0;
                RC.CursorTop = tempTop; 
                RC.ForegroundThemeColor = ConsoleThemeColor.PanelEdges; 

                RC.Write(ConsoleChars.GetLineChar(ConsoleLineStyle.Double, LineChars.BottomLeft) +
                        new string(ConsoleChars.GetLineChar(ConsoleLineStyle.Double, LineChars.BottomCenter), RC.BufferWidth - 2) +
                        ConsoleChars.GetLineChar(ConsoleLineStyle.Double, LineChars.BottomRight));                 
            }
        }

        #region Private Methods
        
        private static void AddSpaceToString(StringBuilder sb)
        {
            sb.AppendLine(); 
        }

        private static void AddEnumToString(StringBuilder sb, ConsoleThemeColor consoleThemeColor, string str)
        {
            sb.AppendLine("<t:" + ((int)consoleThemeColor) + ">" + str + "</t:" + ((int)consoleThemeColor) + ">");
        }

        private static void ThreadStart()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                ThemeDesignerForm Form = new ThemeDesignerForm();
                Form.ThemeChanged += new ThemeDesignerForm.ChangeThemeEvent(Form_ThemeChanged);
                Application.Run(Form);
            }
            catch (Exception ex)
            {
                RC.WriteException(000, ex); 
            }
            finally
            {
                m_Exit = true;
                m_Changed = true; 
            }
        }

        static void Form_ThemeChanged(ConsoleColor background, ConsoleColorTheme Theme)
        {
            m_Background = background;
            m_Theme = Theme;
            m_Changed = true;
        }

        #endregion
    }
}
