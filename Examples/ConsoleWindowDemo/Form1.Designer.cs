﻿namespace ConsoleWindowDemo
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.consoleControl1 = new Rug.Cmd.Forms.ConsoleControl();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.SuspendLayout();
			// 
			// consoleControl1
			// 
			this.consoleControl1.BackColor = System.Drawing.Color.Black;
			this.consoleControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.consoleControl1.Font = new System.Drawing.Font("Courier New", 12F);
			this.consoleControl1.Location = new System.Drawing.Point(0, 0);
			this.consoleControl1.Name = "consoleControl1";
			this.consoleControl1.ReadOnly = true;
			this.consoleControl1.Size = new System.Drawing.Size(605, 556);
			this.consoleControl1.TabIndex = 0;
			this.consoleControl1.Text = "";
			this.consoleControl1.TextChanged += new System.EventHandler(this.consoleControl1_TextChanged);
			// 
			// timer1
			// 
			this.timer1.Interval = 50;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(605, 556);
			this.Controls.Add(this.consoleControl1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}

		#endregion

		private Rug.Cmd.Forms.ConsoleControl consoleControl1;
		private System.Windows.Forms.Timer timer1;

	}
}

