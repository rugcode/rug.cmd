﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Rug.Cmd;
using Rug.Cmd.Colors;

namespace ConsoleWindowDemo
{
	public partial class Form1 : Form
	{
		Random m_Rand = new Random(); 

		public Form1()
		{			
			RC.Verbosity = ConsoleVerbosity.Normal;

			InitializeComponent();
		}

		private void consoleControl1_TextChanged(object sender, EventArgs e)
		{

		}

		private void Form1_Load(object sender, EventArgs e)
		{			
			RC.App = consoleControl1.Buffer;
			RC.BackgroundColor = ConsoleColorExt.White; 
			//RC.Theme = ConsoleColorTheme.Load(ConsoleColor.Black, ConsoleColor.White, ConsoleColorDefaultThemes.Colorful);
			RC.Theme = ConsoleColorTheme.Load(ConsoleColorDefaultThemes.Colorful);
			consoleControl1.SetColors(); 
			RC.WriteLine(ConsoleThemeColor.TitleText, "Thing");
			RC.WriteLine(ConsoleThemeColor.SubText, "Thing");
			RC.WriteLine(ConsoleThemeColor.TextGood, "Thing");
			RC.WriteLine(ConsoleThemeColor.SubTextGood, "Thing");
			RC.WriteLine(ConsoleThemeColor.TextBad, "Thing");
			RC.WriteLine(ConsoleThemeColor.TitleText2, "Thing");
			RC.WriteLine(ConsoleThemeColor.Text2, "Thing");
			RC.WriteLine(ConsoleThemeColor.SubText2, "Thing");

			timer1.Enabled = true; 
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			RC.WriteLine((ConsoleThemeColor)m_Rand.Next(0, 31), "Thing " + m_Rand.NextDouble());
		}
	}
}
