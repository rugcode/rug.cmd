﻿/* 
 * ArgumentExample part of Rugland Console Framework
 * 
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
 * EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * Copyright (C) 2008 Phill Tew. All rights reserved.
 * 
 */

using System;
using System.Collections.Generic;
using Rug.Cmd;
using Rug.Cmd.Colors;

namespace ArgumentExample
{
    class Program
    {
        static void Main(string[] args)
        {            
            // stash the color state
            ConsoleColorState state = RC.ColorState;

            // create the argument parser
            ArgumentParser parser = new ArgumentParser("ArgumentExample", "Example of argument parsing");

            // create the argument for a string
            StringArgument StringArg = new StringArgument("String", "Example string argument", "This argument demonstrates string arguments");

            // add the argument to the parser 
            parser.Add("/", "String", StringArg);

            try
            {       
                // parse arguemnts
                parser.Parse(args);

                // did the parser detect a /? arguemnt 
                if (parser.HelpMode == false) 
                {
                    // was the string argument defiend 
                    if (StringArg.Defined == true)
                    {
                        // write its value
                        RC.WriteLine(ConsoleThemeColor.TitleText, "String argument was defined");
                        CmdHelper.WriteInfoToConsole("String Value", StringArg.Value, RC.Theme[ConsoleThemeColor.TextGood], true);
                    }
                    else
                    {
                        // was not defiend 
                        RC.WriteLine(ConsoleThemeColor.TitleText, "String argument was not defined");
                    }
                }                
            }
            catch (Exception ex)
            {
                // Write the exception to the console 
                RC.WriteException(0001, ex);
            }
            finally
            {
				RC.PromptForKey("Press any key to exit..", true, false);

                // Reset the color state
                RC.ColorState = state;
            }
        }
    }
}
