﻿/* 
 * ApplicationExample part of Rugland Console Framework
 * 
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
 * EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * Copyright (C) 2008 Phill Tew. All rights reserved.
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Rug.Cmd;
using Rug.Cmd.Colors;

namespace ApplicationExample
{
	class Program
	{
		static StringArgument StringArg = new StringArgument("String", "String argument", "This argument demonstrates string arguments");
		static StringListArgument StringListArg = new StringListArgument("List", "List argument", "This argument demonstrates string list arguments");
		static CsvArgument CsvArg = new CsvArgument("CSV", "CSV argument", "This argument demonstrates comma seperated list string arguments");
		static OptionalStringArgument OptionalArg = new OptionalStringArgument("Optional", "Optional argument", "This argument demonstrates optional string arguments");

		static BoolSwitch BuildSwitch = new BoolSwitch("Build mode", "Generates errors compatible with MS VisualStudio");
		static BoolSwitch QuietSwitch = new BoolSwitch("Quiet mode", "Don't display messages about every action");
		static EnumSwitch VerboseSwitch = new EnumSwitch("Message verbosity level", "Use this argument to specify the level of detail the application outputs whilst it is running.", typeof(ConsoleVerbosity));
		static PlusMinusSwitch WarningsAsErrors = new PlusMinusSwitch("Warnings are errors", "When +w warning will be reported as errors.\nWhen -w warnings will not be reported at all.\nThis switch is to be used in conjunction with the /build switch.", false);

		static void Main(string[] args)
		{                        
			RC.ApplicationBuildReportPrefix = "APPTEST"; 

			// stash the color state
			ConsoleColorState state = RC.ColorState;
			
			// uncomment these next 2 lines to demo this app in other background colors
			//RC.BackgroundColor = ConsoleColorExt.DarkRed;
			//Console.Clear(); 

			// set the color theme
			RC.Theme = ConsoleColorTheme.Load(ConsoleColorDefaultThemes.Colorful);

			string App_Title = "Example of a simple application with argument parsing";
			string Title = App_Title + " " + typeof(Program).Assembly.GetName().Version.ToString();
			string AboutText = "Text about the application example";

			ArgumentParser parser = new ArgumentParser("ArgumentExample", Title + "\n" + AboutText);

			parser.AboutText = AboutText;
			parser.AboutTitleText = Title;
			parser.AboutText = AboutText;
			parser.AboutTextLong = "This example is demonstration of the argument parser";
			parser.CreditsText = "Copyright (C) 2008 Phill Tew";
			parser.LegalText = "Legal Text";
			parser.HasApplicationDocument = true;

			parser.FirstArgument = StringArg;

			parser.Add("/", "List", StringListArg);
			parser.Add("/", "CSV", CsvArg);
			parser.Add("/", "Optional", OptionalArg);

			parser.Add("/", "Build", BuildSwitch);
			parser.Add("/", "Quiet", QuietSwitch);
			parser.Add("/", "Verbose", VerboseSwitch);
			parser.Add(PlusMinusSwitch.KeyPrefix, "Warning", WarningsAsErrors);

			try
			{
				parser.Parse(args);

				if (parser.HelpMode == false)
				{
					// wite a banner to the console 
					CmdHelper.WriteSimpleBanner(App_Title, ' ', RC.Theme[ConsoleThemeColor.AppBackground], RC.Theme[ConsoleThemeColor.TitleText]);
					RC.WriteLine(); 

					#region Setup the console environment 

					if (BuildSwitch.Defined == true)
					{
						RC.IsBuildMode = true;
						RC.WriteLine(ConsoleThemeColor.TitleText3, "Build mode argument was defined");
					}

					if (QuietSwitch.Defined == true)
					{
						RC.Verbosity = ConsoleVerbosity.Quiet;
						RC.WriteLine(ConsoleThemeColor.TitleText3, "Quiet mode argument was defined");
					}

					if (VerboseSwitch.Defined == true)
					{
						RC.Verbosity = (ConsoleVerbosity)VerboseSwitch.Value;
						RC.WriteLine(ConsoleThemeColor.TitleText3, "Verbosity level argument was defined");
					}

					if (WarningsAsErrors.Defined == true)
					{
						RC.WarningsAsErrors = WarningsAsErrors.Value;
						RC.ReportWarnings = WarningsAsErrors.Value;
						RC.WriteLine(ConsoleThemeColor.TitleText3, "Warnings as errors argument was defined");
					}

					#endregion

					#region Write a logo 'Demo'

					string[] DemoLogo = new string[] { 
						"{0}{0}{4}{0}{0}{1}{0}{0}{1}{0}{0}{1}", 
						"{0}{4}{1}{0}{0}{4}{1}{1}{1}{0}{4}{1}", 
						"{0}{0}{4}{0}{0}{1}{1}{2}{1}{0}{0}{1}"};
					
					RC.WriteLine("");
					CmdHelper.WriteLogo(RC.CursorLeft + 17, RC.CursorTop + 1, 3, 2, ConsoleShade.Opaque, ConsoleShade.Dark, ConsoleShade.Dark, true, false, DemoLogo);					
					RC.WriteLine("");

					#endregion

					#region Test Console Verbosity

					RC.WriteLine(); 

					RC.WriteLine(ConsoleVerbosity.Silent, ConsoleThemeColor.Text, "This will allways appear");
					RC.WriteLine(ConsoleVerbosity.Quiet, ConsoleThemeColor.Text1, "This will only appear when in vebose level is set to Quiet or higher");
					RC.WriteLine(ConsoleVerbosity.Minimal, ConsoleThemeColor.Text2, "This will only appear when in vebose level is set to Minimal or higher");
					RC.WriteLine(ConsoleVerbosity.Normal, ConsoleThemeColor.SubText2, "This will only appear when in vebose level is set to Normal or higher");
					RC.WriteLine(ConsoleVerbosity.Verbose, ConsoleThemeColor.Text3, "This will only appear when in vebose level is set to Verbose or higher");
					RC.WriteLine(ConsoleVerbosity.Debug, ConsoleThemeColor.SubText3, "This will only appear when in vebose level is set to Debug");

					RC.WriteLine(); 

					#endregion

					#region Write the values from the arguements
					
					// was the string argument defiend 
					if (StringArg.Defined == true)
					{
						// write its value
						RC.WriteLine(ConsoleThemeColor.TitleText3, "String argument was defined");
						CmdHelper.WriteInfoToConsole("String Value", StringArg.Value, RC.Theme[ConsoleThemeColor.SubTextGood], true);
						RC.WriteLine(); 
					}
					else
					{
						// was not defiend 
						RC.WriteLine(ConsoleThemeColor.TitleText2, "String argument was not defined");
						RC.WriteLine(); 
					}

					// was the string list argument defiend 
					if (StringListArg.Defined == true)
					{
						RC.WriteLine(ConsoleThemeColor.TitleText3, "String list argument was defined");
						
						int i = 1; 
						foreach (string value in StringListArg.Value)
						{
							// write its value                        
							CmdHelper.WriteInfoToConsole((i++).ToString(), value, RC.Theme[ConsoleThemeColor.SubTextGood], true);
						}

						RC.WriteLine(); 
					}
					else
					{
						// was not defiend 
						RC.WriteLine(ConsoleThemeColor.TitleText2, "String list argument was not defined");
						RC.WriteLine(); 
					}


					// was the string csv argument defiend 
					if (CsvArg.Defined == true)
					{
						RC.WriteLine(ConsoleThemeColor.TitleText3, "String CSV argument was defined");
					   
						int i = 1;
						foreach (string value in CsvArg.Value)
						{
							// write its value                        
							CmdHelper.WriteInfoToConsole((i++).ToString(), value, RC.Theme[ConsoleThemeColor.SubTextGood], true);
						}

						RC.WriteLine(); 
					}
					else
					{
						// was not defiend 
						RC.WriteLine(ConsoleThemeColor.TitleText2, "String CSV argument was not defined");
						RC.WriteLine(); 
					}
					
					// was the optional string argument defiend 
					if (OptionalArg.Defined == true)
					{
						RC.WriteLine(ConsoleThemeColor.TitleText3, "Optional string argument was defined");

						if (OptionalArg.Value == null)
						{
							RC.WriteLine(ConsoleThemeColor.TextGood, "Optional string had no optional value");
						}
						else
						{
							CmdHelper.WriteInfoToConsole("Value", StringArg.Value, RC.Theme[ConsoleThemeColor.SubTextGood], true);
						}

						RC.WriteLine(); 
					}
					else
					{
						// was not defiend 
						RC.WriteLine(ConsoleThemeColor.TitleText2, "Optional string argument was not defined");
						RC.WriteLine(); 
					}

					#endregion

					#region Test Exception

					RC.WriteLine(); 
					RC.WriteLine(ConsoleThemeColor.TitleText3, "Next is a test exception");
					RC.WriteLine(); 

					throw new Exception("This exception is a test to show how it gets formatted");

					#endregion
				}
			}
			catch (Exception ex)
			{
				// Write the exception to the console 
				RC.WriteException(0001, ex);
			}
			finally
			{
				RC.PromptForKey("Press any key to exit..", true, false);

				// Reset the color state
				RC.ColorState = state;
			}
		}
	}
}
