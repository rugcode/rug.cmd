﻿/* 
 * ApplicationExample part of Rugland Console Framework
 * 
 * THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
 * EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * Copyright (C) 2008 Phill Tew. All rights reserved.
 * 
 */

using System;
using Rug.Cmd;
using Rug.Cmd.Colors;

namespace TextFormatExample
{
	class Program
	{
		static string TextBlock = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus dictum tristique turpis, " + 
								  "consectetur venenatis quam tincidunt vitae. Cras ornare cursus nulla, vitae fringilla risus viverra non. " + 
								  "Mauris hendrerit feugiat ante, nec dictum massa dignissim ac. Morbi non orci sit amet erat posuere sagittis. " + 
								  "Maecenas viverra viverra felis in bibendum. Aliquam molestie suscipit augue, in eleifend tellus sodales quis. " + 
								  "Aenean dignissim, nisl et feugiat auctor, magna erat auctor urna, sit amet venenatis enim leo id justo. " + 
								  "Aenean condimentum velit ac nisl condimentum auctor. Nulla imperdiet velit in orci gravida facilisis sit amet a augue. " + 
								  "Proin nisi nunc, viverra vel luctus venenatis, faucibus eget ligula. Sed risus purus, dapibus sit amet placerat sed, " + 
								  "tempor vulputate dolor. Nullam pulvinar feugiat vestibulum. Duis tincidunt, dolor vel pretium facilisis, " + 
								  "lectus libero volutpat nisi, quis tincidunt neque elit eget nunc. Nulla malesuada tempus diam a egestas. " + 
								  "Donec at ligula eu nisl dignissim sollicitudin.";

		static void Main(string[] args)
		{
			// stash the color state
			ConsoleColorState state = RC.ColorState;

			try 
			{
				// uncomment these next 2 lines to demo this app in other background colors
				//RC.BackgroundColor = ConsoleColorExt.DarkRed;
				//Console.Clear(); 

				// set the color theme
				RC.Theme = ConsoleColorTheme.Load(ConsoleColorDefaultThemes.Colorful);

				CmdHelper.WriteSimpleBanner("Text Formatting Example", ' ', RC.Theme[ConsoleThemeColor.AppBackground], RC.Theme[ConsoleThemeColor.TitleText]);
				RC.WriteLine();

				CmdHelper.WriteSimpleBanner("Large text block using theme colors and padding", ' ', RC.Theme[ConsoleThemeColor.AppBackground], RC.Theme[ConsoleThemeColor.TitleText]);
				RC.WriteInterpreted(GetTextBlockUsingThemes(), 2, 2);

				CmdHelper.WriteSimpleBanner("Large text block using explicit colors and no padding", ' ', RC.Theme[ConsoleThemeColor.AppBackground], RC.Theme[ConsoleThemeColor.TitleText]);
				RC.WriteInterpreted(GetTextBlockUsingExplicitColors());
			}
			catch (Exception ex)
			{
				// Write the exception to the console 
				RC.WriteException(0001, ex);
			}
			finally
			{
				RC.PromptForKey("Press any key to exit..", true, false);

				// Reset the color state
				RC.ColorState = state;
			}
		}

		private static string GetTextBlockUsingThemes()
		{
			ConsoleFormatter text = new ConsoleFormatter();
			text.WriteLine(); 

			text.Write(ConsoleThemeColor.TextBad, "Colorized ");
			text.Write(ConsoleThemeColor.SubTextBad, "text ");
			text.Write(ConsoleThemeColor.TextNutral, "is ");
			text.Write(ConsoleThemeColor.SubTextNutral, "possible ");
			text.Write(ConsoleThemeColor.TextGood, "on ");
			text.Write(ConsoleThemeColor.SubTextGood, "a ");
			text.Write(ConsoleThemeColor.PromptColor1, "single ");
			text.Write(ConsoleThemeColor.PromptColor2, "line");
			text.WriteLine();
			text.WriteLine(); 

			text.WriteLine(ConsoleThemeColor.TitleText1, "Test text block 1 using color themes");
			text.WriteLine(ConsoleThemeColor.Text1, TextBlock);
			text.WriteLine();
			text.WriteLine(); 

			text.WriteLine(ConsoleThemeColor.TitleText2, "Test text block 2 using color themes");
			text.WriteLine(ConsoleThemeColor.Text2, TextBlock);
			text.WriteLine();
			text.WriteLine(); 

			return text.ToString(); 
		}

		public static string GetTextBlockUsingExplicitColors()
		{
			ConsoleFormatter text = new ConsoleFormatter();
			text.WriteLine(); 

			text.Write(ConsoleColorExt.Red, "Colorized ");
			text.Write(ConsoleColorExt.DarkRed, "text ");
			text.Write(ConsoleColorExt.Yellow, "is ");
			text.Write(ConsoleColorExt.DarkYellow, "possible ");
			text.Write(ConsoleColorExt.Green, "on ");
			text.Write(ConsoleColorExt.DarkGreen, "a ");
			text.Write(ConsoleColorExt.White, "single ");
			text.Write(ConsoleColorExt.Blue, "line");
			text.WriteLine();
			text.WriteLine(); 

			text.WriteLine(ConsoleColorExt.White, "Test text block 1 using explicit colors");
			text.WriteLine(ConsoleColorExt.DarkGreen, TextBlock);
			text.WriteLine();
			text.WriteLine(); 

			text.WriteLine(ConsoleColorExt.Cyan, "Test text block 2 using explicit colors");
			text.WriteLine(ConsoleColorExt.DarkCyan, TextBlock);
			text.WriteLine();
			text.WriteLine(); 

			return text.ToString();  
		}
	}
}
