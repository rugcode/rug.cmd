﻿/* 
 * Rug.Cmd
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;

namespace Rug.Cmd
{
    #region Enum Definitions 

    /// <summary>
    /// Unicode shaded chars
    /// </summary>
    public enum ConsoleShade 
    { 
        /// <summary>
        /// Clear shade
        /// </summary>
        Clear = 0, 
        /// <summary>
        /// Lightly dithered shade
        /// </summary>
        Light = 1, 
        /// <summary>
        /// Dimly dithered shade 
        /// </summary>
        Dim = 2, 
        /// <summary>
        /// Darkly dithered shade 
        /// </summary>
        Dark = 3, 
        /// <summary>
        /// Totaly opaque shade 
        /// </summary>
        Opaque = 4, 
        /// <summary>
        /// Shade with the top half opaque and the bottom half clear
        /// </summary>
        HalfTop = 5,
        /// <summary>
        /// Shade with the bottom half opaque and the top half clear
        /// </summary>
        HalfBottom = 6,
        /// <summary>
        /// Shade with the left half opaque and the right half clear
        /// </summary>
        HalfLeft = 7,
        /// <summary>
        /// Shade with the right half opaque and the left half clear
        /// </summary>
        HalfRight = 8 
    }
    
    /// <summary>
    /// Unicode arrows
    /// </summary>
    public enum ConsoleArrows 
    { 
        Up = 0, 
        Down = 1, 
        Left = 2, 
        Right = 3 
    }

    /// <summary>
    /// For easy lookup of extra unicode maths chars (TODO: add more!)
    /// </summary>
    public enum ConsoleMathsChars 
    { 
        /// <summary>
        /// ± (Plus-Minus) 
        /// </summary>
        PlusMinus = 0 
    }

    /// <summary>
    /// Unicode line styles
    /// </summary>
    public enum ConsoleLineStyle
    {
        /// <summary>
        /// No lines 
        /// </summary>
        None,
        /// <summary>
        /// Single lines
        /// </summary>
        Single,
        /// <summary>
        ///  Double lines 
        /// </summary>
        Double,
        /// <summary>
        /// Block Lines
        /// </summary>
        Block
    }

    /// <summary>
    /// Unicode line char lookup 
    /// </summary>
    public enum LineChars
    {
        TopLeft = 0, TopCenter = 1, TopRight = 2,
        MidLeft = 3, MidCenter = 4, MidRight = 5,
        BottomLeft = 6, BottomCenter = 7, BottomRight = 8
    }

    /// <summary>
    /// Not implemented yet
    /// </summary>
    public enum ConsoleDrawMode { Replace, Overlay }

    #endregion 

    public static class ConsoleChars
    {        
        #region Static defines

        public static readonly string[] LineEndSplits = new string[] { Environment.NewLine, "\n" };

        public static readonly char[] SingleLines = new char[] 
                        {
                            '\u250C', '\u2500', '\u2510', 
                            '\u2502', ' ',      '\u2502', 
                            '\u2514', '\u2500', '\u2518'
                        };

        public static readonly char[] DoubleLines = new char[] 
                        {
                            '\u2554', '\u2550', '\u2557',
                            '\u2551', ' ',      '\u2551',
                            '\u255A', '\u2550', '\u255D'
                        };

        public static readonly char[] Shades = new char[] 
                        {
                            ' ',      // clear
                            '\u2591', // light 
                            '\u2592', // dim 
                            '\u2593', // dark 
                            '\u2588', // opaque
                            '\u2580', // half (top)
                            '\u2584', // half (bottom)
                            '\u258c', // half (left)
                            '\u2590'  // half (right)

                        };
        
        public static readonly char[] Arrows = new char[] 
                        {
                            '\u25b2', // up arrow
                            '\u25bC', // down arrow
                            '\u25c4', // left arrow
                            '\u25bA'  // right arrow
                        };

        #endregion

        /// <summary>
        /// Get unicode line chars 
        /// </summary>
        /// <param name="style">line style</param>
        /// <param name="edge">the edge to get</param>
        /// <returns>the unicode char for the line style and egde</returns>
        public static char GetLineChar(ConsoleLineStyle style, LineChars edge)
        {
            switch (style)
            {
                case ConsoleLineStyle.None:
                    return ' '; 
                case ConsoleLineStyle.Single:
                    return SingleLines[(int)edge];
                case ConsoleLineStyle.Double:
                    return DoubleLines[(int)edge];
                case ConsoleLineStyle.Block:
                    return Shades[(int)ConsoleShade.Opaque];
                default:
                    return ' '; 
            }            
        }

        /// <summary>
        /// Gets special Maths unicode chars 
        /// </summary>
        /// <param name="char">the special char to get</param>
        /// <returns>The unicode char.</returns>
        public static char GetMathsChar(ConsoleMathsChars @char)
        {
            switch (@char)
            {
                case ConsoleMathsChars.PlusMinus:
                    return '\u00b1';
                default:
                    return '?';
            }            
        }

        /// <summary>
        /// Get unicode arrow chars
        /// </summary>
        /// <param name="arrow">arrow direction</param>
        /// <returns>unicode char</returns>
        public static char GetArrow(ConsoleArrows arrow)
        {
            return Arrows[(int)arrow];
        }

        /// <summary>
        /// Get unicode shade chars
        /// </summary>
        /// <param name="shade">shade</param>
        /// <returns>unicode char</returns>
        public static char GetShade(ConsoleShade shade)
        {
            return Shades[(int)shade];
        }
    }
}
