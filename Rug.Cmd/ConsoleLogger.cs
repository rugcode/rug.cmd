﻿/* 
 * Rug.Cmd
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Rug.Cmd
{
	public class ConsoleLogger : IConsole, IDisposable 
	{		
		#region Private Members
		
		private object m_Lock = new object();

		private ConsoleFormatter m_Formatter = new ConsoleFormatter();

		private bool m_StripFormatting = true;
		private bool m_EchoToInnerConsole = true;		
		private  IConsole m_InnerConsole; 

		private string m_Title; 
		private int m_BufferWidth;
		private int m_BufferHeight;

		private Colors.ConsoleColorTheme m_Theme;
		private ConsoleColorExt m_ForegroundColor;
		private ConsoleColorExt m_BackgroundColor;
		private ConsoleColorState m_OriginalState;

		private string m_FilePath;
		private FileStream m_Stream;
		private StreamWriter m_Writer;
		private bool m_AllwaysWriteStackTrace = true;

		#endregion		

		#region Public Events And Properties

		public string FilePath { get { return m_FilePath; } }

		public IConsole InnerConsole 
		{ 
			get
			{
				lock (m_Lock)
				{
					return m_InnerConsole;
				}
			} 
			set
			{
				lock (m_Lock)
				{
					m_InnerConsole = value;

					if (m_InnerConsole == null)
					{
						m_InnerConsole = RC.Sys;
					}
				}
			}
		}

		/// <summary>
		/// Should console messages be echoed to the system console
		/// </summary>
		public bool EchoToInnerConsole { get { return m_EchoToInnerConsole; } set { m_EchoToInnerConsole = value; } }

		public bool StripFormatting { get { return m_StripFormatting; } }

		public bool AllwaysWriteStackTrace { get { return m_AllwaysWriteStackTrace; } set { m_AllwaysWriteStackTrace = value; } }

		#endregion

		public ConsoleLogger(string filePath)
			: this(RC.Sys, filePath, false)
		{

		}

		public ConsoleLogger(string filePath, bool stripFormatting)
			: this(RC.Sys, filePath, stripFormatting)
		{

		}

		public ConsoleLogger(IConsole inner, string filePath, bool stripFormatting)
			: this(inner, filePath, stripFormatting, true)
		{

		}

		public ConsoleLogger(IConsole inner, string filePath, bool stripFormatting, bool echoToInner)
		{
			m_StripFormatting = stripFormatting;
			InnerConsole = inner;

			m_Title = m_InnerConsole.Title;
			m_BufferWidth = m_InnerConsole.BufferWidth;
			m_BufferHeight = m_InnerConsole.BufferHeight;
			m_Theme = m_InnerConsole.Theme;
			m_OriginalState = m_InnerConsole.ColorState;
			m_ForegroundColor = m_InnerConsole.ForegroundColor;
			m_BackgroundColor = m_InnerConsole.BackgroundColor;

			m_FilePath = filePath;

			m_EchoToInnerConsole = echoToInner; 

			m_Stream = new FileStream(m_FilePath, FileMode.Create, FileAccess.Write);
			m_Writer = new StreamWriter(m_Stream);

			WriteLine(ConsoleVerbosity.Silent, ConsoleThemeColor.TitleText, "Log file open " + DateTime.Now.ToString());
		}
		
		public void Dispose()
		{
			if (m_Writer != null)
			{
				m_Writer.Dispose();
				m_Writer = null;
			}

			if (m_Stream != null)
			{
				m_Stream.Close();
				m_Stream = null;
			}
		}

		#region Internal Buffer Write Methods

		private void FileWriteLine(string line)
		{
			m_Writer.WriteLine(line);
		}

		private void FileWrite(string line)
		{
			m_Writer.Write(line);
		}

		private void FileFlush()
		{
			m_Writer.Flush();
		}

		private void BufferFlush()
		{
			if (m_Formatter.Length > 0)
			{
				if (m_StripFormatting == true)
				{
					FileWrite(ConsoleFormatter.StripFormat(m_Formatter.ToString()));
				}
				else
				{
					FileWrite(m_Formatter.ToString());
				}

				m_Formatter.Clear();

				FileFlush(); 
			}
		}

		private void BufferWriteFormmatted(string formatted)
		{
			BufferFlush();

			if (m_StripFormatting == true)
			{
				FileWrite(ConsoleFormatter.StripFormat(formatted));
			}
			else
			{
				FileWrite(formatted);
			}
		}

		private void BufferWrite(ConsoleColorExt color, string str)
		{
			if (color == ConsoleColorExt.Inhreit)
			{
				color = m_ForegroundColor;
			}

			m_Formatter.Write(color, str);
		}

		private void BufferWrite(string str)
		{
			m_Formatter.Write(m_ForegroundColor, str);
		}

		private void BufferWriteLine(string str)
		{
			m_Formatter.WriteLine(m_ForegroundColor, str);

			BufferFlush();
		}

		private void BufferWriteLine(ConsoleColorExt color, string str)
		{
			if (color == ConsoleColorExt.Inhreit)
			{
				color = m_ForegroundColor;
			}

			m_Formatter.WriteLine(color, str);

			BufferFlush();
		}

		private void BufferWriteWrapped(ConsoleColorExt colour, string message, int paddingLeft, int paddingRight)
		{
			int maxWidth = BufferWidth - (paddingLeft + paddingRight);

			List<string> lines = new List<string>(message.Split(new string[] { Environment.NewLine, "\n" }, StringSplitOptions.None));

			string prefix = new string(' ', paddingLeft);

			foreach (string line in lines)
			{
				string str = line;

				if (str.Length == 0)
				{
					BufferWriteLine("");
				}
				else
				{
					while (str.Length > 0)
					{
						if (str.Length > maxWidth)
						{
							BufferWriteLine(colour, prefix + str.Substring(0, maxWidth));
							str = str.Substring(maxWidth);
						}
						else
						{
							BufferWriteLine(colour, prefix + str);
							str = "";
						}
					}
				}
			}
		}

		private void BufferWriteStackTrace(string trace)
		{
			if (String.IsNullOrEmpty(trace) == false)
			{
				BufferWriteLine("");
				BufferWriteLine(Theme[ConsoleThemeColor.ErrorColor2], new string(ConsoleChars.GetShade(ConsoleShade.Dim), BufferWidth));
				BufferWriteLine(Theme[ConsoleThemeColor.TitleText], "  " + trace.Replace("\n", "\n  "));
				BufferWriteLine(Theme[ConsoleThemeColor.ErrorColor2], new string(ConsoleChars.GetShade(ConsoleShade.Dim), BufferWidth));
				BufferWriteLine("");
			}
		}

		private void BufferWriteMessage(ConsoleMessage type, ConsoleColorExt colour, string str)
		{
			BufferWriteMessage(type, colour, 0, str);
		}

		private void BufferWriteMessage(ConsoleMessage type, ConsoleThemeColor colour, string str)
		{
			BufferWriteMessage(type, colour, 0, str);
		}

		private void BufferWriteMessage(ConsoleMessage type, ConsoleColorExt colour, int errorId, string str)
		{
			BufferWriteMessage(type, colour, errorId, null, 0, 0, str);
		}

		private void BufferWriteMessage(ConsoleMessage type, ConsoleColorExt colour, int errorId, string sourceFile, int line, int character, string str)
		{
			string realString = str;

			if (IsBuildMode == true)
			{
				string reportType = "";

				if (type == ConsoleMessage.Warning)
				{
					if (WarningsAsErrors == true)
						reportType = "error";
					else
						reportType = "warning";
				}
				else if (type == ConsoleMessage.Prompt)
				{
					if ((this as IConsole).WarningsAsErrors)
						reportType = "error";
					else
						reportType = "warning";
				}
				else
				{
					reportType = "error"; // type.ToString().ToLower();
				}

				string sourceString = null;

				if (sourceFile == null)
				{
					sourceString = Application.ExecutablePath;
				}
				else
				{
					sourceString = sourceFile + "(" + line + "," + character + ")";
				}

				if (reportType != "warning" || (reportType == "warning" && ReportWarnings == true))
				{
					realString = sourceString + ": " + reportType + " " + ApplicationBuildReportPrefix + errorId.ToString().PadLeft(4, '0') + ": " + str.Replace("\n", " ");
				}
			}

			if (CanManipulateBuffer == true)
			{
				if (CursorLeft > 0)
				{
					BufferWriteLine("");
				}

				BufferWriteLine(colour, realString);
			}
			else
			{
				BufferWriteLine(colour, realString);
			}
		}

		private void BufferWriteMessage(ConsoleMessage type, ConsoleThemeColor colour, int errorId, string str)
		{
			BufferWriteMessage(type, colour, errorId, null, 0, 0, str);
		}

		private void BufferWriteMessage(ConsoleMessage type, ConsoleThemeColor colour, int errorId, string sourceFile, int line, int character, string str)
		{
			BufferWriteMessage(type, Theme[colour], errorId, sourceFile, line, character, str);
		}

		#region Buffer Write Interpreted

		private void BufferWriteInterpreted(string buffer)
		{
			BufferWriteInterpreted(buffer, 0, 0);
		}

		private void BufferWriteInterpreted(ConsoleColorExt colour, string buffer, int paddingLeft, int paddingRight)
		{
			ConsoleColorExt col = ForegroundColor;

			if (colour != ConsoleColorExt.Inhreit)
			{
				m_ForegroundColor = colour;
			}

			BufferWriteInterpreted(buffer, paddingLeft, paddingRight);

			m_ForegroundColor = col;
		}

		private void BufferWriteInterpreted(string buffer, int paddingLeft, int paddingRight)
		{
			// It is unfortunate that this method must be internalised to this class from ConsoleFormatter
			// i would hope that i could move it back. 

			int maxWidth = BufferWidth - (paddingLeft + paddingRight);

			string prefix = new string(' ', paddingLeft);

			int lastIndex = 0;
			Stack<ConsoleColorExt> stack = new Stack<ConsoleColorExt>();

			foreach (Match match in ConsoleFormatter.FormatRegex.Matches(buffer))
			{
				if (match.Groups["Tag"].Success)
				{
					// start tag 
					// parse the tags inner value
					// e.g. c:# (where # is the name or ID of the colour to use (From ConsoleColourExt) 
					// add the parsed colour to the stack 

					ConsoleColorExt col = ConsoleFormatter.ParseColour(match.Groups["Inner"].Value, Theme);

					stack.Push(m_ForegroundColor);

					m_ForegroundColor = col;
				}
				else if (match.Groups["EndTag"].Success)
				{
					// end tag 
					// handle stack changes
					if (stack.Count >= 0)
					{
						m_ForegroundColor = stack.Pop();
					}
					else
					{
						throw new Exception(string.Format(Strings.ConsoleInterpreter_UnexpectedEndTag, match.Index));
					}
				}
				else if (match.Groups["Text"].Success)
				{
					string wholeMessage = ConsoleFormatter.UnescapeString(match.Value);

					List<string> lines = ConsoleFormatter.SplitLinebreaks(wholeMessage);

					foreach (string line in lines)
					{
						string str = line;

						if (str == "\n" || str == Environment.NewLine)
						{
							BufferWriteLine("");
							lastIndex = 0;
						}
						else
						{

							while (str.Length > 0)
							{
								if (lastIndex + str.Length > maxWidth)
								{
									int lastWord = str.LastIndexOf(' ', maxWidth - lastIndex, maxWidth - lastIndex);

									if (lastWord <= 0)
									{
										lastWord = maxWidth - lastIndex;
									}

									string toRender;

									if (lastIndex > 0)
									{
										toRender = str.Substring(0, lastWord);
									}
									else
									{
										toRender = prefix + str.Substring(0, lastWord);
									}

									if (BufferWidth == lastIndex + toRender.Length)
									{
										BufferWrite(toRender);
									}
									else
									{
										BufferWriteLine(toRender);
									}

									str = str.Substring(lastWord + 1);

									lastIndex = 0;
								}
								else
								{
									string toRender = str;

									if (lastIndex <= 0)
									{
										toRender = prefix + str;
									}

									BufferWrite(toRender);

									lastIndex += str.Length;

									str = "";
								}
							}
						}
					}
				}
			}
		}

		private void BufferWriteInterpretedLine(string buffer)
		{
			BufferWriteInterpreted(buffer + Environment.NewLine);
		}

		#endregion

		#endregion

		#region IConsole Members

		public string ApplicationBuildReportPrefix
		{
			get
			{
				return m_InnerConsole.ApplicationBuildReportPrefix;
			}
			set
			{
				m_InnerConsole.ApplicationBuildReportPrefix = value;
			}
		}

		public ConsoleVerbosity Verbosity
		{
			get
			{
				return m_InnerConsole.Verbosity;
			}
			set
			{
				m_InnerConsole.Verbosity = value;
			}
		}

		public bool DefaultPromptAnswer
		{
			get
			{
				return m_InnerConsole.DefaultPromptAnswer;
			}
			set
			{
				m_InnerConsole.DefaultPromptAnswer = value;
			}
		}

		public bool UseDefaultPromptAnswer
		{
			get
			{
				return m_InnerConsole.UseDefaultPromptAnswer;
			}
			set
			{
				m_InnerConsole.UseDefaultPromptAnswer = value;
			}
		}

		public bool WarningsAsErrors
		{
			get
			{
				return m_InnerConsole.WarningsAsErrors;
			}
			set
			{
				m_InnerConsole.WarningsAsErrors = value;
			}
		}

		public bool ReportWarnings
		{
			get
			{
				return m_InnerConsole.ReportWarnings;
			}
			set
			{
				m_InnerConsole.ReportWarnings = value;
			}
		}

		public bool IsBuildMode
		{
			get
			{
				return m_InnerConsole.IsBuildMode;
			}
			set
			{
				m_InnerConsole.IsBuildMode = value;
			}
		}

		public bool CanManipulateBuffer
		{
			get { return false; }
		}

		public int BufferWidth
		{
			get
			{
				return m_BufferWidth;
			}
			set
			{
				lock (m_Lock)
				{
					m_BufferWidth = value;
				}
			}
		}

		public int BufferHeight
		{
			get
			{
				return m_BufferHeight;
			}
			set
			{
				lock (m_Lock)
				{
					m_BufferHeight = value;
				}
			}
		}

		public int CursorLeft
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		public int CursorTop
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		public int CursorSize
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		public string Title
		{
			get
			{
				return m_Title; 
			}
			set
			{
				m_Title = value; 
			}
		}

		public Colors.ConsoleColorTheme Theme
		{
			get
			{
				return m_Theme; 
			}
			set
			{
				lock (m_Lock)
				{
					m_Theme = value;
				}
			}
		}

		public ConsoleColorExt ForegroundColor
		{
			get
			{
				return m_ForegroundColor;
			}
			set
			{
				lock (m_Lock)
				{
					m_ForegroundColor = value;
				}
			}
		}

		public ConsoleColorExt BackgroundColor
		{
			get
			{
				return m_BackgroundColor;
			}
			set
			{
				lock (m_Lock)
				{
					m_BackgroundColor = value;
				}
			}
		}

		public ConsoleThemeColor ForegroundThemeColor
		{
			set { ForegroundColor = m_Theme[value]; }
		}

		public ConsoleThemeColor BackgroundThemeColor
		{
			set { BackgroundColor = m_Theme[value]; }
		}

		public ConsoleColorState ColorState
		{
			get 
			{ 
				return new ConsoleColorState(ForegroundColor, BackgroundColor); 
			}
			set
			{
				lock (m_Lock)
				{
					m_ForegroundColor = value.ForegroundColor;
					m_BackgroundColor = value.BackgroundColor;
				}
			}
		}

		public void ResetColor()
		{
			ColorState = m_OriginalState; 
		}

		public void SetCursorPosition(int left, int top)
		{
			throw new NotImplementedException();
		}

		public void Clear()
		{
			m_InnerConsole.Clear(); 
		}

		public bool KeyAvailable
		{
			get { throw new NotImplementedException(); }
		}

		public int Read()
		{
			throw new NotImplementedException();
		}

		public ConsoleKeyInfo ReadKey()
		{
			throw new NotImplementedException();
		}

		public ConsoleKeyInfo ReadKey(bool intercept)
		{
			throw new NotImplementedException();
		}

		public string ReadLine()
		{
			throw new NotImplementedException();
		}

		public ConsoleKeyInfo PromptForKey(string message, bool intercept, bool throwException)
		{
			throw new NotImplementedException();
		}

		public string PromptForLine(string message, bool throwException)
		{
			throw new NotImplementedException();
		}

		public bool ShouldWrite(ConsoleVerbosity verbosity)
		{
			return (int)verbosity <= (int)Verbosity;
		}

		public void Write(string str)
		{
			lock (m_Lock)
			{
				if (ShouldWrite(ConsoleVerbosity.Normal) == true)
				{
					BufferWrite(str);
				}

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.Write(str);
				}
			}
		}

		public void Write(ConsoleVerbosity level, string str)
		{
			lock (m_Lock)
			{
				if (ShouldWrite(level) == true)
				{					
					BufferWrite(str);
				}

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.Write(level, str);
				}
			}
		}

		public void Write(ConsoleVerbosity level, ConsoleColorExt colour, string str)
		{
			lock (m_Lock)
			{
				if (ShouldWrite(level) == true)
				{
					BufferWrite(colour, str);
				}

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.Write(level, colour, str);
				}
			}
		}

		public void Write(ConsoleVerbosity level, ConsoleThemeColor colour, string str)
		{
			lock (m_Lock)
			{
				if (ShouldWrite(level) == true)
				{
					BufferWrite(Theme[colour], str);
				}

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.Write(level, colour, str);
				}
			}
		}

		public void Write(ConsoleColorExt colour, string str)
		{
			lock (m_Lock)
			{
				if (ShouldWrite(ConsoleVerbosity.Normal) == true)
				{
					BufferWrite(colour, str);
				}
				
				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.Write(colour, str);
				}
			}
		}

		public void Write(ConsoleThemeColor colour, string str)
		{
			lock (m_Lock)
			{
				if (ShouldWrite(ConsoleVerbosity.Normal) == true)
				{
					BufferWrite(Theme[colour], str);
				}

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.Write(colour, str);
				}
			}
		}

		public void WriteLine(ConsoleVerbosity level)
		{
			lock (m_Lock)
			{
				if (ShouldWrite(level) == true)
				{
					BufferWriteLine("");
				}

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteLine(level);
				}
			}
		}

		public void WriteLine()
		{
			lock (m_Lock)
			{
				if (ShouldWrite(ConsoleVerbosity.Normal) == true)
				{
					BufferWriteLine("");
				}

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteLine();
				}
			}
		}

		public void WriteLine(string str)
		{
			lock (m_Lock)
			{
				if (ShouldWrite(ConsoleVerbosity.Normal) == true)
				{
					BufferWriteLine(str);
				}

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteLine(str);
				}
			}
		}

		public void WriteLine(ConsoleVerbosity level, string str)
		{
			lock (m_Lock)
			{
				if (ShouldWrite(level) == true)
				{
					BufferWriteLine(str);
				}

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteLine(level, str);
				}
			}
		}

		public void WriteLine(ConsoleVerbosity level, ConsoleColorExt colour, string str)
		{
			lock (m_Lock)
			{
				if (ShouldWrite(level) == true)
				{
					BufferWriteLine(colour, str);
				}

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteLine(level, colour, str);
				}
			}
		}

		public void WriteLine(ConsoleColorExt colour, string str)
		{
			lock (m_Lock)
			{
				if (ShouldWrite(ConsoleVerbosity.Normal) == true)
				{
					BufferWriteLine(colour, str);
				}	

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteLine(colour, str);
				}
			}
		}

		public void WriteLine(ConsoleVerbosity level, ConsoleThemeColor colour, string str)
		{
			lock (m_Lock)
			{
				if (ShouldWrite(level) == true)
				{
					BufferWriteLine(Theme[colour], str);
				}

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteLine(level, colour, str);
				}
			}
		}

		public void WriteLine(ConsoleThemeColor colour, string str)
		{
			lock (m_Lock)
			{
				if (ShouldWrite(ConsoleVerbosity.Normal) == true)
				{
					BufferWriteLine(Theme[colour], str);
				}	

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteLine(colour, str);
				}
			}
		}

		public void WriteWrapped(ConsoleColorExt colour, string message, int paddingLeft, int paddingRight)
		{
			lock (m_Lock)
			{
				BufferWriteWrapped(colour, message, paddingLeft, paddingRight);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteWrapped(colour, message, paddingLeft, paddingRight);
				}
			}
		}

		public void WriteWrapped(ConsoleThemeColor colour, string message, int paddingLeft, int paddingRight)
		{
			lock (m_Lock)
			{
				BufferWriteWrapped(Theme[colour], message, paddingLeft, paddingRight);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteWrapped(colour, message, paddingLeft, paddingRight);
				}
			}
		}

		public void WritePrompt(string str)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(ConsoleMessage.Prompt, ConsoleThemeColor.PromptColor1, str);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WritePrompt(str);
				}
			}
		}

		public void WritePrompt(ConsoleColorExt colour, string str)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(ConsoleMessage.Prompt, colour, str);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WritePrompt(colour, str);
				}
			}
		}

		public void WritePrompt(ConsoleThemeColor colour, string str)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(ConsoleMessage.Prompt, colour, str);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WritePrompt(colour, str);
				}
			}
		}

		public void WriteError(int id, string str)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(ConsoleMessage.Error, ConsoleThemeColor.ErrorColor1, id, str);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteError(id, str);
				}
			}
		}

		public void WriteError(int id, string sourceFile, int line, int character, string str)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(ConsoleMessage.Error, ConsoleThemeColor.ErrorColor1, id, sourceFile, line, character, str);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteError(id, sourceFile, line, character, str);
				}
			}
		}

		public void WriteError(ConsoleColorExt colour, int id, string str)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(ConsoleMessage.Error, colour, id, str);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteError(colour, id, str);
				}
			}
		}

		public void WriteError(ConsoleThemeColor colour, int id, string str)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(ConsoleMessage.Error, colour, id, str);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteError(colour, id, str);
				}
			}
		}

		public void WriteWarning(int id, string str)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(ConsoleMessage.Warning, ConsoleThemeColor.WarningColor1, id, str);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteWarning(id, str);
				}
			}
		}

		public void WriteWarning(ConsoleColorExt colour, int id, string str)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(ConsoleMessage.Warning, colour, id, str);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteWarning(colour, id, str);
				}
			}
		}

		public void WriteWarning(ConsoleThemeColor colour, int id, string str)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(ConsoleMessage.Warning, colour, id, str);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteWarning(colour, id, str);
				}
			}
		}

		public void WriteWarning(ConsoleColorExt colour, int id, string sourceFile, int line, int character, string str)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(ConsoleMessage.Warning, colour, id, sourceFile, line, character, str);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteWarning(colour, id, sourceFile, line, character, str);
				}
			}
		}

		public void WriteWarning(ConsoleThemeColor colour, int id, string sourceFile, int line, int character, string str)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(ConsoleMessage.Warning, colour, id, sourceFile, line, character, str);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteWarning(colour, id, sourceFile, line, character, str);
				}
			}
		}

		public void WriteException(int id, Exception ex)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(ConsoleMessage.Error, ConsoleThemeColor.ErrorColor1, id, ex.Message);

				if (Verbosity == ConsoleVerbosity.Debug || AllwaysWriteStackTrace)
				{
					BufferWriteStackTrace(ex.StackTrace);
				}

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteException(id, ex);
				}
			}
		}

		public void WriteException(int id, string sourceFile, int line, int character, Exception ex)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(ConsoleMessage.Error, ConsoleThemeColor.ErrorColor1, id, sourceFile, line, character, ex.Message);

				if (Verbosity == ConsoleVerbosity.Debug || AllwaysWriteStackTrace)
				{
					BufferWriteStackTrace(ex.StackTrace);
				}

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteException(id, sourceFile, line, character, ex);
				}
			}
		}

		public void WriteException(int id, string title, Exception ex)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(ConsoleMessage.Error, ConsoleThemeColor.ErrorColor1, id, title + Environment.NewLine + ex.Message);

				if (Verbosity == ConsoleVerbosity.Debug || AllwaysWriteStackTrace)
				{
					BufferWriteStackTrace(ex.StackTrace);
				}

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteException(id, title, ex);
				}
			}
		}

		public void WriteException(int id, string sourceFile, int line, int character, string title, Exception ex)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(ConsoleMessage.Error, ConsoleThemeColor.ErrorColor1, id, sourceFile, line, character, title + Environment.NewLine + ex.Message);

				if (Verbosity == ConsoleVerbosity.Debug || AllwaysWriteStackTrace)
				{
					BufferWriteStackTrace(ex.StackTrace);
				}

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteException(id, sourceFile, line, character, title, ex);
				}
			}
		}

		public void WriteStackTrace(string trace)
		{
			lock (m_Lock)
			{
				BufferWriteStackTrace(trace);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteStackTrace(trace);
				}
			}
		}

		public void WriteMessage(ConsoleMessage type, ConsoleColorExt colour, string str)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(type, colour, str);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteMessage(type, colour, str);
				}
			}
		}

		public void WriteMessage(ConsoleMessage type, ConsoleThemeColor colour, string str)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(type, colour, str);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteMessage(type, colour, str);
				}
			}
		}

		public void WriteMessage(ConsoleMessage type, ConsoleColorExt colour, int errorId, string str)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(type, colour, errorId, str);
				
				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteMessage(type, colour, errorId, str);
				}
			}
		}

		public void WriteMessage(ConsoleMessage type, ConsoleColorExt colour, int errorId, string sourceFile, int line, int character, string str)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(type, colour, errorId, sourceFile, line, character, str);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteMessage(type, colour, errorId, sourceFile, line, character, str);
				}
			}
		}

		public void WriteMessage(ConsoleMessage type, ConsoleThemeColor colour, int errorId, string str)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(type, colour, errorId, str);
				
				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteMessage(type, colour, errorId, str);
				}
			}
		}

		public void WriteMessage(ConsoleMessage type, ConsoleThemeColor colour, int errorId, string sourceFile, int line, int character, string str)
		{
			lock (m_Lock)
			{
				BufferWriteMessage(type, colour, errorId, sourceFile, line, character, str);
				
				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteMessage(type, colour, errorId, sourceFile, line, character, str);
				}
			}
		}

		public void WriteInterpreted(string buffer)
		{
			lock (m_Lock)
			{
				BufferWriteInterpreted(buffer);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteInterpreted(buffer);
				}
			}
		}

		public void WriteInterpreted(string buffer, int paddingLeft, int paddingRight)
		{
			lock (m_Lock)
			{
				BufferWriteInterpreted(buffer, paddingLeft, paddingRight);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteInterpreted(buffer, paddingLeft, paddingRight);
				}
			}
		}

		public void WriteInterpreted(ConsoleColorExt colour, string buffer, int paddingLeft, int paddingRight)
		{
			lock (m_Lock)
			{
				BufferWriteInterpreted(colour, buffer, paddingLeft, paddingRight);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteInterpreted(colour, buffer, paddingLeft, paddingRight);
				}
			}
		}

		public void WriteInterpreted(ConsoleThemeColor colour, string buffer, int paddingLeft, int paddingRight)
		{
			lock (m_Lock)
			{
				BufferWriteInterpreted(Theme[colour], buffer, paddingLeft, paddingRight);

				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteInterpreted(colour, buffer, paddingLeft, paddingRight);
				}
			}
		}

		public void WriteInterpretedLine(string buffer)
		{
			lock (m_Lock)
			{
				BufferWriteInterpretedLine(buffer);
				
				if (m_EchoToInnerConsole)
				{
					m_InnerConsole.WriteInterpretedLine(buffer);
				}
			}
		}

		#endregion
	}
}
