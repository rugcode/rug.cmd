﻿/* 
 * Rug.Cmd
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;
using Rug.Cmd.Colors;

namespace Rug.Cmd
{
    #region Enum Defines

    /// <summary>
    /// The wordiness of the console output
    /// </summary>
    public enum ConsoleVerbosity 
    { 
        /// <summary>
        /// Entirly silent. 
        /// This is a special mode to help compatibility with processed that require there to be no output. 
        /// The only exception begin errors and potential warning. 
        /// </summary>
        Silent = 0, 
        
		/// <summary>
        /// Minimal output. 
        /// </summary>
        Minimal = 1, 
        
		/// <summary>
        /// Quite output. 
        /// </summary>
        Quiet = 2, 
        
		/// <summary>
        /// Normal output. 
        /// This is the default level.
        /// </summary>
        Normal = 3, 
        
		/// <summary>
        /// Verbose output. 
        /// At this level the application should describe almost every action it makes. 
        /// </summary>
        Verbose = 4, 

        /// <summary>
        /// Debug level output. 
        /// At this level actions are described in detail. 
        /// Additionaly exception reports include stack traces and any inner exceptions. 
        /// </summary>
        Debug = 5 
    }
    
    /// <summary>
    /// Console message type
    /// </summary>
    public enum ConsoleMessage 
    { 
        /// <summary>
        /// User prompt message
        /// </summary>
        Prompt, 
        /// <summary>
        /// Error message. 
        /// When ConsoleExt.IsBuildMode is true error messages are formatted as MSVS errors. 
        /// </summary>
        Error, 
        /// <summary>
        /// Warning message. 
        /// When ConsoleExt.IsBuildMode is true error messages are formatted as MSVS warnings/errors.
        /// </summary>
        Warning, 
        /// <summary>
        /// Exception report. 
        /// When ConsoleExt.IsBuildMode is true exceptions are formatted as MSVS errors.
        /// When ConsoleExt.Verbosity is set to ConsoleVerbosity.Debug the report will include the stack trace and any inner exceptions.
        /// </summary>
        Exception 
    } 
    
    #endregion 

    /// <summary>
    /// Extented Console
    /// </summary>
    public static class ConsoleExt
    {
		private delegate void SetterInt(int value); 

		static ConsoleExt()
		{
			if (Helper.IsRunningOnMono == false)
			{
				MethodInfo setBufferWidth = typeof(Console).GetProperty("BufferWidth").GetSetMethod();
				m_SetBufferWidth = (SetterInt)Delegate.CreateDelegate(typeof(SetterInt), setBufferWidth);

				MethodInfo setBufferHeight = typeof(Console).GetProperty("BufferHeight").GetSetMethod();
				m_SetBufferHeight = (SetterInt)Delegate.CreateDelegate(typeof(SetterInt), setBufferWidth);
			}
			else
			{
				m_SetBufferWidth = null;
				m_SetBufferHeight = null; 
			}
		}

        #region Private Static Members

		private static readonly SetterInt m_SetBufferWidth;
		private static readonly SetterInt m_SetBufferHeight;

        private static bool m_FeaturesLoaded = false;        
        private static bool m_CanManipulateBuffer = false;        
        
        private static bool m_IsBuildMode = false;
        private static string m_ApplicationBuildReportPrefix = "RUG";

        private static ConsoleVerbosity m_Verbosity = ConsoleVerbosity.Normal;
        
        private static bool m_DefaultPromptAnswer = false;
        private static bool m_UseDefaultPromptAnswer = false;
        private static bool m_WarningsAsErrors = false;
        private static bool m_ReportWarnings = true;

        private static int m_BufferWidth = -1;
        private static int m_BufferHeight = -1;

        private static ConsoleColorTheme m_Theme;

        #endregion 

        #region Ensure Console Features

        private static void EnsureConsoleFeatures()
        {
            if (!m_FeaturesLoaded)
            {
                GetConsoleFeatures(); 
            }
        }

        private static void GetConsoleFeatures()
        {
            try
            {
                m_BufferWidth = Console.BufferWidth;
                m_BufferHeight = Console.BufferHeight;
                m_CanManipulateBuffer = true;
            }
            catch
            {
                m_BufferWidth = 82;
                m_BufferHeight = 1024;
                m_CanManipulateBuffer = false;
            }
            finally
            {
                m_FeaturesLoaded = true; 
            }
        }

        #endregion 

        #region Public Static Properties

        #region Options

        /// <summary>
        /// Gets or sets the prefix for errors and warnings. 
        /// To be used in conjunction with ConsoleExt.IsBuildMode.
        /// </summary>
        public static string ApplicationBuildReportPrefix 
        { 
            get { return m_ApplicationBuildReportPrefix; } 
            set 
            {
                string prefix = value;

                if (prefix == null ||
                    prefix.Trim().Length == 0)
                {
                    prefix = "RUG"; 
                }

                m_ApplicationBuildReportPrefix = prefix.Trim();
            } 
        }

        /// <summary>
        /// Gets or sets the level of wordiness of the applications output
        /// </summary>
        public static ConsoleVerbosity Verbosity { get { return m_Verbosity; } set { m_Verbosity = value; } }

        /// <summary>
        /// Gets or sets the level of wordiness of the applications output
        /// </summary>
        public static bool DefaultPromptAnswer { get { return m_DefaultPromptAnswer; } set { m_DefaultPromptAnswer = value; } }

        /// <summary>
        /// Gets or sets the value indicating the the DefaultPromptAnswer will be used in the case of a confirm prompt
        /// </summary>
        public static bool UseDefaultPromptAnswer { get { return m_UseDefaultPromptAnswer; } set { m_UseDefaultPromptAnswer = value; } }

        /// <summary>
        /// Gets or sets the value indicating that all warnings should be reported as MSVS build errors/warnings at all. 
        /// To be used in conjunction with ConsoleExt.IsBuildMode.
        /// </summary>
        public static bool WarningsAsErrors { get { return m_WarningsAsErrors; } set { m_WarningsAsErrors = value; } }

        /// <summary>
        /// Gets or sets the value indicating that all warnings should be reported as MSVS build errors/warnings at all. 
        /// To be used in conjunction with ConsoleExt.IsBuildMode.
        /// </summary>
        public static bool ReportWarnings { get { return m_ReportWarnings; } set { m_ReportWarnings = value; } }

        /// <summary>
        /// Gets or sets the value indicating whether the applications console output should be formatted to comply with MSVS project build.
        /// </summary>
        public static bool IsBuildMode 
        { 
            get 
            { 
                return m_IsBuildMode; 
            } 
            set 
            { 
                m_IsBuildMode = value; 
            }
        }

        #endregion 

        #region Capablitys

        /// <summary>
        /// True if the console buffer write position can be manualy moved or rolled back.
        /// </summary>
        public static bool CanManipulateBuffer { get { EnsureConsoleFeatures(); return m_CanManipulateBuffer && !m_IsBuildMode; } }

        #endregion         

        #endregion

        #region Properties forwarded from System.Console

        /// <summary>
        /// Gets or sets the width of the buffer area if its available. 
        /// </summary>
        public static int BufferWidth
        {
            get
            {
                EnsureConsoleFeatures();

                if (m_IsBuildMode || !m_CanManipulateBuffer)
                    return m_BufferWidth;
                else
                    return Console.BufferWidth;
            }
            set
            {
				if (m_IsBuildMode || 
					!CanManipulateBuffer || 
					Helper.IsRunningOnMono == true)
				{
					throw new Exception(Strings.ConsoleExt_BufferWidth_Error);
				}

				m_SetBufferWidth(value); 
            }
        }

        /// <summary>
        /// Gets or sets the height of the buffer area if its available.  
        /// </summary>
        public static int BufferHeight
        {
            get
            {
                EnsureConsoleFeatures();

                if (m_IsBuildMode || !m_CanManipulateBuffer)
                    return m_BufferHeight;
                else
                    return Console.BufferHeight;
            }
            set
            {
				if (m_IsBuildMode ||
					!CanManipulateBuffer ||
					Helper.IsRunningOnMono == true)
				{
					throw new Exception(Strings.ConsoleExt_BufferHeight_Error);
				}

				m_SetBufferHeight(value); 
            }
        }

        /// <summary>
        /// Gets or sets the column position of the cursor within the buffer area.
        /// </summary>
        public static int CursorLeft
        {
            get
            {
                EnsureConsoleFeatures();

                if (m_IsBuildMode || !m_CanManipulateBuffer)
                    throw new Exception(Strings.ConsoleExt_CursorManipulate_GetSetError);
                else
                    return Console.CursorLeft;
            }
            set
            {
                if (m_IsBuildMode || !CanManipulateBuffer)
                {
                    throw new Exception(Strings.ConsoleExt_CursorManipulate_GetSetError);
                }

                Console.CursorLeft = value;
            }
        }

        /// <summary>
        /// Gets or sets the row position of the cursor within the buffer area.
        /// </summary>
        public static int CursorTop
        {
            get
            {
                EnsureConsoleFeatures();

                if (m_IsBuildMode || !m_CanManipulateBuffer)
                    throw new Exception(Strings.ConsoleExt_CursorManipulate_GetSetError);
                else
                    return Console.CursorTop;
            }
            set
            {
                if (m_IsBuildMode || !CanManipulateBuffer)
                {
                    throw new Exception(Strings.ConsoleExt_CursorManipulate_GetSetError);
                }

                Console.CursorTop = value;
            }
        }

        /// <summary>
        /// Gets or sets the height of the cursor within a character cell.
        /// </summary>
        public static int CursorSize
        {
            get
            {
                EnsureConsoleFeatures();

                if (m_IsBuildMode || !m_CanManipulateBuffer)
                    throw new Exception(Strings.ConsoleExt_CursorManipulate_GetSetError);
                else
                    return Console.CursorSize;
            }
            set
            {
                if (m_IsBuildMode || !CanManipulateBuffer)
                {
                    throw new Exception(Strings.ConsoleExt_CursorManipulate_GetSetError);
                }

                Console.CursorSize = value;
            }
        }

		/// <summary>
		/// Gets or sets the title to display in the console title bar.
		/// </summary>
		public static string Title
		{
			get 
            {
				return Console.Title;
            }
			set
			{
				Console.Title = value; 
			}
		}

        #region Colours

        /// <summary>
        /// Colour Theme
        /// </summary>
        public static ConsoleColorTheme Theme
        {
            get 
            {
                if (m_Theme == null)
                {
                    m_Theme = ConsoleColorTheme.Load(Console.ForegroundColor, Console.BackgroundColor, ConsoleColorDefaultThemes.None); 
                }

                return m_Theme; 
            }
            set 
            {
                m_Theme = value; 
            }
        }        

        /// <summary>
        /// Gets or sets the foreground color for the current write position.
        /// </summary>
        public static ConsoleColorExt ForegroundColor 
        {   
            get { return (ConsoleColorExt)Console.ForegroundColor; }
            set 
            {
                if (value != ConsoleColorExt.Inhreit) 
                { 
                    Console.ForegroundColor = (ConsoleColor)value; 
                }
            } 
        }

        // private ConsoleColorExt m_BackgroundColor;
             
        /// <summary>
        /// Gets or sets the background color for the current write position.
        /// </summary>
        public static ConsoleColorExt BackgroundColor
        {
            get { return (ConsoleColorExt)(int)Console.BackgroundColor; }
            set
            {
                if (value != ConsoleColorExt.Inhreit)
                {
                    //if ((int)value < (int)ConsoleColorExt.Inhreit)
                        Console.BackgroundColor = (ConsoleColor)value;
                    //else
                    //    Console.BackgroundColor = (ConsoleColor)value;
                }
            }
        }

        /// <summary>
        /// Sets the foreground colour for the current write position to the value of the the theme color.
        /// </summary>
        public static ConsoleThemeColor ForegroundThemeColor { set { ForegroundColor = Theme[value]; } } 

        /// <summary>
        /// Sets the background colour for the current write position to the value of the the theme color.
        /// </summary>
        public static ConsoleThemeColor BackgroundThemeColor { set { BackgroundColor = Theme[value]; } }

        /// <summary>
        /// Reset the colors
        /// </summary>
        public static void ResetColor()
        {
            Console.ResetColor();
        }

        #endregion

        #endregion

        #region State Methods

        /// <summary>
        /// Gets or sets the current console colour state
        /// </summary>
        public static ConsoleColorState ColorState
        {
            get { return new ConsoleColorState(ForegroundColor, BackgroundColor); }
            set
            {
                ForegroundColor = value.ForegroundColor;
                BackgroundColor = value.BackgroundColor;
            }
        }

        #endregion

        #region Methods forwarded from System.Console

        /// <summary>
        /// Sets the position of the cursor.
        /// </summary>
        /// <param name="left">The column position of the cursor.</param>
        /// <param name="top">The row position of the cursor.</param>
        public static void SetCursorPosition(int left, int top)
        {
            EnsureConsoleFeatures();

            if (m_IsBuildMode || !m_CanManipulateBuffer)
                throw new Exception(Strings.ConsoleExt_CursorManipulate_GetSetError);

            Console.SetCursorPosition(left, top);
        }

		/// <summary>
		/// Clears the console buffer and corresponding console window of display information.
		/// </summary>
		public static void Clear()
		{
			EnsureConsoleFeatures();

			if (m_IsBuildMode || !m_CanManipulateBuffer)
				throw new Exception(Strings.ConsoleExt_CursorManipulate_GetSetError);

			Console.Clear(); 
		}

        #endregion 
		
		#region Read Key / Line Methods

		private static void ThrowInputDuringBuildModeException()
		{
			if (IsBuildMode == true || CanManipulateBuffer == false)
			{
				throw new Exception(Strings.ConsoleExt_CannotAcceptInput);
			}
		}

		public static bool KeyAvailable 
		{
			get
			{
				ThrowInputDuringBuildModeException();

				return Console.KeyAvailable; 
			}
		}

		/// <summary>
		/// Reads the next character from the standard input stream.
		/// </summary>
		/// <returns>The next character from the input stream, or negative one (-1) if there are currently no more characters to be read.</returns>
		public static int Read()
		{
			ThrowInputDuringBuildModeException(); 

			return Console.Read(); 
		}
		     
		/// <summary>
		/// Obtains the next character or function key pressed by the user. The pressed key is displayed in the console window.
		/// </summary>
		/// <returns>A System.ConsoleKeyInfo object that describes the System.ConsoleKey constant and Unicode character, if any, that correspond to the pressed console key. The System.ConsoleKeyInfo object also describes, in a bitwise combination of System.ConsoleModifiers values, whether one or more SHIFT, ALT, or CTRL modifier keys was pressed simultaneously with the console key.</returns>
		public static ConsoleKeyInfo ReadKey()
		{
			ThrowInputDuringBuildModeException();

			return Console.ReadKey();
		}
		
		/// <summary>
		/// Obtains the next character or function key pressed by the user. The pressed key is optionally displayed in the console window.
		/// </summary>
		/// <param name="intercept">Determines whether to display the pressed key in the console window. true to not display the pressed key; otherwise, false.</param>
		/// <returns>A System.ConsoleKeyInfo object that describes the System.ConsoleKey constant and Unicode character, if any, that correspond to the pressed console key. The System.ConsoleKeyInfo object also describes, in a bitwise combination of System.ConsoleModifiers values, whether one or more SHIFT, ALT, or CTRL modifier keys was pressed simultaneously with the console key.</returns>
		public static ConsoleKeyInfo ReadKey(bool intercept)
		{
			ThrowInputDuringBuildModeException();

			return Console.ReadKey(intercept); 
		}

		/// <summary>
		/// Reads the next line of characters from the standard input stream.
		/// </summary>
		/// <returns>The next line of characters from the input stream, or null if no more lines are available.</returns>
		public static string ReadLine()
		{
			ThrowInputDuringBuildModeException();

			return Console.ReadLine(); 
		}

		#endregion

		#region Special Prompt Methods

		/// <summary>
		/// Prompt for key input 
		/// </summary>
		/// <param name="message">message to display</param>
		/// <param name="intercept">Determines whether to display the pressed key in the console window. true to not display the pressed key; otherwise, false.</param>
		/// <param name="throwException">Should this method throw an exception if key input is not possible</param>
		/// <returns>A System.ConsoleKeyInfo object that describes the System.ConsoleKey constant and Unicode character, if any, that correspond to the pressed console key. The System.ConsoleKeyInfo object also describes, in a bitwise combination of System.ConsoleModifiers values, whether one or more SHIFT, ALT, or CTRL modifier keys was pressed simultaneously with the console key.</returns>
		public static ConsoleKeyInfo PromptForKey(string message, bool intercept, bool throwException)
		{
			WritePrompt(message);

			if (IsBuildMode == true || CanManipulateBuffer == false)
			{ 
				if (throwException)
				{
					throw new Exception(Strings.ConsoleExt_CannotAcceptInput);
				}
				else 
				{
					return new ConsoleKeyInfo(); 
				}
			}
			else 
			{
				return Console.ReadKey(intercept); 
			}
		}

		/// <summary>
		/// Prompt for line input 
		/// </summary>
		/// <param name="message">message to display</param>
		/// <param name="throwException">Should this method throw an exception if key input is not possible</param>
		/// <returns>The next line of characters from the input stream, or null if no more lines are available.</returns>
		public static string PromptForLine(string message, bool throwException)
		{
			WritePrompt(message);

			if (IsBuildMode == true || CanManipulateBuffer == false)
			{
				if (throwException)
				{
					throw new Exception(Strings.ConsoleExt_CannotAcceptInput);
				}
				else
				{
					return null;
				}
			}
			else
			{
				return Console.ReadLine();
			}
		}

		#endregion

		#region Write and WriteLine Methods

		/// <summary>
        /// Check if a massage would be written
        /// </summary>
        /// <param name="verbosity">level to check against</param>
        /// <returns>true if the ConsoleVerbosity supplyed would result in a message in the current context</returns>
        public static bool ShouldWrite(ConsoleVerbosity verbosity)
        {
            return (int)verbosity <= (int)m_Verbosity;
        }

        /// <summary>
        /// Writes the specified string value to the standard output stream at the ConsoleVerbosity.Normal level.
        /// </summary>
        /// <param name="str">The value to write.</param>
        public static void Write(string str)
        {
            if ((int)ConsoleVerbosity.Normal <= (int)m_Verbosity)
            {
                Console.Write(str);
            }
        }

        /// <summary>
        /// Writes the specified string value to the standard output stream at the verbosity level specified.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        /// <param name="str">The value to write.</param>
        public static void Write(ConsoleVerbosity level, string str)
        {
            if ((int)level <= (int)m_Verbosity)
            {
                Console.Write(str);
            }
        }

        /// <summary>
        /// Writes the specified string value to the standard output stream at the verbosity level specified and in the specified colour.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        /// <param name="colour">The colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        public static void Write(ConsoleVerbosity level, ConsoleThemeColor colour, string str)
        {
            Write(level, Theme[colour], str);
        }

        /// <summary>
        /// Writes the specified string value to the standard output stream at the verbosity level specified and in the specified colour.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        /// <param name="colour">The colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        public static void Write(ConsoleVerbosity level, ConsoleColorExt colour, string str)
        {
            if ((int)level <= (int)m_Verbosity)
            {
                if (colour != ConsoleColorExt.Inhreit)
                {
                    ConsoleColor col = Console.ForegroundColor;

                    if (colour != ConsoleColorExt.Inhreit) 
                        Console.ForegroundColor = (ConsoleColor)colour;

                    Console.Write(str);
                    Console.ForegroundColor = col;
                }
                else
                    Console.Write(str);
            }
        }

        /// <summary>
        /// Writes the specified string value to the standard output stream at the ConsoleVerbosity.Normal level and in the specified colour.
        /// </summary>
        /// <param name="colour">The colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        public static void Write(ConsoleThemeColor colour, string str)
        {
            Write(Theme[colour], str);
        }

        /// <summary>
        /// Writes the specified string value to the standard output stream at the ConsoleVerbosity.Normal level and in the specified colour.
        /// </summary>
        /// <param name="colour">The colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        public static void Write(ConsoleColorExt colour, string str)
        {
            if ((int)ConsoleVerbosity.Normal <= (int)m_Verbosity)
            {
                if (colour != ConsoleColorExt.Inhreit)
                {
                    ConsoleColor col = Console.ForegroundColor;

                    if (colour != ConsoleColorExt.Inhreit)
                        Console.ForegroundColor = (ConsoleColor)colour;

                    Console.Write(str);
                    Console.ForegroundColor = col;
                }
                else
                    Console.Write(str);
            }
        }

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the ConsoleVerbosity.Normal level.
        /// </summary>
        public static void WriteLine(ConsoleVerbosity level)
        {
            if ((int)level <= (int)m_Verbosity)
            {
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the ConsoleVerbosity.Normal level.
        /// </summary>
        public static void WriteLine()
        {
            if ((int)ConsoleVerbosity.Normal <= (int)m_Verbosity)
            {
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the ConsoleVerbosity.Normal level.
        /// </summary>
        /// <param name="str">The value to write.</param>
        public static void WriteLine(string str)
        {
            if ((int)ConsoleVerbosity.Normal <= (int)m_Verbosity)
            {
                Console.WriteLine(str);
            }
        }

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the verbosity level specified.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        /// <param name="str">The value to write.</param>
        public static void WriteLine(ConsoleVerbosity level, string str)
        {
            if ((int)level <= (int)m_Verbosity)
            {
                Console.WriteLine(str);
            }
        }

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the verbosity level specified and in the specified colour.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        /// <param name="colour">The colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        public static void WriteLine(ConsoleVerbosity level, ConsoleThemeColor colour, string str)
        {
            WriteLine(level, Theme[colour], str);
        }

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the verbosity level specified and in the specified colour.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        /// <param name="colour">The colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        public static void WriteLine(ConsoleVerbosity level, ConsoleColorExt colour, string str)
        {
            if ((int)level <= (int)m_Verbosity)
            {
                if (colour != ConsoleColorExt.Inhreit)
                {
                    ConsoleColor col = Console.ForegroundColor;

                    if (colour != ConsoleColorExt.Inhreit)
                        Console.ForegroundColor = (ConsoleColor)colour;

                    Console.WriteLine(str);
                    Console.ForegroundColor = col;
                }
                else
                    Console.WriteLine(str);
            }
        }

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the ConsoleVerbosity.Normal level and in the specified colour.
        /// </summary>
        /// <param name="colour">The colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        public static void WriteLine(ConsoleThemeColor colour, string str)
        {
            WriteLine(Theme[colour], str);
        }

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the ConsoleVerbosity.Normal level and in the specified colour.
        /// </summary>
        /// <param name="colour">The colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        public static void WriteLine(ConsoleColorExt colour, string str)
        {
            if ((int)ConsoleVerbosity.Normal <= (int)m_Verbosity)
            {
                if (colour != ConsoleColorExt.Inhreit)
                {
                    ConsoleColor col = Console.ForegroundColor;

                    if (colour != ConsoleColorExt.Inhreit)
                        Console.ForegroundColor = (ConsoleColor)colour;

                    Console.WriteLine(str);
                    Console.ForegroundColor = col;
                }
                else
                    Console.WriteLine(str);
            }
        }

        #endregion 

        #region Special Write Methods

        #region Overrides 

        /// <summary>
        /// Write text as wrapped text within a fixed width
        /// </summary>
        /// <param name="colour">color to write in</param>
        /// <param name="message">the text to write</param>
        /// <param name="paddingLeft">margin at the left</param>
        /// <param name="paddingRight">margin at the right</param>
        public static void WriteWrapped(ConsoleThemeColor colour, string message, int paddingLeft, int paddingRight)
        {
            WriteWrapped(Theme[colour], message, paddingLeft, paddingRight);
        }

        /// <summary>
        /// Write text as wrapped text within a fixed width
        /// </summary>
        /// <param name="colour">color to write in</param>
        /// <param name="message">the text to write</param>
        /// <param name="paddingLeft">margin at the left</param>
        /// <param name="paddingRight">margin at the right</param>
        public static void WriteWrapped(ConsoleColorExt colour, string message, int paddingLeft, int paddingRight)
        {
            ConsoleColor col = Console.ForegroundColor;

            if (colour != ConsoleColorExt.Inhreit)
                Console.ForegroundColor = (ConsoleColor)colour;
            
            int maxWidth = ConsoleExt.BufferWidth - (paddingLeft + paddingRight);

            List<string> lines = new List<string>(message.Split(new string[] { Environment.NewLine, "\n" }, StringSplitOptions.None));

            string prefix = new string(' ', paddingLeft); 

            foreach (string line in lines)
            {
                string str = line;

                if (str.Length == 0)
                    Console.WriteLine();
                else
                {
                    while (str.Length > 0)
                    {
                        if (str.Length > maxWidth)
                        {
                            Console.WriteLine(prefix + str.Substring(0, maxWidth));
                            str = str.Substring(maxWidth);
                        }
                        else
                        {
                            Console.WriteLine(prefix + str);
                            str = "";
                        }
                    }
                }
            }

            Console.ForegroundColor = col;
        }

        /// <summary>
        /// Write a prompt message.
        /// If this application is set to build mode then the prompt will be a error.
        /// The reading of any keys is the responsibility of the calling program.
        /// </summary>
        /// <param name="str">message</param>
        public static void WritePrompt(string str)
        {
            WriteMessage(ConsoleMessage.Prompt, ConsoleThemeColor.PromptColor1, str);
        }

        /// <summary>
        /// Write a prompt message.
        /// If this application is set to build mode then the prompt will be a error.
        /// The reading of any keys is the responsibility of the calling program.
        /// </summary>        
        /// <param name="colour">colour</param>
        /// <param name="str">message</param>
        public static void WritePrompt(ConsoleThemeColor colour, string str)
        {
            WriteMessage(ConsoleMessage.Prompt, Theme[colour], str);
        }

        /// <summary>
        /// Write a prompt message.
        /// If this application is set to build mode then the prompt will be a error.
        /// The reading of any keys is the responsibility of the calling program.
        /// </summary>        
        /// <param name="colour">colour</param>
        /// <param name="str">message</param>
        public static void WritePrompt(ConsoleColorExt colour, string str)
        {
            WriteMessage(ConsoleMessage.Prompt, colour, str);
        }

        /// <summary>
        /// Write a error to the console.
        /// If this application is set to build mode the error will be formatted for Visual Studio 
        /// </summary>
        /// <param name="id">id for the error</param>
        /// <param name="str">message</param>
        public static void WriteError(int id, string str)
        {
            WriteMessage(ConsoleMessage.Error, ConsoleThemeColor.ErrorColor1, id, str); 
        }


        /// <summary>
        /// Write a error to the console.
        /// If this application is set to build mode the error will be formatted for Visual Studio 
        /// </summary>
        /// <param name="id">id for the error</param>
        /// <param name="sourceFile">a path to the file where the error can be located</param>
        /// <param name="line">the line in the file where the error occured</param>
        /// <param name="character">the character offset from the start of the line</param>
        /// <param name="str">message</param>
        public static void WriteError(int id, string sourceFile, int line, int character, string str)
        {
            WriteMessage(ConsoleMessage.Error, ConsoleThemeColor.ErrorColor1, id, sourceFile, line, character, str);
        }


        /// <summary>
        /// Write a error to the console.
        /// If this application is set to build mode the error will be formatted for Visual Studio 
        /// </summary>
        /// <param name="colour">colour</param>
        /// <param name="id">id for the error</param>
        /// <param name="str">message</param>
        public static void WriteError(ConsoleThemeColor colour, int id, string str)
        {
            WriteMessage(ConsoleMessage.Error, Theme[colour], id, str);
        }

        /// <summary>
        /// Write a error to the console.
        /// If this application is set to build mode the error will be formatted for Visual Studio 
        /// </summary>
        /// <param name="colour">colour</param>
        /// <param name="id">id for the error</param>
        /// <param name="str">message</param>
        public static void WriteError(ConsoleColorExt colour, int id, string str)
        {
            WriteMessage(ConsoleMessage.Error, colour, id, str); 
        }


        /// <summary>
        /// Write a error to the console. 
        /// If this application is set to build mode the warning will be formatted for Visual Studio.            
        /// If the WarningsAsErrors flag has been set to true the warning will be treated as an error. 
        /// </summary>
        /// <param name="id">id for the warning</param>
        /// <param name="str">message</param>
        public static void WriteWarning(int id, string str)
        {
            WriteMessage(ConsoleMessage.Warning, ConsoleThemeColor.WarningColor2, id, str);
        }



        /// <summary>
        /// Write a error to the console. 
        /// If this application is set to build mode the warning will be formatted for Visual Studio.            
        /// If the WarningsAsErrors flag has been set to true the warning will be treated as an error. 
        /// </summary>
        /// <param name="colour">colour</param>
        /// <param name="id">id for the warning</param>
        /// <param name="str">message</param>
        public static void WriteWarning(ConsoleThemeColor colour, int id, string str)
        {
            WriteMessage(ConsoleMessage.Warning, Theme[colour], id, str);
        }

        /// <summary>
        /// Write a error to the console. 
        /// If this application is set to build mode the warning will be formatted for Visual Studio.            
        /// If the WarningsAsErrors flag has been set to true the warning will be treated as an error. 
        /// </summary>
        /// <param name="colour">colour</param>
        /// <param name="id">id for the warning</param>
        /// <param name="str">message</param>
        public static void WriteWarning(ConsoleColorExt colour, int id, string str)
        {
            WriteMessage(ConsoleMessage.Warning, colour, id, str);
        }

        /// <summary>
        /// Write a error to the console. 
        /// If this application is set to build mode the warning will be formatted for Visual Studio.            
        /// If the WarningsAsErrors flag has been set to true the warning will be treated as an error. 
        /// </summary>
        /// <param name="colour">colour</param>
        /// <param name="id">id for the warning</param>        
        /// <param name="sourceFile">a path to the file where the warning can be located</param>
        /// <param name="line">the line in the file where the warning occured</param>
        /// <param name="character">the character offset from the warning of the line</param>
        /// <param name="str">message</param>
        public static void WriteWarning(ConsoleThemeColor colour, int id, string sourceFile, int line, int character, string str)
        {
            WriteMessage(ConsoleMessage.Warning, Theme[colour], id, sourceFile, line, character, str);
        }

        /// <summary>
        /// Write a error to the console. 
        /// If this application is set to build mode the warning will be formatted for Visual Studio.            
        /// If the WarningsAsErrors flag has been set to true the warning will be treated as an error. 
        /// </summary>
        /// <param name="colour">colour</param>
        /// <param name="id">id for the warning</param>        
        /// <param name="sourceFile">a path to the file where the warning can be located</param>
        /// <param name="line">the line in the file where the warning occured</param>
        /// <param name="character">the character offset from the warning of the line</param>
        /// <param name="str">message</param>
        public static void WriteWarning(ConsoleColorExt colour, int id, string sourceFile, int line, int character, string str)
        {
            WriteMessage(ConsoleMessage.Warning, colour, id, sourceFile, line, character, str);
        }


        /// <summary>
        /// Write a exception to the console. 
        /// </summary>
        /// <param name="id">id of the error</param>
        /// <param name="ex">exception object</param>
        public static void WriteException(int id, Exception ex)
        {
            WriteMessage(ConsoleMessage.Error, ConsoleThemeColor.ErrorColor1, id, ex.Message);

            if (Verbosity == ConsoleVerbosity.Debug)
                WriteStackTrace(ex.StackTrace); 
        }

        /// <summary>
        /// Write a exception to the console. 
        /// </summary>
        /// <param name="id">id of the error</param>
        /// <param name="sourceFile">a path to the file where the warning can be located</param>
        /// <param name="line">the line in the file where the exception occured</param>
        /// <param name="character">the character offset from the exception causing of the line</param>
        /// <param name="ex">exception object</param>
        public static void WriteException(int id, string sourceFile, int line, int character, Exception ex)
        {
            WriteMessage(ConsoleMessage.Error, ConsoleThemeColor.ErrorColor1, id, sourceFile, line, character, ex.Message);

            if (Verbosity == ConsoleVerbosity.Debug)
                WriteStackTrace(ex.StackTrace);
        }

        /// <summary>
        /// Write a exception to the console. 
        /// </summary>
        /// <param name="id">id of the error</param>
        /// <param name="title">title text for message</param>
        /// <param name="ex">exception object</param>
        public static void WriteException(int id, string title, Exception ex)
        {
            WriteMessage(ConsoleMessage.Error, ConsoleThemeColor.ErrorColor1, id, title + Environment.NewLine + ex.Message);

            if (Verbosity == ConsoleVerbosity.Debug)
                WriteStackTrace(ex.StackTrace); 
        }

        /// <summary>
        /// Write a exception to the console. 
        /// </summary>
        /// <param name="id">id of the error</param>
        /// <param name="sourceFile">a path to the file where the warning can be located</param>
        /// <param name="line">the line in the file where the exception occured</param>
        /// <param name="character">the character offset from the exception causing of the line</param>
        /// <param name="title">title text for message</param>
        /// <param name="ex">exception object</param>
        public static void WriteException(int id, string sourceFile, int line, int character, string title, Exception ex)
        {
            WriteMessage(ConsoleMessage.Error, ConsoleThemeColor.ErrorColor1, id, sourceFile, line, character, title + Environment.NewLine + ex.Message);

            if (Verbosity == ConsoleVerbosity.Debug)
                WriteStackTrace(ex.StackTrace);
        }

        /// <summary>
        /// Write a stack trace string to the console
        /// </summary>
        /// <param name="trace">trace string</param>
        public static void WriteStackTrace(string trace)
        {
			if (String.IsNullOrEmpty(trace) == false)
			{
				ConsoleColor col = Console.ForegroundColor;

				Console.WriteLine();
				Console.ForegroundColor = (ConsoleColor)Theme[ConsoleThemeColor.ErrorColor2];
				Console.WriteLine(new string(ConsoleChars.GetShade(ConsoleShade.Dim), ConsoleExt.BufferWidth));

				Console.ForegroundColor = (ConsoleColor)Theme[ConsoleThemeColor.TitleText];
				Console.WriteLine("  " + trace.Replace("\n", "\n  "));
				Console.WriteLine();

				Console.ForegroundColor = (ConsoleColor)Theme[ConsoleThemeColor.ErrorColor2];
				Console.WriteLine(new string(ConsoleChars.GetShade(ConsoleShade.Dim), ConsoleExt.BufferWidth));
				Console.WriteLine();

				Console.ForegroundColor = col;
			}
        }

        /// <summary>
        /// Write a message to the console
        /// </summary>
        /// <param name="type">message type</param>
        /// <param name="colour">colour</param>
        /// <param name="str">message</param>
        public static void WriteMessage(ConsoleMessage type, ConsoleThemeColor colour, string str)
        {
            WriteMessage(type, Theme[colour], 0, str);
        }

        /// <summary>
        /// Write a message to the console
        /// </summary>
        /// <param name="type">message type</param>
        /// <param name="colour">colour</param>
        /// <param name="str">message</param>
        public static void WriteMessage(ConsoleMessage type, ConsoleColorExt colour, string str)
        {
            WriteMessage(type, colour, 0, str); 
        }

        #endregion

        #region Main Write Message Implementation 

        /// <summary>
        /// Write a message to the console
        /// </summary>
        /// <param name="type">message type</param>
        /// <param name="colour">colour</param>
        /// <param name="errorId">id for the error, warning or exception</param>
        /// <param name="str">message</param>
        public static void WriteMessage(ConsoleMessage type, ConsoleThemeColor colour, int errorId, string str)
        {
            WriteMessage(type, Theme[colour], errorId, null, 0, 0, str);
        }

        /// <summary>
        /// Write a message to the console
        /// </summary>
        /// <param name="type">message type</param>
        /// <param name="colour">colour</param>
        /// <param name="errorId">id for the error, warning or exception</param>
        /// <param name="str">message</param>
        public static void WriteMessage(ConsoleMessage type, ConsoleColorExt colour, int errorId, string str)
        {
            WriteMessage(type, colour, errorId, null, 0, 0, str);  
        }

                /// <summary>
        /// Write a message to the console
        /// </summary>
        /// <param name="type">message type</param>
        /// <param name="colour">colour</param>
        /// <param name="errorId">id for the error, warning or exception</param>
        /// <param name="sourceFile">a path to the file where the warning can be located</param>
        /// <param name="line">the line in the file where the exception occured</param>
        /// <param name="character">the character offset from the exception causing of the line</param>
        /// <param name="str">message</param>
        public static void WriteMessage(ConsoleMessage type, ConsoleThemeColor colour, int errorId, string sourceFile, int line, int character, string str)
        {
            WriteMessage(type, Theme[colour], errorId, sourceFile, line, character, str);
        }

        /// <summary>
        /// Write a message to the console
        /// </summary>
        /// <param name="type">message type</param>
        /// <param name="colour">colour</param>
        /// <param name="errorId">id for the error, warning or exception</param>
        /// <param name="sourceFile">a path to the file where the warning can be located</param>
        /// <param name="line">the line in the file where the exception occured</param>
        /// <param name="character">the character offset from the exception causing of the line</param>
        /// <param name="str">message</param>
        public static void WriteMessage(ConsoleMessage type, ConsoleColorExt colour, int errorId, string sourceFile, int line, int character, string str)
        {
            string realString = str;

            if (ConsoleExt.IsBuildMode)
            {
                string reportType = "";

                if (type == ConsoleMessage.Warning)
                {
                    if (WarningsAsErrors)
                        reportType = "error";
                    else
                        reportType = "warning"; 
                }
                else if (type == ConsoleMessage.Prompt)
                {
                    if (WarningsAsErrors)
                        reportType = "error";
                    else
                        reportType = "warning"; 
                }
                else
                {
                    reportType = "error"; // type.ToString().ToLower();
                }
                
                string sourceString = null;

                if (sourceFile == null)
                    sourceString = Application.ExecutablePath;
                else
                    sourceString = sourceFile + "(" + line + "," + character + ")"; 
 
                if (reportType != "warning" || (reportType == "warning" && ReportWarnings))
                    realString = sourceString + ": " + reportType + " " + ApplicationBuildReportPrefix + errorId.ToString().PadLeft(4, '0') + ": " + str.Replace("\n", " ");
            }

            ConsoleColor col = Console.ForegroundColor;
            
            if (colour != ConsoleColorExt.Inhreit)
                Console.ForegroundColor = (ConsoleColor)colour;

            if (CanManipulateBuffer)
            {
                if (Console.CursorLeft > 0)
                    Console.WriteLine();

                Console.WriteLine(realString);
            }
            else
            {
                Console.WriteLine(Environment.NewLine + realString);
            }

            Console.ForegroundColor = col;
        }

        #endregion

        #endregion

        #region Interpreted Console Write 
        
        /// <summary>
        /// Write a interpreted buffer int the console
        /// </summary>
        /// <param name="buffer">formated buffer</param>
        public static void WriteInterpreted(string buffer)
        {
            ConsoleFormatter.WriteInterpreted(m_SystemConsole, buffer);
        }

        /// <summary>
        /// Write a interpreted buffer int the console
        /// </summary>
        /// <param name="buffer">formated buffer</param>
        /// <param name="paddingLeft">margin on the left</param>
        /// <param name="paddingRight">margin on the right</param>
        public static void WriteInterpreted(string buffer, int paddingLeft, int paddingRight)
        {
            ConsoleFormatter.WriteInterpreted(m_SystemConsole, buffer, paddingLeft, paddingRight);
        }

        /// <summary>
        /// Write a interpreted buffer int the console
        /// </summary>
        /// <param name="colour">initial colour</param>
        /// <param name="buffer">formated buffer</param>
        /// <param name="paddingLeft">margin on the left</param>
        /// <param name="paddingRight">margin on the right</param>
        public static void WriteInterpreted(ConsoleThemeColor colour, string buffer, int paddingLeft, int paddingRight)
        {
            ConsoleFormatter.WriteInterpreted(m_SystemConsole, Theme[colour], buffer, paddingLeft, paddingRight);
        }

        /// <summary>
        /// Write a interpreted buffer int the console
        /// </summary>
        /// <param name="colour">initial colour</param>
        /// <param name="buffer">formated buffer</param>
        /// <param name="paddingLeft">margin on the left</param>
        /// <param name="paddingRight">margin on the right</param>
        public static void WriteInterpreted(ConsoleColorExt colour, string buffer, int paddingLeft, int paddingRight)
        {
            ConsoleFormatter.WriteInterpreted(m_SystemConsole, colour, buffer, paddingLeft, paddingRight);
        }

        /// <summary>
        /// Write a interpreted line buffer
        /// </summary>
        /// <param name="buffer">formated buffer</param>
        public static void WriteInterpretedLine(string buffer)
        {
            ConsoleFormatter.WriteInterpretedLine(m_SystemConsole, buffer);
        }

        #endregion        

        #region Static System Console Instance

        private static IConsole m_SystemConsole = new SystemConsoleImpementation(); 
        public static IConsole SystemConsole { get { return m_SystemConsole; } }

        #endregion

        #region IConsole Impementation

        internal class SystemConsoleImpementation : IConsole
        {
            #region IConsole Members

            public string ApplicationBuildReportPrefix
            {
                get
                {
                    return ConsoleExt.ApplicationBuildReportPrefix; 
                }
                set
                {
                    ConsoleExt.ApplicationBuildReportPrefix = value; 
                }
            }

            public ConsoleVerbosity Verbosity
            {
                get
                {
                    return ConsoleExt.Verbosity;
                }
                set
                {
                    ConsoleExt.Verbosity = value;
                }
            }

            public bool DefaultPromptAnswer
            {
                get
                {
                    return ConsoleExt.DefaultPromptAnswer;
                }
                set
                {
                    ConsoleExt.DefaultPromptAnswer = value;
                }
            }

            public bool UseDefaultPromptAnswer
            {
                get
                {
                    return ConsoleExt.UseDefaultPromptAnswer;
                }
                set
                {
                    ConsoleExt.UseDefaultPromptAnswer = value;
                }
            }

            public bool WarningsAsErrors
            {
                get
                {
                    return ConsoleExt.WarningsAsErrors;
                }
                set
                {
                    ConsoleExt.WarningsAsErrors = value;
                }
            }

            public bool ReportWarnings
            {
                get
                {
                    return ConsoleExt.ReportWarnings;
                }
                set
                {
                    ConsoleExt.ReportWarnings = value;
                }
            }

            public bool IsBuildMode
            {
                get
                {
                    return ConsoleExt.IsBuildMode;
                }
                set
                {
                    ConsoleExt.IsBuildMode = value;
                }
            }

            public bool CanManipulateBuffer
            {
                get
                {
                    return ConsoleExt.CanManipulateBuffer;
                }
            }

            public int BufferWidth
            {
                get
                {
                    return ConsoleExt.BufferWidth;
                }
                set
                {
                    ConsoleExt.BufferWidth = value;
                }
            }

            public int BufferHeight
            {
                get
                {
                    return ConsoleExt.BufferHeight;
                }
                set
                {
                    ConsoleExt.BufferHeight = value;
                }
            }

            public int CursorLeft
            {
                get
                {
                    return ConsoleExt.CursorLeft;
                }
                set
                {
                    ConsoleExt.CursorLeft = value;
                }
            }

            public int CursorTop
            {
                get
                {
                    return ConsoleExt.CursorTop;
                }
                set
                {
                    ConsoleExt.CursorTop = value;
                }
            }

            public int CursorSize
            {
                get
                {
                    return ConsoleExt.CursorSize;
                }
                set
                {
                    ConsoleExt.CursorSize = value;
                }
            }

			public string Title
			{
				get
				{
					return ConsoleExt.Title;
				}
				set
				{
					ConsoleExt.Title = value;
				}
			}

            #region Colours

            public ConsoleColorTheme Theme
            {
                get { return ConsoleExt.Theme; }
                set { ConsoleExt.Theme = value; }
            }

            public ConsoleColorExt ForegroundColor
            {
                get { return ConsoleExt.ForegroundColor; }
                set { ConsoleExt.ForegroundColor = value; }
            }

            public ConsoleColorExt BackgroundColor
            {
                get { return ConsoleExt.BackgroundColor; }
                set { ConsoleExt.BackgroundColor = value; }
            }

            public ConsoleThemeColor ForegroundThemeColor
            {
                set { ConsoleExt.ForegroundThemeColor = value; }
            }

            public ConsoleThemeColor BackgroundThemeColor
            {
                set { ConsoleExt.BackgroundThemeColor = value; }
            }

            #endregion

            #region State Methods

            public ConsoleColorState ColorState
            {
                get { return ConsoleExt.ColorState; }
                set { ConsoleExt.ColorState = value; } 
            }

            #endregion

            public void ResetColor()
            {
                ConsoleExt.ResetColor(); 
            }

            public void SetCursorPosition(int left, int top)
            {
                ConsoleExt.SetCursorPosition(left, top);
            }

			public void Clear()
			{
				ConsoleExt.Clear(); 
			}

			#region Read Key / Line Methods

			public bool KeyAvailable { get { return ConsoleExt.KeyAvailable; } }

			public int Read()
			{
				return ConsoleExt.Read();
			}

			public ConsoleKeyInfo ReadKey()
			{
				return ConsoleExt.ReadKey();
			}

			public ConsoleKeyInfo ReadKey(bool intercept)
			{
				return ConsoleExt.ReadKey(intercept);
			}

			public string ReadLine()
			{
				return ConsoleExt.ReadLine();
			}

			#endregion

			#region Special Prompt Methods

			public ConsoleKeyInfo PromptForKey(string message, bool intercept, bool throwException)
			{
				return ConsoleExt.PromptForKey(message, intercept, throwException);
			}

			public string PromptForLine(string message, bool throwException)
			{
				return ConsoleExt.PromptForLine(message, throwException);
			}

			#endregion

            public bool ShouldWrite(ConsoleVerbosity verbosity)
            {
                return ConsoleExt.ShouldWrite(verbosity);
            }

            public void Write(string str)
            {
                ConsoleExt.Write(str); 
            }

            public void Write(ConsoleVerbosity level, string str)
            {
                ConsoleExt.Write(level, str);
            }

            public void Write(ConsoleVerbosity level, ConsoleColorExt colour, string str)
            {
                ConsoleExt.Write(level, colour, str);
            }

            public void Write(ConsoleVerbosity level, ConsoleThemeColor colour, string str)
            {
                ConsoleExt.Write(level, colour, str);
            }

            public void Write(ConsoleColorExt colour, string str)
            {
                ConsoleExt.Write(colour, str);
            }

            public void Write(ConsoleThemeColor colour, string str)
            {
                ConsoleExt.Write(colour, str);
            }

            public void WriteLine(ConsoleVerbosity level)
            {
                ConsoleExt.WriteLine(level);
            }

            public void WriteLine()
            {
                ConsoleExt.WriteLine();
            }

            public void WriteLine(string str)
            {
                ConsoleExt.WriteLine(str);
            }

            public void WriteLine(ConsoleVerbosity level, string str)
            {
                ConsoleExt.WriteLine(level, str);
            }

            public void WriteLine(ConsoleVerbosity level, ConsoleColorExt colour, string str)
            {
                ConsoleExt.WriteLine(level, colour, str);
            }

            public void WriteLine(ConsoleVerbosity level, ConsoleThemeColor colour, string str)
            {
                ConsoleExt.WriteLine(level, colour, str);
            }

            public void WriteLine(ConsoleColorExt colour, string str)
            {
                ConsoleExt.WriteLine(colour, str);
            }

            public void WriteLine(ConsoleThemeColor colour, string str)
            {
                ConsoleExt.WriteLine(colour, str);
            }

            public void WriteWrapped(ConsoleColorExt colour, string message, int paddingLeft, int paddingRight)
            {
                ConsoleExt.WriteWrapped(colour, message, paddingLeft, paddingRight);
            }

            public void WriteWrapped(ConsoleThemeColor colour, string message, int paddingLeft, int paddingRight)
            {
                ConsoleExt.WriteWrapped(colour, message, paddingLeft, paddingRight);
            }

            public void WritePrompt(string str)
            {
                ConsoleExt.WritePrompt(str);
            }

            public void WritePrompt(ConsoleColorExt colour, string str)
            {
                ConsoleExt.WritePrompt(colour, str);
            }

            public void WritePrompt(ConsoleThemeColor colour, string str)
            {
                ConsoleExt.WritePrompt(colour, str);
            }

            public void WriteError(int id, string str)
            {
                ConsoleExt.WriteError(id, str);
            }

            public void WriteError(int id, string sourceFile, int line, int character, string str)
            {
                ConsoleExt.WriteError(id, sourceFile, line, character, str);
            }

            public void WriteError(ConsoleColorExt colour, int id, string str)
            {
                ConsoleExt.WriteError(colour, id, str);
            }

            public void WriteError(ConsoleThemeColor colour, int id, string str)
            {
                ConsoleExt.WriteError(colour, id, str);
            }

            public void WriteWarning(int id, string str)
            {
                ConsoleExt.WriteWarning(id, str);
            }

            public void WriteWarning(ConsoleColorExt colour, int id, string str)
            {
                ConsoleExt.WriteWarning(colour, id, str);
            }

            public void WriteWarning(ConsoleThemeColor colour, int id, string str)
            {
                ConsoleExt.WriteWarning(colour, id, str);
            }

            public void WriteWarning(ConsoleColorExt colour, int id, string sourceFile, int line, int character, string str)
            {
                ConsoleExt.WriteWarning(colour, id, sourceFile, line, character, str);
            }

            public void WriteWarning(ConsoleThemeColor colour, int id, string sourceFile, int line, int character, string str)
            {
                ConsoleExt.WriteWarning(colour, id, sourceFile, line, character, str);
            }

            public void WriteException(int id, Exception ex)
            {
                ConsoleExt.WriteException(id, ex);
            }

            public void WriteException(int id, string sourceFile, int line, int character, Exception ex)
            {
                ConsoleExt.WriteException(id, sourceFile, line, character, ex);
            }

            public void WriteException(int id, string title, Exception ex)
            {
                ConsoleExt.WriteException(id, title, ex);
            }

            public void WriteException(int id, string sourceFile, int line, int character, string title, Exception ex)
            {
                ConsoleExt.WriteException(id, sourceFile, line, character, title, ex);
            }

            public void WriteStackTrace(string trace)
            {
                ConsoleExt.WriteStackTrace(trace);
            }

            public void WriteMessage(ConsoleMessage type, ConsoleColorExt colour, string str)
            {
                ConsoleExt.WriteMessage(type, colour, str);
            }

            public void WriteMessage(ConsoleMessage type, ConsoleThemeColor colour, string str)
            {
                ConsoleExt.WriteMessage(type, colour, str);
            }

            public void WriteMessage(ConsoleMessage type, ConsoleColorExt colour, int errorId, string str)
            {
                ConsoleExt.WriteMessage(type, colour, errorId, str);
            }

            public void WriteMessage(ConsoleMessage type, ConsoleThemeColor colour, int errorId, string str)
            {
                ConsoleExt.WriteMessage(type, colour, errorId, str);
            }

            public void WriteMessage(ConsoleMessage type, ConsoleColorExt colour, int errorId, string sourceFile, int line, int character, string str)
            {
                ConsoleExt.WriteMessage(type, colour, errorId, sourceFile, line, character, str);
            }

            public void WriteMessage(ConsoleMessage type, ConsoleThemeColor colour, int errorId, string sourceFile, int line, int character, string str)
            {
                ConsoleExt.WriteMessage(type, colour, errorId, sourceFile, line, character, str);
            }

            public void WriteInterpreted(string buffer)
            {
                ConsoleExt.WriteInterpreted(buffer);
            }

            public void WriteInterpreted(string buffer, int paddingLeft, int paddingRight)
            {
                ConsoleExt.WriteInterpreted(buffer, paddingLeft, paddingRight);
            }

            public void WriteInterpreted(ConsoleColorExt colour, string buffer, int paddingLeft, int paddingRight)
            {
                ConsoleExt.WriteInterpreted(colour, buffer, paddingLeft, paddingRight);
            }

            public void WriteInterpreted(ConsoleThemeColor colour, string buffer, int paddingLeft, int paddingRight)
            {
                ConsoleExt.WriteInterpreted(colour, buffer, paddingLeft, paddingRight);
            }

            public void WriteInterpretedLine(string buffer)
            {
                ConsoleExt.WriteInterpretedLine(buffer);
            }

            #endregion
        }

        #endregion		
	}
}
