﻿/* 
 * Rug.Cmd
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using Rug.Cmd.Colors;

namespace Rug.Cmd
{
    public interface IConsole
    {        
        #region Public Properties

        #region Options

        /// <summary>
        /// Gets or sets the prefix for errors and warnings. 
        /// To be used in conjunction with ConsoleExt.IsBuildMode.
        /// </summary>
        string ApplicationBuildReportPrefix { get; set; }

        /// <summary>
        /// Gets or sets the level of wordiness of the applications output
        /// </summary>
        ConsoleVerbosity Verbosity { get; set; }

        /// <summary>
        /// Gets or sets the default prompt answer i.e the yes / no for any confirm prompt 
        /// </summary>
        bool DefaultPromptAnswer { get; set; }

        /// <summary>
        /// Gets or sets the value indicating the the DefaultPromptAnswer will be used in the case of a confirm prompt
        /// </summary>
        bool UseDefaultPromptAnswer { get; set; }

        /// <summary>
        /// Gets or sets the value indicating that all warnings should be handled as errors. 
        /// To be used in conjunction with ConsoleExt.IsBuildMode.
        /// </summary>
        bool WarningsAsErrors { get; set; }

        /// <summary>
        /// Gets or sets the value indicating that all warnings should be reported as MSVS build errors/warnings at all. 
        /// To be used in conjunction with ConsoleExt.IsBuildMode.
        /// </summary>
        bool ReportWarnings { get; set; }

        /// <summary>
        /// Gets or sets the value indicating whether the applications console output should be formatted to comply with MSVS project build.
        /// </summary>
        bool IsBuildMode { get; set; }

        #endregion

        #region Capablitys

        /// <summary>
        /// True if the console buffer write position can be manualy moved or rolled back.
        /// </summary>
        bool CanManipulateBuffer { get; }

        #endregion

        #endregion

        #region Properties forwarded from System.Console

        /// <summary>
        /// Gets or sets the width of the buffer area if its available. 
        /// </summary>
        int BufferWidth { get; set; }

        /// <summary>
        /// Gets or sets the height of the buffer area if its available.  
        /// </summary>
        int BufferHeight { get; set; }

        /// <summary>
        /// Gets or sets the column position of the cursor within the buffer area.
        /// </summary>
        int CursorLeft { get; set; }

        /// <summary>
        /// Gets or sets the row position of the cursor within the buffer area.
        /// </summary>
        int CursorTop { get; set; }

        /// <summary>
        /// Gets or sets the height of the cursor within a character cell.
        /// </summary>
        int CursorSize { get; set; }

		/// <summary>
		/// Gets or sets the title to display in the console title bar.
		/// </summary>
		string Title { get; set; }

        #region Colours

        /// <summary>
        /// Gets or sets the current ConsoleColorTheme
        /// </summary>
        ConsoleColorTheme Theme { get; set; } 

        /// <summary>
        /// Gets or sets the foreground color for the current write position.
        /// </summary>
        ConsoleColorExt ForegroundColor { get; set; }

        /// <summary>
        /// Gets or sets the background color for the current write position.
        /// </summary>
        ConsoleColorExt BackgroundColor { get; set; }

        /// <summary>
        /// Sets the foreground colour for the current write position to the value of the the theme color.
        /// </summary>
        ConsoleThemeColor ForegroundThemeColor { set; }

        /// <summary>
        /// Sets the background colour for the current write position to the value of the the theme color.
        /// </summary>
        ConsoleThemeColor BackgroundThemeColor { set; } 

        #endregion

        #endregion

        #region State Methods

        /// <summary>
        /// Gets or sets the current console colour state
        /// </summary>
        ConsoleColorState ColorState { get; set; } 

        #endregion

        #region Methods forwarded from System.Console

        /// <summary>
        /// Reset the colors
        /// </summary>
        void ResetColor(); 

        /// <summary>
        /// Sets the position of the cursor.
        /// </summary>
        /// <param name="left">The column position of the cursor.</param>
        /// <param name="top">The row position of the cursor.</param>
        void SetCursorPosition(int left, int top);

		/// <summary>
		/// Clears the console buffer and corresponding console window of display information.
		/// </summary>
		void Clear(); 

        #endregion

		#region Read Key / Line Methods

		/// <summary>
		/// Gets a value indicating whether a key press is available in the input stream.
		/// returns true if a key press is available; otherwise, false.
		/// </summary>
		bool KeyAvailable { get;  }

		/// <summary>
		/// Reads the next character from the standard input stream.
		/// </summary>
		/// <returns>The next character from the input stream, or negative one (-1) if there are currently no more characters to be read.</returns>
		int Read();
	
		/// <summary>
		/// Obtains the next character or function key pressed by the user. The pressed key is displayed in the console window.
		/// </summary>
		/// <returns>A System.ConsoleKeyInfo object that describes the System.ConsoleKey constant and Unicode character, if any, that correspond to the pressed console key. The System.ConsoleKeyInfo object also describes, in a bitwise combination of System.ConsoleModifiers values, whether one or more SHIFT, ALT, or CTRL modifier keys was pressed simultaneously with the console key.</returns>
		ConsoleKeyInfo ReadKey();

		/// <summary>
		/// Obtains the next character or function key pressed by the user. The pressed key is optionally displayed in the console window.
		/// </summary>
		/// <param name="intercept">Determines whether to display the pressed key in the console window. true to not display the pressed key; otherwise, false.</param>
		/// <returns>A System.ConsoleKeyInfo object that describes the System.ConsoleKey constant and Unicode character, if any, that correspond to the pressed console key. The System.ConsoleKeyInfo object also describes, in a bitwise combination of System.ConsoleModifiers values, whether one or more SHIFT, ALT, or CTRL modifier keys was pressed simultaneously with the console key.</returns>
		ConsoleKeyInfo ReadKey(bool intercept);
		
		/// <summary>
		/// Reads the next line of characters from the standard input stream.
		/// </summary>
		/// <returns>The next line of characters from the input stream, or null if no more lines are available.</returns>
		string ReadLine(); 

		#endregion

		#region Special Prompt Methods

		/// <summary>
		/// Prompt for key input 
		/// </summary>
		/// <param name="message">message to display</param>
		/// <param name="intercept">Determines whether to display the pressed key in the console window. true to not display the pressed key; otherwise, false.</param>
		/// <param name="throwException">Should this method throw an exception if key input is not possible</param>
		/// <returns>A System.ConsoleKeyInfo object that describes the System.ConsoleKey constant and Unicode character, if any, that correspond to the pressed console key. The System.ConsoleKeyInfo object also describes, in a bitwise combination of System.ConsoleModifiers values, whether one or more SHIFT, ALT, or CTRL modifier keys was pressed simultaneously with the console key.</returns>
		ConsoleKeyInfo PromptForKey(string message, bool intercept, bool throwException); 		

		/// <summary>
		/// Prompt for line input 
		/// </summary>
		/// <param name="message">message to display</param>
		/// <param name="throwException">Should this method throw an exception if key input is not possible</param>
		/// <returns>The next line of characters from the input stream, or null if no more lines are available.</returns>
		string PromptForLine(string message, bool throwException);

		#endregion

        #region Write and WriteLine Methods

        /// <summary>
        /// Check if a massage would be written
        /// </summary>
        /// <param name="verbosity">level to check against</param>
        /// <returns>true if the ConsoleVerbosity supplyed would result in a message in the current context</returns>
        bool ShouldWrite(ConsoleVerbosity verbosity);

        /// <summary>
        /// Writes the specified string value to the standard output stream at the ConsoleVerbosity.Normal level.
        /// </summary>
        /// <param name="str">The value to write.</param>
        void Write(string str);

        /// <summary>
        /// Writes the specified string value to the standard output stream at the verbosity level specified.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        /// <param name="str">The value to write.</param>
        void Write(ConsoleVerbosity level, string str);

        /// <summary>
        /// Writes the specified string value to the standard output stream at the verbosity level specified and in the specified colour.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        /// <param name="colour">The colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        void Write(ConsoleVerbosity level, ConsoleColorExt colour, string str);

        /// <summary>
        /// Writes the specified string value to the standard output stream at the verbosity level specified and in the specified colour.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        /// <param name="colour">The theme colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        void Write(ConsoleVerbosity level, ConsoleThemeColor colour, string str);

        /// <summary>
        /// Writes the specified string value to the standard output stream at the ConsoleVerbosity.Normal level and in the specified colour.
        /// </summary>
        /// <param name="colour">The colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        void Write(ConsoleColorExt colour, string str);

        /// <summary>
        /// Writes the specified string value to the standard output stream at the ConsoleVerbosity.Normal level and in the specified colour.
        /// </summary>
        /// <param name="colour">The theme colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        void Write(ConsoleThemeColor colour, string str);

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the ConsoleVerbosity.Normal level.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        void WriteLine(ConsoleVerbosity level);

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the ConsoleVerbosity.Normal level.
        /// </summary>
        void WriteLine();

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the ConsoleVerbosity.Normal level.
        /// </summary>
        /// <param name="str">The value to write.</param>
        void WriteLine(string str);

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the verbosity level specified.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        /// <param name="str">The value to write.</param>
        void WriteLine(ConsoleVerbosity level, string str);

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the verbosity level specified and in the specified colour.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        /// <param name="colour">The colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        void WriteLine(ConsoleVerbosity level, ConsoleColorExt colour, string str);

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the ConsoleVerbosity.Normal level and in the specified colour.
        /// </summary>
        /// <param name="colour">The colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        void WriteLine(ConsoleColorExt colour, string str);

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the verbosity level specified and in the specified colour.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        /// <param name="colour">The theme colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        void WriteLine(ConsoleVerbosity level, ConsoleThemeColor colour, string str);

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the ConsoleVerbosity.Normal level and in the specified colour.
        /// </summary>
        /// <param name="colour">The theme colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        void WriteLine(ConsoleThemeColor colour, string str);

        #endregion

        #region Special Write Methods

        #region Overrides

        /// <summary>
        /// Write text as wrapped text within a fixed width
        /// </summary>
        /// <param name="colour">color to write in</param>
        /// <param name="message">the text to write</param>
        /// <param name="paddingLeft">margin at the left</param>
        /// <param name="paddingRight">margin at the right</param>
        void WriteWrapped(ConsoleColorExt colour, string message, int paddingLeft, int paddingRight);

        /// <summary>
        /// Write text as wrapped text within a fixed width
        /// </summary>
        /// <param name="colour">theme color to write in</param>
        /// <param name="message">the text to write</param>
        /// <param name="paddingLeft">margin at the left</param>
        /// <param name="paddingRight">margin at the right</param>
        void WriteWrapped(ConsoleThemeColor colour, string message, int paddingLeft, int paddingRight);

        /// <summary>
        /// Write a prompt message.
        /// If this application is set to build mode then the prompt will be a error.
        /// The reading of any keys is the responsibility of the calling program.
        /// </summary>
        /// <param name="str">message</param>
        void WritePrompt(string str);

        /// <summary>
        /// Write a prompt message.
        /// If this application is set to build mode then the prompt will be a error.
        /// The reading of any keys is the responsibility of the calling program.
        /// </summary>        
        /// <param name="colour">colour</param>
        /// <param name="str">message</param>
        void WritePrompt(ConsoleColorExt colour, string str);

        /// <summary>
        /// Write a prompt message.
        /// If this application is set to build mode then the prompt will be a error.
        /// The reading of any keys is the responsibility of the calling program.
        /// </summary>        
        /// <param name="colour">colour</param>
        /// <param name="str">message</param>
        void WritePrompt(ConsoleThemeColor colour, string str);

        /// <summary>
        /// Write a error to the console.
        /// If this application is set to build mode the error will be formatted for Visual Studio 
        /// </summary>
        /// <param name="id">id for the error</param>
        /// <param name="str">message</param>
        void WriteError(int id, string str);

        /// <summary>
        /// Write a error to the console.
        /// If this application is set to build mode the error will be formatted for Visual Studio 
        /// </summary>
        /// <param name="id">id for the error</param>
        /// <param name="sourceFile">a path to the file where the error can be located</param>
        /// <param name="line">the line in the file where the error occured</param>
        /// <param name="character">the character offset from the start of the line</param>
        /// <param name="str">message</param>
        void WriteError(int id, string sourceFile, int line, int character, string str);

        /// <summary>
        /// Write a error to the console.
        /// If this application is set to build mode the error will be formatted for Visual Studio 
        /// </summary>
        /// <param name="colour">colour</param>
        /// <param name="id">id for the error</param>
        /// <param name="str">message</param>
        void WriteError(ConsoleColorExt colour, int id, string str);

        /// <summary>
        /// Write a error to the console.
        /// If this application is set to build mode the error will be formatted for Visual Studio 
        /// </summary>
        /// <param name="colour">colour</param>
        /// <param name="id">id for the error</param>
        /// <param name="str">message</param>
        void WriteError(ConsoleThemeColor colour, int id, string str);

        /// <summary>
        /// Write a error to the console. 
        /// If this application is set to build mode the warning will be formatted for Visual Studio.            
        /// If the WarningsAsErrors flag has been set to true the warning will be treated as an error. 
        /// </summary>
        /// <param name="id">id for the warning</param>
        /// <param name="str">message</param>
        void WriteWarning(int id, string str);

        /// <summary>
        /// Write a error to the console. 
        /// If this application is set to build mode the warning will be formatted for Visual Studio.            
        /// If the WarningsAsErrors flag has been set to true the warning will be treated as an error. 
        /// </summary>
        /// <param name="colour">colour</param>
        /// <param name="id">id for the warning</param>
        /// <param name="str">message</param>
        void WriteWarning(ConsoleColorExt colour, int id, string str);


        /// <summary>
        /// Write a error to the console. 
        /// If this application is set to build mode the warning will be formatted for Visual Studio.            
        /// If the WarningsAsErrors flag has been set to true the warning will be treated as an error. 
        /// </summary>
        /// <param name="colour">colour</param>
        /// <param name="id">id for the warning</param>
        /// <param name="str">message</param>
        void WriteWarning(ConsoleThemeColor colour, int id, string str);

        /// <summary>
        /// Write a error to the console. 
        /// If this application is set to build mode the warning will be formatted for Visual Studio.            
        /// If the WarningsAsErrors flag has been set to true the warning will be treated as an error. 
        /// </summary>
        /// <param name="colour">colour</param>
        /// <param name="id">id for the warning</param>        
        /// <param name="sourceFile">a path to the file where the warning can be located</param>
        /// <param name="line">the line in the file where the warning occured</param>
        /// <param name="character">the character offset from the warning of the line</param>
        /// <param name="str">message</param>
        void WriteWarning(ConsoleColorExt colour, int id, string sourceFile, int line, int character, string str);

        /// <summary>
        /// Write a error to the console. 
        /// If this application is set to build mode the warning will be formatted for Visual Studio.            
        /// If the WarningsAsErrors flag has been set to true the warning will be treated as an error. 
        /// </summary>
        /// <param name="colour">colour</param>
        /// <param name="id">id for the warning</param>        
        /// <param name="sourceFile">a path to the file where the warning can be located</param>
        /// <param name="line">the line in the file where the warning occured</param>
        /// <param name="character">the character offset from the warning of the line</param>
        /// <param name="str">message</param>
        void WriteWarning(ConsoleThemeColor colour, int id, string sourceFile, int line, int character, string str);

        /// <summary>
        /// Write a exception to the console. 
        /// </summary>
        /// <param name="id">id of the error</param>
        /// <param name="ex">exception object</param>
        void WriteException(int id, Exception ex);

        /// <summary>
        /// Write a exception to the console. 
        /// </summary>
        /// <param name="id">id of the error</param>
        /// <param name="sourceFile">a path to the file where the warning can be located</param>
        /// <param name="line">the line in the file where the exception occured</param>
        /// <param name="character">the character offset from the exception causing of the line</param>
        /// <param name="ex">exception object</param>
        void WriteException(int id, string sourceFile, int line, int character, Exception ex);

        /// <summary>
        /// Write a exception to the console. 
        /// </summary>
        /// <param name="id">id of the error</param>
        /// <param name="title">title text for message</param>
        /// <param name="ex">exception object</param>
        void WriteException(int id, string title, Exception ex);

        /// <summary>
        /// Write a exception to the console. 
        /// </summary>
        /// <param name="id">id of the error</param>
        /// <param name="sourceFile">a path to the file where the warning can be located</param>
        /// <param name="line">the line in the file where the exception occured</param>
        /// <param name="character">the character offset from the exception causing of the line</param>
        /// <param name="title">title text for message</param>
        /// <param name="ex">exception object</param>
        void WriteException(int id, string sourceFile, int line, int character, string title, Exception ex);

        /// <summary>
        /// Write a stack trace string to the console
        /// </summary>
        /// <param name="trace">trace string</param>
        void WriteStackTrace(string trace);

        /// <summary>
        /// Write a message to the console
        /// </summary>
        /// <param name="type">message type</param>
        /// <param name="colour">colour</param>
        /// <param name="str">message</param>
        void WriteMessage(ConsoleMessage type, ConsoleColorExt colour, string str);

        /// <summary>
        /// Write a message to the console
        /// </summary>
        /// <param name="type">message type</param>
        /// <param name="colour">colour</param>
        /// <param name="str">message</param>
        void WriteMessage(ConsoleMessage type, ConsoleThemeColor colour, string str);

        #endregion

        #region Main Write Message Implementation

        /// <summary>
        /// Write a message to the console
        /// </summary>
        /// <param name="type">message type</param>
        /// <param name="colour">colour</param>
        /// <param name="errorId">id for the error, warning or exception</param>
        /// <param name="str">message</param>
        void WriteMessage(ConsoleMessage type, ConsoleColorExt colour, int errorId, string str);

        /// <summary>
        /// Write a message to the console
        /// </summary>
        /// <param name="type">message type</param>
        /// <param name="colour">colour</param>
        /// <param name="errorId">id for the error, warning or exception</param>
        /// <param name="sourceFile">a path to the file where the warning can be located</param>
        /// <param name="line">the line in the file where the exception occured</param>
        /// <param name="character">the character offset from the exception causing of the line</param>
        /// <param name="str">message</param>
        void WriteMessage(ConsoleMessage type, ConsoleColorExt colour, int errorId, string sourceFile, int line, int character, string str);

        /// <summary>
        /// Write a message to the console
        /// </summary>
        /// <param name="type">message type</param>
        /// <param name="colour">colour</param>
        /// <param name="errorId">id for the error, warning or exception</param>
        /// <param name="str">message</param>
        void WriteMessage(ConsoleMessage type, ConsoleThemeColor colour, int errorId, string str);

        /// <summary>
        /// Write a message to the console
        /// </summary>
        /// <param name="type">message type</param>
        /// <param name="colour">colour</param>
        /// <param name="errorId">id for the error, warning or exception</param>
        /// <param name="sourceFile">a path to the file where the warning can be located</param>
        /// <param name="line">the line in the file where the exception occured</param>
        /// <param name="character">the character offset from the exception causing of the line</param>
        /// <param name="str">message</param>
        void WriteMessage(ConsoleMessage type, ConsoleThemeColor colour, int errorId, string sourceFile, int line, int character, string str);      


        #endregion

        #endregion

        #region Interpreted Console Write

        /// <summary>
        /// Write a interpreted buffer int the console
        /// </summary>
        /// <param name="buffer">formated buffer</param>
        void WriteInterpreted(string buffer);

        /// <summary>
        /// Write a interpreted buffer int the console
        /// </summary>
        /// <param name="buffer">formated buffer</param>
        /// <param name="paddingLeft">margin on the left</param>
        /// <param name="paddingRight">margin on the right</param>
        void WriteInterpreted(string buffer, int paddingLeft, int paddingRight);

        /// <summary>
        /// Write a interpreted buffer int the console
        /// </summary>
        /// <param name="colour">initial colour</param>
        /// <param name="buffer">formated buffer</param>
        /// <param name="paddingLeft">margin on the left</param>
        /// <param name="paddingRight">margin on the right</param>
        void WriteInterpreted(ConsoleColorExt colour, string buffer, int paddingLeft, int paddingRight);

        /// <summary>
        /// Write a interpreted buffer int the console
        /// </summary>
        /// <param name="colour">initial colour</param>
        /// <param name="buffer">formated buffer</param>
        /// <param name="paddingLeft">margin on the left</param>
        /// <param name="paddingRight">margin on the right</param>
        void WriteInterpreted(ConsoleThemeColor colour, string buffer, int paddingLeft, int paddingRight);

        /// <summary>
        /// Write a interpreted line buffer
        /// </summary>
        /// <param name="buffer">formated buffer</param>
        void WriteInterpretedLine(string buffer);

        #endregion  
    }
}
