﻿/* 
 * Rug.Cmd
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using Rug.Cmd.Colors;

namespace Rug.Cmd
{
    public class RC : RugConsole { }

    public class RugConsole 
    {
        /// <summary>
        /// Static refrence to a instance of IConsole that is the system console
        /// </summary>
        public static IConsole Sys = ConsoleExt.SystemConsole;

        /// <summary>
        /// Static refrence to a instance of IConsole that is the application console
        /// </summary>
        public static IConsole App = ConsoleExt.SystemConsole;

        #region IConsole Members

        #region Options

        /// <summary>
        /// Gets or sets the prefix for errors and warnings. 
        /// To be used in conjunction with ConsoleExt.IsBuildMode.
        /// </summary>
        public static string ApplicationBuildReportPrefix
        {
            get
            {
                return App.ApplicationBuildReportPrefix;
            }
            set
            {
                App.ApplicationBuildReportPrefix = value;
            }
        }

        /// <summary>
        /// Gets or sets the level of wordiness of the applications output
        /// </summary>
        public static ConsoleVerbosity Verbosity
        {
            get
            {
                return App.Verbosity;
            }
            set
            {
                App.Verbosity = value;
            }
        }

        /// <summary>
        /// Gets or sets the default prompt answer i.e the yes / no for any confirm prompt 
        /// </summary>
        public static bool DefaultPromptAnswer
        {
            get
            {
                return App.DefaultPromptAnswer;
            }
            set
            {
                App.DefaultPromptAnswer = value;
            }
        }

        /// <summary>
        /// Gets or sets the value indicating the the DefaultPromptAnswer will be used in the case of a confirm prompt
        /// </summary>
        public static bool UseDefaultPromptAnswer
        {
            get
            {
                return App.UseDefaultPromptAnswer;
            }
            set
            {
                App.UseDefaultPromptAnswer = value;
            }
        }

        /// <summary>
        /// Gets or sets the value indicating that all warnings should be handled as errors. 
        /// To be used in conjunction with ConsoleExt.IsBuildMode.
        /// </summary>
        public static bool WarningsAsErrors
        {
            get
            {
                return App.WarningsAsErrors;
            }
            set
            {
                App.WarningsAsErrors = value;
            }
        }

        /// <summary>
        /// Gets or sets the value indicating that all warnings should be reported as MSVS build errors/warnings at all. 
        /// To be used in conjunction with ConsoleExt.IsBuildMode.
        /// </summary>
        public static bool ReportWarnings
        {
            get
            {
                return App.ReportWarnings;
            }
            set
            {
                App.ReportWarnings = value;
            }
        }

        /// <summary>
        /// Gets or sets the value indicating whether the applications console output should be formatted to comply with MSVS project build.
        /// </summary>
        public static bool IsBuildMode
        {
            get
            {
                return App.IsBuildMode;
            }
            set
            {
                App.IsBuildMode = value;
            }
        }

        #endregion

        #region Capablitys

        /// <summary>
        /// True if the console buffer write position can be manualy moved or rolled back.
        /// </summary>
        public static bool CanManipulateBuffer
        {
            get
            {
                return App.CanManipulateBuffer;
            }
        }

        #endregion

        #region Buffer Properties

        /// <summary>
        /// Gets or sets the width of the buffer area if its available. 
        /// </summary>
        public static int BufferWidth
        {
            get
            {
                return App.BufferWidth;
            }
            set
            {
                App.BufferWidth = value;
            }
        }

        /// <summary>
        /// Gets or sets the height of the buffer area if its available.  
        /// </summary>
        public static int BufferHeight
        {
            get
            {
                return App.BufferHeight;
            }
            set
            {
                App.BufferHeight = value;
            }
        }

        /// <summary>
        /// Gets or sets the column position of the cursor within the buffer area.
        /// </summary>
        public static int CursorLeft
        {
            get
            {
                return App.CursorLeft;
            }
            set
            {
                App.CursorLeft = value;
            }
        }

        /// <summary>
        /// Gets or sets the row position of the cursor within the buffer area.
        /// </summary>
        public static int CursorTop
        {
            get
            {
                return App.CursorTop;
            }
            set
            {
                App.CursorTop = value;
            }
        }

        /// <summary>
        /// Gets or sets the height of the cursor within a character cell.
        /// </summary>
        public static int CursorSize
        {
            get
            {
                return App.CursorSize;
            }
            set
            {
                App.CursorSize = value;
            }
        }
        
        #endregion

        #region Colours

        /// <summary>
        /// Gets or sets the current colour theme 
        /// </summary>
        public static ConsoleColorTheme Theme
        {
            get { return App.Theme; }
            set { App.Theme = value; } 
        }

        /// <summary>
        /// Gets or sets the foreground color for the current write position.
        /// </summary>
        public static ConsoleColorExt ForegroundColor
        {
            get { return App.ForegroundColor; }
            set 
            {
                if (value != ConsoleColorExt.Inhreit)
                    App.ForegroundColor = value; 
            } 
        }

        /// <summary>
        /// Gets or sets the background color for the current write position.
        /// </summary>
        public static ConsoleColorExt BackgroundColor
        {
            get { return App.BackgroundColor; }
            set
            {
                if (value != ConsoleColorExt.Inhreit)
                    App.BackgroundColor = value;
            }
        }

        /// <summary>
        /// Sets the foreground colour for the current write position to the value of the the theme color.
        /// </summary>
        public static ConsoleThemeColor ForegroundThemeColor
        {
            set { App.ForegroundThemeColor = value; }
        }

        /// <summary>
        /// Sets the background colour for the current write position to the value of the the theme color.
        /// </summary>
        public static ConsoleThemeColor BackgroundThemeColor
        {
            set { App.BackgroundThemeColor = value; }
        }

        #endregion

        #region State Methods
        
        /// <summary>
        /// Gets or sets the current console colour state
        /// </summary>
        public static ConsoleColorState ColorState
        {
            get { return new ConsoleColorState(ForegroundColor, BackgroundColor); }
            set
            {
                ForegroundColor = value.ForegroundColor;
                BackgroundColor = value.BackgroundColor;
            }
        }

        #endregion

        #region Methods forwarded from System.Console

		/// <summary>
		/// Reset the colors
		/// </summary>
        public static void ResetColor()
        {
            App.ResetColor(); 
        }

        /// <summary>
        /// Sets the position of the cursor.
        /// </summary>
        /// <param name="left">The column position of the cursor.</param>
        /// <param name="top">The row position of the cursor.</param>
        public static void SetCursorPosition(int left, int top)
        {
            App.SetCursorPosition(left, top);
        }

		/// <summary>
		/// Clears the console buffer and corresponding console window of display information.
		/// </summary>
		public static void Clear()
		{
			App.Clear();
		}

        #endregion 

		#region Read Key / Line Methods

		/// <summary>
		/// Gets a value indicating whether a key press is available in the input stream.
		/// returns true if a key press is available; otherwise, false.
		/// </summary>
		public static bool KeyAvailable { get { return App.KeyAvailable; } } 

		/// <summary>
		/// Reads the next character from the standard input stream.
		/// </summary>
		/// <returns>The next character from the input stream, or negative one (-1) if there are currently no more characters to be read.</returns>
		public static int Read()
		{
			return App.Read();
		}

		/// <summary>
		/// Obtains the next character or function key pressed by the user. The pressed key is displayed in the console window.
		/// </summary>
		/// <returns>A System.ConsoleKeyInfo object that describes the System.ConsoleKey constant and Unicode character, if any, that correspond to the pressed console key. The System.ConsoleKeyInfo object also describes, in a bitwise combination of System.ConsoleModifiers values, whether one or more SHIFT, ALT, or CTRL modifier keys was pressed simultaneously with the console key.</returns>
		public static ConsoleKeyInfo ReadKey()
		{
			return App.ReadKey();
		}


		/// <summary>
		/// Obtains the next character or function key pressed by the user. The pressed key is optionally displayed in the console window.
		/// </summary>
		/// <param name="intercept">Determines whether to display the pressed key in the console window. true to not display the pressed key; otherwise, false.</param>
		/// <returns>A System.ConsoleKeyInfo object that describes the System.ConsoleKey constant and Unicode character, if any, that correspond to the pressed console key. The System.ConsoleKeyInfo object also describes, in a bitwise combination of System.ConsoleModifiers values, whether one or more SHIFT, ALT, or CTRL modifier keys was pressed simultaneously with the console key.</returns>
		public static ConsoleKeyInfo ReadKey(bool intercept)
		{
			return App.ReadKey(intercept);
		}


		/// <summary>
		/// Reads the next line of characters from the standard input stream.
		/// </summary>
		/// <returns>The next line of characters from the input stream, or null if no more lines are available.</returns>
		public static string ReadLine()
		{
			return App.ReadLine();
		}


		#endregion

		#region Special Prompt Methods

		/// <summary>
		/// Prompt for key input 
		/// </summary>
		/// <param name="message">message to display</param>
		/// <param name="intercept">Determines whether to display the pressed key in the console window. true to not display the pressed key; otherwise, false.</param>
		/// <param name="throwException">Should this method throw an exception if key input is not possible</param>
		/// <returns>A System.ConsoleKeyInfo object that describes the System.ConsoleKey constant and Unicode character, if any, that correspond to the pressed console key. The System.ConsoleKeyInfo object also describes, in a bitwise combination of System.ConsoleModifiers values, whether one or more SHIFT, ALT, or CTRL modifier keys was pressed simultaneously with the console key.</returns>
		public static ConsoleKeyInfo PromptForKey(string message, bool intercept, bool throwException)
		{
			return App.PromptForKey(message, intercept, throwException);
		}


		/// <summary>
		/// Prompt for line input 
		/// </summary>
		/// <param name="message">message to display</param>
		/// <param name="throwException">Should this method throw an exception if key input is not possible</param>
		/// <returns>The next line of characters from the input stream, or null if no more lines are available.</returns>
		public static string PromptForLine(string message, bool throwException)
		{
			return App.PromptForLine(message, throwException);
		}


		#endregion


        #region Write Methods
        
        /// <summary>
        /// Check if a massage would be written
        /// </summary>
        /// <param name="verbosity">level to check against</param>
        /// <returns>true if the ConsoleVerbosity supplyed would result in a message in the current context</returns>
        public static bool ShouldWrite(ConsoleVerbosity verbosity)
        {
            return App.ShouldWrite(verbosity);
        }

        /// <summary>
        /// Writes the specified string value to the standard output stream at the ConsoleVerbosity.Normal level.
        /// </summary>
        /// <param name="str">The value to write.</param>
        public static void Write(string str)
        {
            App.Write(str);
        }

        /// <summary>
        /// Writes the specified string value to the standard output stream at the verbosity level specified.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        /// <param name="str">The value to write.</param>
        public static void Write(ConsoleVerbosity level, string str)
        {
            App.Write(level, str);
        }

        /// <summary>
        /// Writes the specified string value to the standard output stream at the verbosity level specified and in the specified colour.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        /// <param name="colour">The colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        public static void Write(ConsoleVerbosity level, ConsoleColorExt colour, string str)
        {
            App.Write(level, colour, str);
        }

        /// <summary>
        /// Writes the specified string value to the standard output stream at the verbosity level specified and in the specified colour.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        /// <param name="colour">The theme colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        public static void Write(ConsoleVerbosity level, ConsoleThemeColor colour, string str)
        {
            App.Write(level, colour, str);
        }

        /// <summary>
        /// Writes the specified string value to the standard output stream at the ConsoleVerbosity.Normal level and in the specified colour.
        /// </summary>
        /// <param name="colour">The colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        public static void Write(ConsoleColorExt colour, string str)
        {
            App.Write(colour, str);
        }

        /// <summary>
        /// Writes the specified string value to the standard output stream at the ConsoleVerbosity.Normal level and in the specified colour.
        /// </summary>
        /// <param name="colour">The theme colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        public static void Write(ConsoleThemeColor colour, string str)
        {
            App.Write(colour, str);
        }

        #endregion

        #region Write Line Methods

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the ConsoleVerbosity.Normal level.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        public static void WriteLine(ConsoleVerbosity level)
        {
            App.WriteLine(level);
        }

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the ConsoleVerbosity.Normal level.
        /// </summary>
        public static void WriteLine()
        {
            App.WriteLine();
        }

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the ConsoleVerbosity.Normal level.
        /// </summary>
        /// <param name="str">The value to write.</param>
        public static void WriteLine(string str)
        {
            App.WriteLine(str);
        }

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the verbosity level specified.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        /// <param name="str">The value to write.</param>
        public static void WriteLine(ConsoleVerbosity level, string str)
        {
            App.WriteLine(level, str);
        }

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the verbosity level specified and in the specified colour.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        /// <param name="colour">The colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        public static void WriteLine(ConsoleVerbosity level, ConsoleColorExt colour, string str)
        {
            App.WriteLine(level, colour, str);
        }

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the verbosity level specified and in the specified colour.
        /// </summary>
        /// <param name="level">The verbosity level.</param>
        /// <param name="colour">The theme colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        public static void WriteLine(ConsoleVerbosity level, ConsoleThemeColor colour, string str)
        {
            App.WriteLine(level, colour, str);
        }

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the ConsoleVerbosity.Normal level and in the specified colour.
        /// </summary>
        /// <param name="colour">The colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        public static void WriteLine(ConsoleColorExt colour, string str)
        {
            App.WriteLine(colour, str);
        }

        /// <summary>
        /// Writes the specified string value, followed by the current line terminator, to the standard output stream at the ConsoleVerbosity.Normal level and in the specified colour.
        /// </summary>
        /// <param name="colour">The theme colour that the string should be written in.</param>        
        /// <param name="str">The value to write.</param>
        public static void WriteLine(ConsoleThemeColor colour, string str)
        {
            App.WriteLine(colour, str);
        }

        #endregion

        #region Additional Write Methods

        /// <summary>
        /// Write text as wrapped text within a fixed width
        /// </summary>
        /// <param name="colour">color to write in</param>
        /// <param name="message">the text to write</param>
        /// <param name="paddingLeft">margin at the left</param>
        /// <param name="paddingRight">margin at the right</param>
        public static void WriteWrapped(ConsoleColorExt colour, string message, int paddingLeft, int paddingRight)
        {
            App.WriteWrapped(colour, message, paddingLeft, paddingRight);
        }


        /// <summary>
        /// Write text as wrapped text within a fixed width
        /// </summary>
        /// <param name="colour">theme color to write in</param>
        /// <param name="message">the text to write</param>
        /// <param name="paddingLeft">margin at the left</param>
        /// <param name="paddingRight">margin at the right</param>
        public static void WriteWrapped(ConsoleThemeColor colour, string message, int paddingLeft, int paddingRight)
        {
            App.WriteWrapped(colour, message, paddingLeft, paddingRight);
        }

        /// <summary>
        /// Write a prompt message.
        /// If this application is set to build mode then the prompt will be a error.
        /// The reading of any keys is the responsibility of the calling program.
        /// </summary>
        /// <param name="str">message</param>
        public static void WritePrompt(string str)
        {
            App.WritePrompt(str);
        }

        /// <summary>
        /// Write a prompt message.
        /// If this application is set to build mode then the prompt will be a error.
        /// The reading of any keys is the responsibility of the calling program.
        /// </summary>        
        /// <param name="colour">colour</param>
        /// <param name="str">message</param>
        public static void WritePrompt(ConsoleColorExt colour, string str)
        {
            App.WritePrompt(colour, str);
        }

        /// <summary>
        /// Write a prompt message.
        /// If this application is set to build mode then the prompt will be a error.
        /// The reading of any keys is the responsibility of the calling program.
        /// </summary>        
        /// <param name="colour">colour</param>
        /// <param name="str">message</param>
        public static void WritePrompt(ConsoleThemeColor colour, string str)
        {
            App.WritePrompt(colour, str);
        }

        /// <summary>
        /// Write a error to the console.
        /// If this application is set to build mode the error will be formatted for Visual Studio 
        /// </summary>
        /// <param name="id">id for the error</param>
        /// <param name="str">message</param>
        public static void WriteError(int id, string str)
        {
            App.WriteError(id, str);
        }

        /// <summary>
        /// Write a error to the console.
        /// If this application is set to build mode the error will be formatted for Visual Studio 
        /// </summary>
        /// <param name="id">id for the error</param>
        /// <param name="sourceFile">a path to the file where the error can be located</param>
        /// <param name="line">the line in the file where the error occured</param>
        /// <param name="character">the character offset from the start of the line</param>
        /// <param name="str">message</param>
        public static void WriteError(int id, string sourceFile, int line, int character, string str)
        {
            App.WriteError(id, sourceFile, line, character, str);
        }

        /// <summary>
        /// Write a error to the console.
        /// If this application is set to build mode the error will be formatted for Visual Studio 
        /// </summary>
        /// <param name="colour">colour</param>
        /// <param name="id">id for the error</param>
        /// <param name="str">message</param>
        public static void WriteError(ConsoleColorExt colour, int id, string str)
        {
            App.WriteError(colour, id, str);
        }

        /// <summary>
        /// Write a error to the console.
        /// If this application is set to build mode the error will be formatted for Visual Studio 
        /// </summary>
        /// <param name="colour">colour</param>
        /// <param name="id">id for the error</param>
        /// <param name="str">message</param>
        public static void WriteError(ConsoleThemeColor colour, int id, string str)
        {
            App.WriteError(colour, id, str);
        }

        /// <summary>
        /// Write a error to the console. 
        /// If this application is set to build mode the warning will be formatted for Visual Studio.            
        /// If the WarningsAsErrors flag has been set to true the warning will be treated as an error. 
        /// </summary>
        /// <param name="id">id for the warning</param>
        /// <param name="str">message</param>
        public static void WriteWarning(int id, string str)
        {
            App.WriteWarning(id, str);
        }

        /// <summary>
        /// Write a error to the console. 
        /// If this application is set to build mode the warning will be formatted for Visual Studio.            
        /// If the WarningsAsErrors flag has been set to true the warning will be treated as an error. 
        /// </summary>
        /// <param name="colour">colour</param>
        /// <param name="id">id for the warning</param>
        /// <param name="str">message</param>
        public static void WriteWarning(ConsoleColorExt colour, int id, string str)
        {
            App.WriteWarning(colour, id, str);
        }

        /// <summary>
        /// Write a error to the console. 
        /// If this application is set to build mode the warning will be formatted for Visual Studio.            
        /// If the WarningsAsErrors flag has been set to true the warning will be treated as an error. 
        /// </summary>
        /// <param name="colour">colour</param>
        /// <param name="id">id for the warning</param>
        /// <param name="str">message</param>
        public static void WriteWarning(ConsoleThemeColor colour, int id, string str)
        {
            App.WriteWarning(colour, id, str);
        }

        /// <summary>
        /// Write a error to the console. 
        /// If this application is set to build mode the warning will be formatted for Visual Studio.            
        /// If the WarningsAsErrors flag has been set to true the warning will be treated as an error. 
        /// </summary>
        /// <param name="colour">colour</param>
        /// <param name="id">id for the warning</param>        
        /// <param name="sourceFile">a path to the file where the warning can be located</param>
        /// <param name="line">the line in the file where the warning occured</param>
        /// <param name="character">the character offset from the warning of the line</param>
        /// <param name="str">message</param>
        public static void WriteWarning(ConsoleColorExt colour, int id, string sourceFile, int line, int character, string str)
        {
            App.WriteWarning(colour, id, sourceFile, line, character, str);
        }


        /// <summary>
        /// Write a error to the console. 
        /// If this application is set to build mode the warning will be formatted for Visual Studio.            
        /// If the WarningsAsErrors flag has been set to true the warning will be treated as an error. 
        /// </summary>
        /// <param name="colour">colour</param>
        /// <param name="id">id for the warning</param>        
        /// <param name="sourceFile">a path to the file where the warning can be located</param>
        /// <param name="line">the line in the file where the warning occured</param>
        /// <param name="character">the character offset from the warning of the line</param>
        /// <param name="str">message</param>
        public static void WriteWarning(ConsoleThemeColor colour, int id, string sourceFile, int line, int character, string str)
        {
            App.WriteWarning(colour, id, sourceFile, line, character, str);
        }

        /// <summary>
        /// Write a exception to the console. 
        /// </summary>
        /// <param name="id">id of the error</param>
        /// <param name="ex">exception object</param>
        public static void WriteException(int id, Exception ex)
        {
            App.WriteException(id, ex);
        }

        /// <summary>
        /// Write a exception to the console. 
        /// </summary>
        /// <param name="id">id of the error</param>
        /// <param name="sourceFile">a path to the file where the warning can be located</param>
        /// <param name="line">the line in the file where the exception occured</param>
        /// <param name="character">the character offset from the exception causing of the line</param>
        /// <param name="ex">exception object</param>
        public static void WriteException(int id, string sourceFile, int line, int character, Exception ex)
        {
            App.WriteException(id, sourceFile, line, character, ex);
        }

        /// <summary>
        /// Write a exception to the console. 
        /// </summary>
        /// <param name="id">id of the error</param>
        /// <param name="title">title text for message</param>
        /// <param name="ex">exception object</param>
        public static void WriteException(int id, string title, Exception ex)
        {
            App.WriteException(id, title, ex);
        }

        /// <summary>
        /// Write a exception to the console. 
        /// </summary>
        /// <param name="id">id of the error</param>
        /// <param name="sourceFile">a path to the file where the warning can be located</param>
        /// <param name="line">the line in the file where the exception occured</param>
        /// <param name="character">the character offset from the exception causing of the line</param>
        /// <param name="title">title text for message</param>
        /// <param name="ex">exception object</param>
        public static void WriteException(int id, string sourceFile, int line, int character, string title, Exception ex)
        {
            App.WriteException(id, sourceFile, line, character, title, ex);
        }

        /// <summary>
        /// Write a stack trace string to the console
        /// </summary>
        /// <param name="trace">trace string</param>
        public static void WriteStackTrace(string trace)
        {
            App.WriteStackTrace(trace);
        }

        /// <summary>
        /// Write a message to the console
        /// </summary>
        /// <param name="type">message type</param>
        /// <param name="colour">colour</param>
        /// <param name="str">message</param>
        public static void WriteMessage(ConsoleMessage type, ConsoleColorExt colour, string str)
        {
            App.WriteMessage(type, colour, str);
        }

        /// <summary>
        /// Write a message to the console
        /// </summary>
        /// <param name="type">message type</param>
        /// <param name="colour">colour</param>
        /// <param name="str">message</param>
        public static void WriteMessage(ConsoleMessage type, ConsoleThemeColor colour, string str)
        {
            App.WriteMessage(type, colour, str);
        }

        /// <summary>
        /// Write a message to the console
        /// </summary>
        /// <param name="type">message type</param>
        /// <param name="colour">colour</param>
        /// <param name="errorId">id for the error, warning or exception</param>
        /// <param name="str">message</param>
        public static void WriteMessage(ConsoleMessage type, ConsoleColorExt colour, int errorId, string str)
        {
            App.WriteMessage(type, colour, errorId, str);
        }

        /// <summary>
        /// Write a message to the console
        /// </summary>
        /// <param name="type">message type</param>
        /// <param name="colour">colour</param>
        /// <param name="errorId">id for the error, warning or exception</param>
        /// <param name="str">message</param>
        public static void WriteMessage(ConsoleMessage type, ConsoleThemeColor colour, int errorId, string str)
        {
            App.WriteMessage(type, colour, errorId, str);
        }

        /// <summary>
        /// Write a message to the console
        /// </summary>
        /// <param name="type">message type</param>
        /// <param name="colour">colour</param>
        /// <param name="errorId">id for the error, warning or exception</param>
        /// <param name="sourceFile">a path to the file where the warning can be located</param>
        /// <param name="line">the line in the file where the exception occured</param>
        /// <param name="character">the character offset from the exception causing of the line</param>
        /// <param name="str">message</param>
        public static void WriteMessage(ConsoleMessage type, ConsoleColorExt colour, int errorId, string sourceFile, int line, int character, string str)
        {
            App.WriteMessage(type, colour, errorId, sourceFile, line, character, str);
        }

        /// <summary>
        /// Write a message to the console
        /// </summary>
        /// <param name="type">message type</param>
        /// <param name="colour">colour</param>
        /// <param name="errorId">id for the error, warning or exception</param>
        /// <param name="sourceFile">a path to the file where the warning can be located</param>
        /// <param name="line">the line in the file where the exception occured</param>
        /// <param name="character">the character offset from the exception causing of the line</param>
        /// <param name="str">message</param>
        public static void WriteMessage(ConsoleMessage type, ConsoleThemeColor colour, int errorId, string sourceFile, int line, int character, string str)
        {
            App.WriteMessage(type, colour, errorId, sourceFile, line, character, str);
        }

        /// <summary>
        /// Write a interpreted buffer int the console
        /// </summary>
        /// <param name="buffer">formated buffer</param>
        public static void WriteInterpreted(string buffer)
        {
            App.WriteInterpreted(buffer);
        }
        
        /// <summary>
        /// Write a interpreted buffer int the console
        /// </summary>
        /// <param name="buffer">formated buffer</param>
        /// <param name="paddingLeft">margin on the left</param>
        /// <param name="paddingRight">margin on the right</param>
        public static void WriteInterpreted(string buffer, int paddingLeft, int paddingRight)
        {
            App.WriteInterpreted(buffer, paddingLeft, paddingRight);
        }

        /// <summary>
        /// Write a interpreted buffer int the console
        /// </summary>
        /// <param name="colour">initial colour</param>
        /// <param name="buffer">formated buffer</param>
        /// <param name="paddingLeft">margin on the left</param>
        /// <param name="paddingRight">margin on the right</param>
        public static void WriteInterpreted(ConsoleThemeColor colour, string buffer, int paddingLeft, int paddingRight)
        {
            App.WriteInterpreted(colour, buffer, paddingLeft, paddingRight);
        }

        /// <summary>
        /// Write a interpreted buffer int the console
        /// </summary>
        /// <param name="colour">initial colour</param>
        /// <param name="buffer">formated buffer</param>
        /// <param name="paddingLeft">margin on the left</param>
        /// <param name="paddingRight">margin on the right</param>
        public static void WriteInterpreted(ConsoleColorExt colour, string buffer, int paddingLeft, int paddingRight)
        {
            App.WriteInterpreted(colour, buffer, paddingLeft, paddingRight);
        }

        /// <summary>
        /// Write a interpreted line buffer
        /// </summary>
        /// <param name="buffer">formated buffer</param>
        public static void WriteInterpretedLine(string buffer)
        {
            App.WriteInterpretedLine(buffer);
        }

        #endregion

        #endregion
    }
}
