﻿/* 
 * Rug.Cmd
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System.Xml;

namespace Rug.Cmd
{
    /// <summary>
    /// Version 1.0.0.0
    /// </summary>
    public partial class Helper
    {
        #region Node helpers

        public static XmlNode FindChild(XmlNode node, string name)
        {
            return node.SelectSingleNode(name);
        }

        public static XmlElement AppendChild(XmlElement element, string name)
        {
            if (IsNotNullOrEmpty(name) && element != null)
            {
                XmlElement el = element.OwnerDocument.CreateElement(name);

                element.AppendChild(el);

                return el;
            }

            return null;
        }

        public static void AppendAttributeAndValue(XmlElement element, string name, string value)
        {
            if (IsNotNullOrEmpty(value))
            {
                element.Attributes.Append(element.OwnerDocument.CreateAttribute(name));
                element.Attributes[name].Value = value;
            }
        }

        public static string GetAttributeValue(XmlNode node, string name, string @default)
        {
            if ((node.Attributes[name] != null))
                return node.Attributes[name].Value;
            else
                return @default;
        }

        public static bool HasAttribute(XmlNode node, string name)
        {
            return node.Attributes[name] != null;
        }

        #endregion
    }
}