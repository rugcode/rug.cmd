/* 
 * Rug.Cmd
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;
using System.IO;

namespace Rug.Cmd.Colors
{
    /// <summary>
    /// Holds a dictionary of console color themes keyed by background color. 
    /// </summary>
    public sealed class ConsoleColorThemeDirectory
    {
        #region Private Members

        // Constant length of the theme lookup
        private const int Length = 32 * 16;
        // Raw bytes of the theme lookup
        private byte[] m_RawBytes = new byte[Length];

        #endregion

        #region Lookup Indexer

        /// <summary>
        /// Get console color theme for a given background color
        /// </summary>
        /// <param name="background">the background color to use as the key to lookup the correct theme</param>
        /// <returns>the console color them for the background color</returns>
        public ConsoleColorTheme this[ConsoleColor background]
        {
            get 
            {
                ConsoleColorTheme theme = new ConsoleColorTheme();

                int index = ((int)background) * 32;

                for (int i = 0; i < 32; i++)
                {
                    // load the color theme from the raw bytes
                    theme.Mappings[i] = (ConsoleColorExt)m_RawBytes[index++];
                }

                return theme; 
            }
            set 
            {
                int index = ((int)background) * 32;

                for (int i = 0; i < 32; i++)
                {
                    // write the colors into the raw bytes
                    m_RawBytes[index++] = (byte)value.Mappings[i];
                }
            }
        }

        #endregion 

        #region Read

        /// <summary>
        /// Read a lookup from a file path
        /// </summary>
        /// <param name="path">the file path to read from</param>
        /// <returns>a lookup of console color themes keyed by background color</returns>
        public static ConsoleColorThemeDirectory Read(string path)
        {
            FileInfo info = new FileInfo(path);

            if (!info.Exists)
            {
                throw new Exception("");
            }

            using (FileStream stream = new FileStream(path, FileMode.Open))
            {
                return Read(stream);
            }
        }

        /// <summary>
        /// Read a lookup from a stream 
        /// </summary>
        /// <param name="stream">the stream containing the lookup</param>
        /// <returns>a lookup of console color themes keyed by background color</returns>
        public static ConsoleColorThemeDirectory Read(Stream stream)
        {
            ConsoleColorThemeDirectory themes = new ConsoleColorThemeDirectory();

            if (stream.Length == Length)
            {
                stream.Read(themes.m_RawBytes, 0, Length);
            }

            return themes;
        }

        #endregion

        #region Write

        /// <summary>
        /// Write the lookup to a file
        /// </summary>
        /// <param name="path">the path to write too</param>
        public void Write(string path)
        {
            FileInfo info = new FileInfo(path);

            if (info.Exists)
            {
                info.Delete();
            }

            using (FileStream stream = new FileStream(path, FileMode.Create))
            {
                Write(stream);
            }
        }

        /// <summary>
        /// Write the lookup to a stream
        /// </summary>
        /// <param name="stream">a stream to write to</param>
        public void Write(Stream stream)
        {
            stream.Write(m_RawBytes, 0, Length);
        }

        #endregion
    }
}
