﻿/* 
 * Rug.Cmd
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;

namespace Rug.Cmd
{
    /// <summary>
    /// Specifies constants that define foreground and background colors for the console.
    /// </summary>
    [Serializable]    
    public enum ConsoleColorExt
    {
        /// <summary>
        /// The color black.
        /// </summary>             
        Black = 0,

        /// <summary>
        /// The color dark blue.
        /// </summary>
        DarkBlue = 1,

        /// <summary>
        /// The color dark green.
        /// </summary>
        DarkGreen = 2,

        /// <summary>
        /// The color dark cyan (dark blue-green).
        /// </summary>
        DarkCyan = 3,

        /// <summary>
        /// The color dark red.
        /// </summary>
        DarkRed = 4,

        /// <summary>
        /// The color dark magenta (dark purplish-red).
        /// </summary>
        DarkMagenta = 5,

        /// <summary>
        /// The color dark yellow (ochre).
        /// </summary>
        DarkYellow = 6,

        /// <summary>
        /// The color gray.
        /// </summary>
        Gray = 7,

        /// <summary>
        /// The color dark gray.
        /// </summary>
        DarkGray = 8,

        /// <summary>
        /// The color blue.
        /// </summary>
        Blue = 9,

        /// <summary>
        /// The color green.
        /// </summary>
        Green = 10,

        /// <summary>
        /// The color cyan (blue-green).
        /// </summary>
        Cyan = 11,

        /// <summary>
        /// The color red.
        /// </summary>
        Red = 12,

        /// <summary>
        /// The color magenta (purplish-red).
        /// </summary>
        Magenta = 13,

        /// <summary>
        /// The color yellow.
        /// </summary>
        Yellow = 14,

        /// <summary>
        /// The color white.
        /// </summary>
        White = 15,

        /// <summary>
        /// The extra colour, Inhreit allows the ConsoleControl to avoid changing the colours.
        /// </summary>
        Inhreit = 16
    }
}
