/* 
 * Rug.Cmd
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;

namespace Rug.Cmd
{
    /// <summary>
    /// Specifies constants that define foreground and background colors for console theme elements.
    /// </summary>
    [Serializable]  
    public enum ConsoleThemeColor
    {
        /// <summary>
        /// Application Background
        /// </summary>
        AppBackground = 0,

        /// <summary>
        /// Default title text colour
        /// </summary>
        TitleText = 1,

        /// <summary>
        /// Alt 1 title text colour
        /// </summary>
        TitleText1 = 2,

        /// <summary>
        /// Alt 2 title text colour
        /// </summary>
        TitleText2 = 3,

        /// <summary>
        /// Alt 2 title text colour
        /// </summary>
        TitleText3 = 4,

        /// <summary>
        /// Default text colour
        /// </summary>
        Text = 5,

        /// <summary>
        /// Alt 1 text colour
        /// </summary>
        Text1 = 6,

        /// <summary>
        /// Alt 2 text colour
        /// </summary>
        Text2 = 7,

        /// <summary>
        /// Alt 2 text colour
        /// </summary>
        Text3 = 8,

        /// <summary>
        /// Default sub text colour
        /// </summary>
        SubText = 9,

        /// <summary>
        /// Alt 1 sub text colour
        /// </summary>
        SubText1 = 10,

        /// <summary>
        /// Alt 2 sub text colour
        /// </summary>
        SubText2 = 11,

        /// <summary>
        /// Alt 2 sub text colour
        /// </summary>
        SubText3 = 12,

        /// <summary>
        /// Error text 1 
        /// </summary>
        ErrorColor1 = 13,
        /// <summary>
        /// Error text 2 
        /// </summary>
        ErrorColor2 = 14,

        /// <summary>
        /// Warning text 1 
        /// </summary>
        WarningColor1 = 15,
        /// <summary>
        /// Warning text 2 
        /// </summary>
        WarningColor2 = 16,

        /// <summary>
        /// Prompt text 1 
        /// </summary>
        PromptColor1 = 17,
        /// <summary>
        /// Prompt text 2
        /// </summary>
        PromptColor2 = 18,

        /// <summary>
        /// Colour for things that are 'Good' 
        /// </summary>
        TextGood = 19,

        /// <summary>
        /// Sub text colour for things that are 'Good' 
        /// </summary>
        SubTextGood = 20,

        /// <summary>
        /// Colour for things that are 'Nutral' 
        /// </summary>
        TextNutral = 21,

        /// <summary>
        /// Sub text colour for things that are 'Nutral' 
        /// </summary>
        SubTextNutral = 22,

        /// <summary>
        /// Colour for things that are 'Bad' 
        /// </summary>
        TextBad = 23,

        /// <summary>
        /// Sub text colour for things that are 'Bad' 
        /// </summary>
        SubTextBad = 24,

        /// <summary>
        /// Panel Background
        /// </summary>
        PanelBackground = 25,

        /// <summary>
        /// Panel Edges 
        /// </summary>
        PanelEdges = 26,

        /// <summary>
        /// Panel Title Text
        /// </summary>
        PanelTitleText = 27,

        /// <summary>
        /// Panel Text
        /// </summary>
        PanelText = 28,

        /// <summary>
        /// Panel Sub Text
        /// </summary>
        PanelSubText = 29,

        /// <summary>
        /// Control Background
        /// </summary>
        ControlBackground = 30,

        /// <summary>
        /// Control Text
        /// </summary>
        ControlText = 31
    }
}
