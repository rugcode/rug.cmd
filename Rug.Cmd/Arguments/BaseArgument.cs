﻿/* 
 * Rug.Cmd
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

namespace Rug.Cmd
{
    public abstract class BaseArgument : IArgumentValue
    {
        #region Private Members

        private string m_ShortHelp;
        private string m_Help;
        private bool m_Defined = false;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the short help string 
        /// This is the string that is seen when the user is show a overview of all the arguments
        /// </summary>
        public virtual string ShortHelp
        {
            get { return m_ShortHelp; }
        }

        /// <summary>
        /// Gets the long help string 
        /// This is the string that is seen when the user is show the spacific usage for this argument
        /// </summary>
        public virtual string Help
        {
            get { return m_Help; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this argument was defiend in the parsed command line
        /// </summary>
        public bool Defined
        {
            get { return m_Defined; }
            set { m_Defined = value; }
        }

        /// <summary>
        /// Get the object value of this argument 
        /// </summary>
        public abstract object ObjectValue { get; }

        #endregion

        #region Constructor

        public BaseArgument(string shortHelp, string help)
        {
            m_ShortHelp = shortHelp;
            m_Help = help;
        }

        #endregion

        /// <summary>
        /// Undefines the argument and resets it to its default value
        /// </summary>
        public virtual void Reset()
        {
            m_Defined = false;
        }
        
        /// <summary>
        /// Get short string for the command line usage readout
        /// </summary>
        public abstract string ArgumentString();

        /// <summary>
        /// Parse values from arguments array
        /// </summary>
		/// <param name="parser">the parser that is currently parsing this argument</param>
        /// <param name="key">the key that matched</param>
        /// <param name="arguments">the full arguments array</param>
        /// <param name="index">the current arguemnt index</param>
        /// <returns>true if this argument should now become the current argument, otherwise false</returns>
		public abstract bool Parse(ArgumentParser parser, string key, string[] arguments, ref int index);

        /// <summary>
        /// Set the value of this argument
        /// </summary>
        /// <param name="value">string value</param>
        /// <returns>true if the value was parsed</returns>
        public abstract bool SetValue(string value); 
    }

}
