﻿/* 
 * Rug.Cmd
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

namespace Rug.Cmd
{
    /// <summary>
    /// Base interface for all ArgumentParser Arguemnts
    /// </summary>
    public interface IArgumentValue
    {
        /// <summary>
        /// Get short string for the command line usage readout
        /// </summary>
        string ArgumentString();

        /// <summary>
        /// Gets the short help string 
        /// This is the string that is seen when the user is show a overview of all the arguments
        /// </summary>
        string ShortHelp { get; }

        /// <summary>
        /// Gets the long help string 
        /// This is the string that is seen when the user is show the spacific usage for this argument
        /// </summary>
        string Help { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this argument was defiend in the parsed command line
        /// </summary>
        bool Defined { get; set; }

        /// <summary>
        /// Set the value of this argument
        /// </summary>
        /// <param name="value">string value</param>
        /// <returns>true if the value was parsed</returns>
        bool SetValue(string value);

        /// <summary>
        /// Get the object value of this argument 
        /// </summary>
        object ObjectValue { get; }

        /// <summary>
        /// Parse values from arguments array
        /// </summary>
		/// <param name="parser">the parser that is currently parsing this arguemnt</param>
        /// <param name="key">the key that matched</param>
        /// <param name="arguments">the full arguments array</param>
        /// <param name="index">the current arguemnt index</param>
        /// <returns>true if this argument should now become the current argument, otherwise false</returns>
        bool Parse(ArgumentParser parser, string key, string[] arguments, ref int index);

        /// <summary>
        /// Undefines the argument and resets it to its default value
        /// </summary>
        void Reset();
    }
}
