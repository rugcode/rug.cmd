﻿/* 
 * Rug.Cmd
 * 
 * Copyright (C) 2008 Phill Tew (peatew@gmail.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
 * IN THE SOFTWARE.
 * 
 */

using System;

namespace Rug.Cmd
{
    /// <summary>
    /// Argument lookup key e.g. '/Option' where Prefix = '/', Symbol = 'O' and Name = 'Option'
    /// </summary>
    public class ArgumentKey
    {
        /// <summary>
        /// Argument prefix e.g '/' 
        /// </summary>
        public readonly string Prefix;       

        /// <summary>
        /// Argument Symbol (short lookup string) 
        /// </summary>
        public readonly string Symbol;

        /// <summary>
        /// Arguemnt Name (long lookup string)
        /// </summary>
        public readonly string Name;

        /// <summary>
        /// Argument Lookup Key e.g. '/Option'
        /// </summary>
        /// <param name="prefix">key prefix e.g. '/'</param>
        /// <param name="name">key long lookup string e.g. 'Option'</param>
        /// <param name="symbol">key short lookup string e.g. 'O'</param>
        public ArgumentKey(string prefix, string name, string symbol)
        {
            Prefix = prefix;
            Name = name;
            Symbol = symbol;
        }

        /// <summary>
        /// Equals, this can be another ArgumentKey or a string in the form Prefix + (Name or Symbol) e.g. '/Option'
        /// </summary>
        /// <param name="obj">another ArgumentKey or a string in the form Prefix + (Name or Symbol) e.g. '/Option'</param>
        /// <returns>true if the object supplyed equals this ArgumentKey</returns>
        public override bool Equals(object obj)
        {
            if (obj is ArgumentKey)
            {
                return Symbol.Equals((obj as ArgumentKey).Symbol, StringComparison.InvariantCultureIgnoreCase);
            }
            else if (obj is string)
            {
                string str = obj as string;

                if (str.StartsWith(Prefix))
                {
                    string sub = str.Substring(Prefix.Length);

                    if (sub.Equals(Name, StringComparison.InvariantCultureIgnoreCase) ||
                        sub.Equals(Symbol, StringComparison.InvariantCultureIgnoreCase))
                        return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Get a hash of this ArgumentKey 
        /// </summary>
        /// <returns>a hash of this ArgumentKey</returns>
        public override int GetHashCode()
        {
            return string.Format("{0}/{1}/{2}", Prefix, Name, Symbol).GetHashCode();
        }
    }
}
